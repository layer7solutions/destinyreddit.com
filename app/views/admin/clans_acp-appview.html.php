<?php
$app = xa_stash('app');
$app_id = $app['app_id'];
$data = $app['data'];
$d_user = $data['d_user'];

$clan = from($data, 'clan') ?: [];
$application = from($data, 'application') ?: [];
$DM_list = from($app, 'DM_list') ?: [];
?>
<div class="App__view" data-id="<?php xecho($app_id); ?>">
    <div class="App__head">
        <b>App #<?php xecho($app_id); ?>, on <?php xecho(timeConvert($app['submitted_utc'])); ?></b>
    </div>
    <div class="App__main">
        <?php if (stash('app_type') === 'recruit'): ?>
            <div class="App__clanInfo">
                <h3>Clan</h3>
                <dl>
                    <dt>ID</dt>
                    <dd><?php xecho($clan['id']) ?></dd>

                    <dt>Name</dt>
                    <dd class="App__clanNameParent App__editableField" data-field="name">
                        <span class="fieldDisplay App__clanName outOfEdit"><?php
                            echo html_entity_decode(xssafe($clan['name']));
                        ?></span>
                        <input type="text" class="fieldEdit App__clanNameEdit inEdit hide" value="<?php
                            echo htmlspecialchars($clan['name']);
                        ?>">
                        <button type="button" class="edit outOfEdit">Edit</button>
                        <button type="button" class="save inEdit hide">Save</button>
                        <button type="button" class="cancel inEdit hide">Cancel</button>
                        <span class="status"></span>
                    </dd>

                    <dt>Description</dt>
                    <dd class="App__clanDescParent App__editableField" data-field="desc">
                        <div class="fieldDisplay App__clanDesc outOfEdit"><?php
                            echo html_entity_decode(xssafe($clan['desc']));
                        ?></div>
                        <textarea name="desc" class="fieldEdit App__clanDescEdit inEdit hide"
                            style="position:relative"><?php
                            echo html_entity_decode(xssafe($clan['desc']));
                        ?></textarea>
                        <div class="valign">
                            <div class="outOfEdit">
                                <button type="button" class="edit">Edit</button>
                            </div>
                            <div class="inEdit hide">
                                <button type="button" class="save">Save</button>
                                <button type="button" class="cancel">Cancel</button>
                            </div>
                            <span class="status"></span>
                        </div>
                    </dd>

                    <dt>Link</dt>
                    <dd><a href="<?php xecho($clan['link']) ?>"><?php xecho($clan['link']) ?></a></dd>

                    <dt>Regions</dt>
                    <dd><?php xecho($clan['regions']) ?></dd>

                    <dt>Platforms</dt>
                    <dd><?php xecho($clan['platforms']) ?></dd>

                    <dt>Last Accepted App</dt>
                    <dd><?php
                    // clan[app_last_submission] is deprecated in favor of application[last_submission_time]
                    // but we're keeping it in for backwards compatibility with old applications
                    xecho(timeConvert(
                        from($application, 'last_submission_time') ?:
                        from($clan, 'app_last_submission') ?:
                        'n/a'));
                    ?></dd>

                    <dt>Clan Is Blacklisted</dt>
                    <dd><?php echo $clan['is_blacklisted'] ?
                        '<span class="green">yes</span>' : '<span class="red">no</span>' ?></dd>

                    <dt>Status</dt>
                    <dd>
                        <?php if (!isset($app['accepted'])): ?>
                            <span>Unresolved</span>
                        <?php else:
                            echo $app['accepted'] ? '<span class="green">Accepted</span>' : '<span class="red">Denied</span>';
                            ?>
                            by <b><?php xecho($app['acp_user'] ?? 'n/a'); ?></b>
                            on <b><?php xecho(timeConvert($app['acp_time']) ?? 'n/a'); ?></b>
                        <?php endif; ?>
                    </dd>

                    <dt>Mod Note</dt>
                    <dd><?php xecho($app['acp_reason'] ?? 'n/a'); ?></dd>

                    <dt>Action:</dt>
                    <dd>
                        <div class="spacer5-top">
                            <input type="text" name="reason" placeholder="Custom mod note (optional, only mods see this)" />
                        </div>
                        <button type="submit" name="action" value="accept">Accept</button>
                        <button type="submit" name="action" value="deny">Deny</button>

                        <?php if (b_stash('in_queue')): ?>
                            <button type="submit" name="action" value="dequeue"
                                title="Silently remove from queue without any action (user won't be notified)">
                                <span>Dequeue (?)</span></button>
                        <?php else: ?>
                            <button type="submit" name="action" value="enqueue"
                                title="Silently move back to queue without any action">
                                <span>Enqueue (?)</span></button>
                        <?php endif; ?>
                    </dd>
                </dl>
            </div>
        <?php elseif (stash('app_type') === 'apply'): ?>
            <div class="App__applyInfo">
                <h3>Application</h3>
                <dl>
                    <dt>Platforms</dt>
                    <dd><?php xecho($application['platforms']) ?></dd>

                    <dt>Last Accepted App</dt>
                    <dd><?php xecho(timeConvert($application['last_submission_time']) ?: 'n/a') ?></dd>

                    <dt>Status</dt>
                    <dd>
                        <?php if (!isset($app['accepted'])): ?>
                            <span>Unresolved</span>
                        <?php else:
                            echo $app['accepted'] ? '<span class="green">Accepted</span>' : '<span class="red">Denied</span>';
                            ?>
                            by <b><?php xecho($app['acp_user'] ?? 'n/a'); ?></b>
                            on <b><?php xecho(timeConvert($app['acp_time']) ?? 'n/a'); ?></b>
                        <?php endif; ?>
                    </dd>

                    <dt>Mod Note</dt>
                    <dd><?php xecho($app['acp_reason'] ?? 'n/a'); ?></dd>

                    <dt>Normal Action:</dt>
                    <dd class="spacer10-bottom">
                        <div class="spacer5-top">
                            <input type="text" name="reason" placeholder="Custom mod note (optional, only mods see this)" />
                        </div>
                        <button type="submit" name="action" value="approve">Approve</button>
                        <button type="submit" name="action" value="deny">Deny for Infractions</button>

                        <?php if (b_stash('in_queue')): ?>
                            <button type="submit" name="action" value="dequeue"
                                title="Silently remove from queue without any action (user won't be notified)">
                                <span>Dequeue (?)</span></button>
                        <?php else: ?>
                            <button type="submit" name="action" value="enqueue"
                                title="Silently move back to queue without any action">
                                <span>Enqueue (?)</span></button>
                        <?php endif; ?>
                    </dd>
                    <dt>Custom Action:</dt>
                    <dd>
                        <p style="line-height: 16px;margin-top: 5px;">
                        The "custom mod note" field above will also work for these custom actions.</p>
                        <textarea name="message" class="spacer10-top" style="min-height:210px;position:relative"
                        placeholder='<?php echo
                            'Write your custom message to be DMed by SweeperBot to the Discord User '.
                            'here then click "Approve" or "Deny"' ?>'></textarea>
                        <button type="submit" name="action" value="approve_custom">Approve</button>
                        <button type="submit" name="action" value="deny_custom">Deny</button>
                    </dd>
                </dl>
                <h3>Messages</h3>
                <?php if (!empty($DM_list)): ?>
                <dl>
                    <?php foreach ($DM_list as $msg): ?>
                    <dt><?php xecho(timeConvert($msg['sent_utc'], true)) ?></dt>
                    <dd>
                        <?php echo html_entity_decode(xssafe($msg['body'])) ?><br/>
                        <em>Sent by <strong><?php xecho($msg['acp_user']) ?></strong> on
                        <?php xecho(timeConvert($msg['sent_utc'])) ?></em>
                    </dd>
                    <?php endforeach; ?>
                </dl>
                <?php else: ?>
                <p class="spacer10-left spacer5-top">None</p>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="App__duserInfo">
            <h3>Discord User</h3>
            <dl>
                <dt>ID</dt>
                <dd><?php xecho($d_user['id']) ?></dd>

                <dt>User Ping</dt>
                <dd><?php xecho($d_user['userping']) ?></dd>

                <dt>Bungie Profile</dt>
                <dd><a href="<?php xecho($d_user['bungie_profile']) ?>"><?php
                        xecho($d_user['bungie_profile']) ?></a></dd>

                <dt>User Is Blacklisted</dt>
                <dd><?php echo $d_user['is_blacklisted'] ? '<span class="green">yes</span>'
                    : '<span class="red">no</span>' ?></dd>

                <dt>Activity 14d</dt>
                <dd><?php xecho($d_user['activity_14d']) ?> messages</dd>

                <dt>In DTG SVR? <span style="color:red;font-weight:bold">*</span></dt>
                <dd><?php echo discord_mod()->in_server(DISCORD_GUILD_ID, $d_user['id'])
                    ? '<span class="green">yes</span>' : '<span class="red">no</span>' ?></dd>

                <dt>In DTG Clan SVR? <span style="color:red;font-weight:bold">*</span></dt>
                <dd><?php echo discord_mod()->in_server(DISCORD_CLAN_GUILD_ID, $d_user['id'])
                    ? '<span class="green">yes</span>' : '<span class="red">no</span>' ?></dd>
            </dl>
            <h3>User Actioned</h3>
            <dl>
                <dt>Ban</dt>
                <dd><?php xecho($d_user['actioned']['Ban'] ?? '?') ?></dd>
                <dt>Kick</dt>
                <dd><?php xecho($d_user['actioned']['Kick'] ?? '?') ?></dd>
                <dt>Warn</dt>
                <dd><?php xecho($d_user['actioned']['Warn'] ?? '?') ?></dd>
                <dt>Note</dt>
                <dd><?php xecho($d_user['actioned']['Note'] ?? '?') ?></dd>
                <dt>Mute</dt>
                <dd><?php xecho($d_user['actioned']['Mute'] ?? '?') ?></dd>
                <dt>Total Infractions<br/>(Ban, Kick, Warn, Mute)</dt>
                <dd><?php xecho($d_user['actioned']['Infractions'] ?? '?') ?></dd>
            </dl>
        </div>
    </div>
</div>