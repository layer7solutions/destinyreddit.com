<?php $max_items_amount = \app\lib\clans\ManualQueue::MAX_ITEMS_AMOUNT; ?>
<form class="historyNav fsplit" method="POST" action="/admin/clans/history_update">
    <input type="hidden" name="cont" value="<?php echo u_stash('cont') ?>" />
    <input type="hidden" name="listing_type" value="<?php echo u_stash('listing_type') ?>" />
    <div class="valign">
        <!-- FIRST PAGE BUTTON -->
        <?php if (i_stash('page_number') > 1): ?>
            <a href="?page=1"><button type="button">first</button></a>
        <?php else: ?>
            <span><button type="button" disabled>first</button></span>
        <?php endif; ?>

        <!-- PREVIOUS PAGE BUTTON -->
        <?php if (i_stash('page_number') > 1): ?>
            <a href="?page=<?php echo (i_stash('page_number') - 1) ?>"><button type="button">prev</button></a>
        <?php else: ?>
            <span><button type="button" disabled>prev</button></span>
        <?php endif; ?>

        <!-- NEXT PAGE BUTTON -->
        <?php if (i_stash('page_number') < i_stash('page_count')): ?>
            <a href="?page=<?php echo (i_stash('page_number') + 1) ?>"><button type="button">next</button></a>
        <?php else: ?>
            <span><button type="button" disabled>next</button></span>
        <?php endif; ?>

        <!-- PAGE JUMP FORM -->
        <p class="spacer5-left">
            <span>Page </span>
            <input type="number" name="page" min="1" max="<?php echo x_stash('page_count') ?>" value="<?php echo x_stash('page_number') ?>" />
            <span> of <?php echo x_stash('page_count') ?></span>
        </p>
        <button class="submit" name="action" value="page_jump">jump</button>
    </div>
    <div class="valign">
        <span class="spacer5-right">Items per page:</span>
        <input type="number" name="amount" min="1" max="<?php xecho($max_items_amount) ?>" value="<?php echo i_stash('amount') ?>" />
        <button type="submit" name="action" value="history_amount">Update</button>
        <button type="submit" name="action" value="history_amount_reset">Reset</button>
    </div>
</form>