<?php
namespace app\lib\util;

use app\lib\clans\Application;

abstract class Message {
    public $to;
    public $body;
    public $sent_by;

    /**
     * Construct a new message
     *
     * @param string|int $to where/who to send message to (username, user id, channel id, conversation id, ...)
     * @param string $body the message body
     * @param string|int $sent_by who sent the message
     */
    public function __construct($to, string $body, $sent_by = null) {
        $this->to = $to;
        $this->body = $body;
        $this->sent_by = $sent_by;
    }

    public abstract function send(Application $app = null);

}