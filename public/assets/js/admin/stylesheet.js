init('StylesheetACP', function(opts) {
    var module_names = opts.module_names;
    var module_changes = {};
    forEach(Object.keys(module_names), function(pkg_name) {
        module_changes[pkg_name] = {};
    });

    // COLLAPSE TRIGGER
    // --------------------------------------------------------------------------------
    var collapse_speed = 100; // animation time (ms)

    function init_collapse_trigger(element) {
        (function() {
            // handle initial state
            var trigger_element = element;
            var collapsable_element = n.closest(trigger_element, '.collapsable');
            var collapsable_id = collapsable_element.getAttribute('id');

            if (!collapsable_element) return;

            if (collapsable_id && true === localStorage.getObject('collapse_state.'+collapsable_id)) {
                n.addClass(collapsable_element, 'collapsed');
            }

            if (n.hasClass(collapsable_element, 'collapsed')) {
                // initial state: collapsed
                collapsable_element.style.height = trigger_element.getBoundingClientRect().height + 'px';
                n.addClass(trigger_element, 'collapsed');
                n.addClass(collapsable_element, 'collapsable-initialized');

                setTimeout(function() {
                    var collapsable_height = 0;
                    for (var j = 0; j < collapsable_element.children.length; j++) {
                        collapsable_height += collapsable_element.children[j].getBoundingClientRect().height;
                    }
                    collapsable_element.setAttribute('data-initial-height', parseInt(collapsable_height)+'px');
                }, 5);
            } else {
                // initial state: uncollapsed
                n.addClass(collapsable_element, 'uncollapsed');
                n.addClass(trigger_element, 'uncollapsed');
                n.addClass(collapsable_element, 'collapsable-initialized');
            }
        })();

        n.on(element, 'click', function(event) {
            var trigger_element = this;
            var collapsable_element = n.closest(trigger_element, '.collapsable');
            var collapsable_id = collapsable_element.getAttribute('id');

            if (!collapsable_element ||
                    n.hasClass(collapsable_element, 'collapsing') ||
                    n.hasClass(collapsable_element, 'uncollapsing')) {
                return;
            }

            function change_state(add_class_name, remove_class_name) {
                n.addClass(collapsable_element, add_class_name);
                n.addClass(trigger_element, add_class_name);

                n.removeClass(collapsable_element, remove_class_name);
                n.removeClass(trigger_element, remove_class_name);
            }

            if (n.hasClass(collapsable_element, 'collapsed')) {
                // uncollapse
                var collapsable_height = collapsable_element.getAttribute('data-initial-height');

                change_state('uncollapsing', 'collapsed');
                Velocity(collapsable_element, { height: collapsable_height }, collapse_speed).then(function() {
                    change_state('uncollapsed', 'uncollapsing');
                    change_state('uncollapsed', 'uncollapsing');
                    collapsable_element.style.height = 'auto';
                });

                if (collapsable_id) {
                    localStorage.setObject('collapse_state.'+collapsable_id, false);
                }
            } else {
                // collapse
                var trigger_height = trigger_element.getBoundingClientRect().height;
                var collapsable_height = collapsable_element.getBoundingClientRect().height;

                collapsable_element.setAttribute('data-initial-height', parseInt(collapsable_height)+'px');
                change_state('collapsing', 'uncollapsed');

                Velocity(collapsable_element, { height: trigger_height }, collapse_speed).then(function() {
                    change_state('collapsed', 'collapsing');
                    change_state('collapsed', 'collapsing');
                });

                if (collapsable_id) {
                    localStorage.setObject('collapse_state.'+collapsable_id, true);
                }
            }
        });
    }

    var collapse_triggers = document.querySelectorAll('.collapse-trigger');
    for (var i = 0; i < collapse_triggers.length; i++) {
        init_collapse_trigger(collapse_triggers[i]);
    }

    // MODULE HANDLERS
    // --------------------------------------------------------------------------------
    function show_module_notice(module_el, notice_type) {
        var notices_el = module_el.querySelector('.module_notice');
        var notice_item = module_el.querySelector('.module_notice--'+notice_type);
        if (!notice_item) return;

        n.removeClass(notices_el, 'hide');
        n.removeClass(notice_item, 'hide');
    }
    function hide_module_notice(module_el, notice_type) {
        var notices_el = module_el.querySelector('.module_notice');
        var notice_item = module_el.querySelector('.module_notice--'+notice_type);
        if (!notice_item) return;

        n.addClass(notice_item, 'hide');
        if (notices_el.querySelectorAll('.hide').length == notices_el.children.length) {
            n.addClass(notices_el, 'hide');
        }
    }

    function module_name(element) {
        if (!n.hasClass(element, 'module')) {
            element = n.closest(element, '.module');
        }

        if (element) {
            return element.getAttribute('data-module');
        }
        return null;
    }

    function num_changes(pkg_name, mod_name) {
        if (n.isElement(mod_name)) {
            mod_name = module_name(mod_name);
        }
        if (!isset(module_changes[pkg_name])) {
            module_changes[pkg_name] = {};
        }
        return Object.keys(module_changes[pkg_name]).length;
    }
    function remove_change(pkg_name, mod_name, property) {
        if (n.isElement(mod_name)) {
            mod_name = module_name(mod_name);
        }
        if (!isset(module_changes[pkg_name])) {
            module_changes[pkg_name] = {};
        }
        if (!isset(module_changes[pkg_name][mod_name])) {
            module_changes[pkg_name][mod_name] = {};
        }
        delete module_changes[pkg_name][mod_name][property];
    }
    function set_change(pkg_name, mod_name, property, value) {
        if (n.isElement(mod_name)) {
            mod_name = module_name(mod_name);
        }
        if (!isset(module_changes[pkg_name])) {
            module_changes[pkg_name] = {};
        }
        if (!isset(module_changes[pkg_name][mod_name])) {
            module_changes[pkg_name][mod_name] = {};
        }
        module_changes[pkg_name][mod_name][property] = value;
    }
    function get_change(pkg_name, mod_name, property, value) {
        if (n.isElement(mod_name)) {
            mod_name = module_name(mod_name);
        }
        if (!isset(module_changes[pkg_name])) {
            module_changes[pkg_name] = {};
        }
        if (!isset(module_changes[pkg_name][mod_name])) {
            module_changes[pkg_name][mod_name] = {};
        }
        return module_changes[pkg_name][mod_name][property];
    }
    function set_deleted(pkg_name, mod_name, state) {
        if (state) {
            set_change(pkg_name, mod_name, 'deleted', true);
        } else {
            remove_change(pkg_name, mod_name, 'deleted');
        }
    }

    function init_module(module_el, is_new) {
        var pkg_name = module_el.getAttribute('data-package');
        module_changes[pkg_name][module_name(module_el)] = {};
        n.addClass(module_el, 'module-initialized');

        is_new = is_new || false;

        // DELETE TOOL
        // ~~~~~~~~~~~
        var delete_button = module_el.querySelector('.module_tool__deleteButton');
        var delete_undo = module_el.querySelector('.module_tool__deleteUndo');

        n.on(delete_button, 'click', function() {
            var module_el = n.closest(this, '.module');

            var pkg_name = module_el.getAttribute('data-package');
            var mod_name = module_name(module_el);

            set_deleted(pkg_name, mod_name, true);
            n.addClass(module_el, 'is--deleted');

            if (get_change(pkg_name, mod_name, 'new_module') === true) {
                // completely remove only if new, unsaved module
                n.remove(module_el);
                delete module_changes[pkg_name][mod_name];
                module_names[pkg_name] = filter(module_names[pkg_name], function(item) {
                    return item != mod_name;
                });
            }
        });
        n.on(delete_undo, 'click', function() {
            var module_el = n.closest(this, '.module');
            var pkg_name = module_el.getAttribute('data-package');

            set_deleted(pkg_name, module_el, false);
            n.removeClass(module_el, 'is--deleted');
        });

        // QUIT HERE IF CORE MODULE
        // --------------------------------------------------------------------------------
        if (n.hasClass(module_el, 'core_module')) {
            return; // done for core modules
        }

        // ENABLED CHECKBOX
        // ~~~~~~~~~~~~~~~~
        var enabled_input = module_el.querySelector('input[type=checkbox][name="enabled"]');
        n.on(enabled_input, 'change', function(event) {
            var original_state = this.getAttribute('data-original-state') == "1";
            var new_state = this.checked;
            var mod_name = module_name(this);

            set_change(pkg_name, module_el, 'enabled', new_state);
            show_module_notice(module_el, 'unsaved');
        });

        // MODULE NAME INPUT
        // ~~~~~~~~~~~~~~~~~
        var name_input = module_el.querySelector('input[type=text][name="mod_name"]');

        function adjust_name_input_width() {
            var new_width = ((name_input.value.length + 1) * 7);
            if (new_width < 162) {
                new_width = 162;
            }
            name_input.style.width = new_width + 'px';
        }

        adjust_name_input_width();

        n.on(name_input, 'keyup', function(event) {
            adjust_name_input_width();

            if (contains(module_names[pkg_name], this.value) && module_name(module_el) != this.value) {
                show_module_notice(module_el, 'name_conflict');
                n.addClass(this, 'is--name_conflict');
            } else {
                hide_module_notice(module_el, 'name_conflict');
                n.removeClass(this, 'is--name_conflict');
            }
        });
        n.on(name_input, 'change', function(event) {
            if (n.hasClass(this, 'is--name_conflict')) {
                n.removeClass(this, 'is--name_conflict');
                this.value = module_name(module_el);
                hide_module_notice(module_el, 'name_conflict');
                return;
            }

            var new_name = this.value;
            var old_name = module_name(module_el);
            var my_index = module_names[pkg_name].indexOf(old_name);

            module_names[pkg_name][my_index] = new_name;
            module_changes[pkg_name][new_name] = module_changes[pkg_name][old_name];
            delete module_changes[pkg_name][old_name];
            module_el.setAttribute('data-module', new_name);

            show_module_notice(module_el, 'unsaved');
        });

        // STYLES TEXTAREA
        // ~~~~~~~~~~~~~~~
        var style_input = module_el.querySelector('textarea[name="styles"]');
        n.on(style_input, 'change', function(event) {
            show_module_notice(module_el, 'unsaved');
            set_change(pkg_name, module_el, 'styles', this.value);
        });

        // ORDER TOOLS
        // ~~~~~~~~~~~

        var move_up = module_el.querySelector('.module_movePriorityUp');
        var move_down = module_el.querySelector('.module_movePriorityDown');

        n.on(move_up, 'click', function(event) {
            var mod_name = module_name(this);

            var my_index = module_names[pkg_name].indexOf(mod_name);
            var target_index = my_index - 1;
            var upper_limit = module_names[pkg_name][0];

            if (target_index == upper_limit) {
                return;
            }

            n.insertBefore(module_el, module_el.previousElementSibling);

            module_names[pkg_name][my_index] = module_names[pkg_name][target_index];
            module_names[pkg_name][target_index] = mod_name;

            n.removeClass(module_el, 'flash');
            n.addClass(module_el, 'flash');
        });
        n.on(move_down, 'click', function(event) {
            var mod_name = module_name(this);

            var my_index = module_names[pkg_name].indexOf(mod_name);
            var target_index = my_index + 1;
            var lower_limit = module_names[pkg_name].length;

            if (target_index == lower_limit) {
                return;
            }

            n.insertAfter(module_el, module_el.nextElementSibling);

            module_names[pkg_name][my_index] = module_names[pkg_name][target_index];
            module_names[pkg_name][target_index] = mod_name;

            n.removeClass(module_el, 'flash');
            n.addClass(module_el, 'flash');
        });

        // IF NEW MODULE
        // ~~~~~~~~~~~~~
        if (is_new) {
            show_module_notice(module_el, 'unsaved');
            set_change(pkg_name, module_el, 'new_module', true);
        }
    }

    // NEW MODULE CREATION
    // --------------------------------------------------------------------------------
    n.on('.new_user_module_button', 'click', function() {
        var pkg_name = this.getAttribute('data-pkg-name');
        var new_module_name = "New module";
        var new_module_name_i = 0;

        if (!module_names[pkg_name]) {
            module_names[pkg_name] = [];
        }
        while (contains(module_names[pkg_name], new_module_name)) {
            new_module_name = "New module (" + (++new_module_name_i) + ")";
        }

        var parent = n.closest(this, '.new_user_module_area');
        r('/admin/stylesheet/new_module', {
            data: {
                'pkg_name': pkg_name,
                'mod_name': new_module_name,
                'enabled':  true,
                'styles':   '',
                'mod_type': 'user_module'
            },
            success: function(result) {
                n.insertBefore(n.frag(result), parent);
                module_names[pkg_name].push(new_module_name);

                setTimeout(function() {
                    var new_module_el = n.select(
                        '.user_modules .module.collapsable:not(.collapsable-initialized)');
                    if (new_module_el.length) {
                        init_module(new_module_el[0], true);
                    }

                    var trigger_el = n.select(
                        '.user_modules .module.collapsable:not(.collapsable-initialized) .collapse-trigger');
                    if (trigger_el.length) {
                        init_collapse_trigger(trigger_el[0]);
                    }
                }, 0);
            },
        });
    });

    // SAVE PACKAGE
    // --------------------------------------------------------------------------------
    n.on('.deploy_button, .save_button', 'click', function() {
        var pkg_name = this.getAttribute('data-pkg-name');
        var package_el = n.closest(this, 'article');
        var order = module_names[pkg_name].slice(module_names[pkg_name][0] + 1);

        var button_el = this;
        if (button_el.hasAttribute('disabled')) {
            return;
        }

        r('/admin/stylesheet/deploy_package', {
            data: {
                'package':   pkg_name,
                'changes':   JSON.stringify(module_changes[pkg_name]),
                'order':     JSON.stringify(order),
                'save_only': n.hasClass(this, 'save_button'),
            },
            start: function() {
                button_el.setAttribute('disabled', 'disabled');
            },
            success: function(result) {
                // ----- free button
                button_el.removeAttribute('disabled');

                // ----- remove local change indicators
                var user_modules =
                    package_el.querySelectorAll('.user_modules .module[data-package="'+pkg_name+'"]');
                forEach(user_modules, function(module_el) {
                    hide_module_notice(module_el, 'unsaved');
                    if (n.hasClass(module_el, 'is--deleted')) {
                        n.remove(module_el);
                    }
                });
                module_changes[pkg_name] = {};

                // ----- show status label
                var status_selector = ".deploy_status--" + result['op'];
                status_selector += '.' + (result['success'] ? 'success' : 'failure');

                n.select(package_el, '.deploy_status').addClass('hide');

                var status_item = package_el.querySelector(status_selector);
                n.removeClass(status_item, 'hide');

                setTimeout(function() {
                    Velocity(status_item, { opacity: 0 }, 500).then(function() {
                        n.addClass(status_item, 'hide');
                        status_item.style.opacity = '';
                    });
                }, 3000)
            },
            error: function(errcode, errmsg) {
                alert('An error occured ('+errcode+'): ' + errmsg);
            }
        });
    });

    // INIT CURRENT MODULES
    // --------------------------------------------------------------------------------
    var all_modules = document.querySelectorAll('.module:not(.base_module)');
    forEach(all_modules, function(item) {
        init_module(item, false);
    });
});