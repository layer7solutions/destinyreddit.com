<?php
namespace app\controllers\admin;

use app\lib\RedditMod;
use app\lib\flair\FlairManager;
use app\lib\flair\FlairConfig;
use app\lib\StylesheetCTL;

use app\lib\clans\forms\RecruitForm;
use app\lib\clans\ManualQueue;
use app\lib\clans\Blacklist;

use app\controllers\HomepageController;

class AdminMiscEndpointController extends \Controller {

    public function __construct() {
        $this->middleware(function() {
            if (!reddit_user()->is_admin() && !discord_user()->is_admin()) {
                return $this->error(403);
            }
        });
    }

    protected function default_action(array $params) {
        if (reddit_user()->is_admin()) {
            switch ($params['page']) {
                case 'get_stylesheet_images':
                    dispatch_json(FlairManager::get_stylesheet_images());
                    exit;
                case 'get_stylesheet_image_urls':
                    dispatch_json(FlairManager::get_stylesheet_image_urls());
                    exit;
                case 'force_stylesheet_images_cache_expiry':
                    FlairManager::force_stylesheet_images_cache_expiry();
                    echo "Done.";
                    exit;
                case 'force_tweet_cache_expiry':
                    HomepageController::expire_and_fetch_tweets();
                    echo "Done".
                    exit;
                case 'create_css':
                    header('Content-Type: text/css');
                    echo FlairConfig::create_css(
                            $_REQUEST['for_display'] ?? false, $_REQUEST['use_scale'] ?? false);
                    exit;
                case "package_css":
                    if (!isset($_REQUEST['package']))
                        die("Invalid request");
                    header('Content-Type: text/css');
                    echo StylesheetCTL::deploy($_REQUEST['package'], true);
                    exit;
                case "page500":
                    invoke_500_error_page();
            }
        }

        return false;
    }

}