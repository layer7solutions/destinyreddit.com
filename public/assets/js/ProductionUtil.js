if(!Array.prototype.extend){Array.prototype.extend=function(){if(arguments.length==1&&Object.prototype.toString.call(arguments[0])==='[object Array]'){arguments=arguments[0];}
for(var i=0;i<arguments.length;i++){this.push(arguments[i]);}
return this;}}
if(!String.prototype.format){String.prototype.format=function(){var args=arguments,d={};for(var i=0,j=0;i<args.length;i++){var o=args[i];if(o!==null&&typeof o==='object'&&Object.prototype.toString.call(o)!=='[object Array]'){var keys=Object.keys(o);for(var k=0,len=keys.length;k<len;k++){d[keys[k]]=o[keys[k]];}}
else{d[j++]=o;}}
var i=0;return this.replace(/\{([A-z0-9]*)\}/g,function(match,key){key=key||i++;return typeof d[key]!=='undefined'?d[key]:match;});};}
if(!Storage.prototype.setObject){Storage.prototype.setObject=function(key,value,expires){this.setItem(key,JSON.stringify({value:value,expires:expires&&(Date.now()/1000|0)+expires}));}}
if(!Storage.prototype.getObject){Storage.prototype.getObject=function(key){var stringified=this.getItem(key),data=stringified&&JSON.parse(stringified),expires=(data?data.expires:null)||Infinity;if(!data)return undefined;return(Date.now()/1000|0)>=expires?undefined:data.value;}}
if(!Storage.prototype.removeObject){Storage.prototype.removeObject=function(key){this.removeItem(key);}};function is_array(o){return(Object.prototype.toString.call(o)==='[object Array]');}
function is_array_like(o){if(is_array(o))return true;if(!isset(o)||is_string(o))return false;if(typeof o!=='object')return false;if(n.isNode(o))return false;if(typeof o.length!=='number')return false;if(o.length<0)return false;if(o.length>0)return o.hasOwnProperty(0);return true;}
function array_equals(a,b){if(a===b)return true;if(a==null||b==null)return false;if(a.length!=b.length)return false;for(var i=0;i<a.length;++i){if(a[i]!==b[i])return false;}
return true;}
function to_array(o){if(typeof o==='string'){return[o];}
return Array.prototype.slice.call(o);}
function in_array(array,item){if(is_array(array)&&typeof Array.prototype.indexOf==='function'){return array.indexOf(item)>-1;}else if(is_array_like(array)){for(var i=0;i<array.length;i++){if(array[i]===item){return true;}}}
return false;}
function any(a,predicate){if(!isset(a)||a.length==0)return false;return!is_undefined(first(a,predicate||isset));}
function every(a,predicate){if(!isset(a)||a.length==0)return false;return filter(a,predicate||isset).length===a.length;}
function first(a,predicate){if(!isset(a))return undefined;if(!isset(predicate)){return a[0];}
for(var i=0;i<a.length;i++){if(predicate(a[i])){return a[i];}}
return undefined;}
function last(a,predicate){if(!isset(a))return undefined;if(!isset(predicate)){return a[a.length-1];}
for(var i=a.length-1;i>=0;i--){if(predicate(a[i])){return a[i];}}
return undefined;}
function intersect(a1,a2){var a3=[];forEach(a1,function(itm){if(contains(a2,itm)){a3.push(itm);}});return a3;}
function diff(a1,a2){var a3=[];forEach(a1,function(itm){if(!contains(a2,itm)){a3.push(itm);}});return a3;}
function union(a1,a2){var a3=[];var arr=a1.concat(a2),len=arr.length;while(len--){var itm=arr[len];if(a3.indexOf(itm)===-1){a3.unshift(itm);}}
return a3;}
function flatten(){function f(args){var ret=[];forEach(args,function(item){if(is_array(item)){ret.extend(f(item));}else{ret.push(item);}});return ret;}
return f(arguments);};function awaitElement(selector,callback){function try_await(){var element=document.querySelector(selector);if(!element){window.requestAnimationFrame(try_await);}else{callback(element);}}
try_await();}
function awaitElements(selector,callback){function try_await(){var elements=document.querySelectorAll(selector);if(!elements.length){window.requestAnimationFrame(try_await);}else{callback(elements);}}
try_await();}
function extract_domain(url){var domain;if(url.indexOf("://")>-1){domain=url.split('/')[2];}else{domain=url.split('/')[0];}
domain=domain.split(':')[0];return domain;}
function add_protocol(URL,protocol){protocol=rtrim(protocol||'https',':/');var
parts=URL.split(/\/\/(.*)/),URN=(parts.length==1)?parts[0]:parts[1];return protocol+'://'+URN;}
function get_hash(){return location.hash.substr(1);}
function query_param(name,url){if(!url){url=window.location.href;}
name=name.replace(/[\[\]]/g,"\\$&");var regex=new RegExp("(?:^|[?&])"+name+"(=([^&#]*)|&|#|$)"),results=regex.exec(url);if(!results)return null;if(!results[2])return'';return decodeURIComponent(results[2].replace(/\+/g," "));}
function is_mobile(){return!!(navigator.userAgent.match(/Android/i)||navigator.userAgent.match(/webOS/i)||navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPod/i)||navigator.userAgent.match(/Opera Mini/i)||navigator.userAgent.match(/BlackBerry/i)||navigator.userAgent.match(/IEMobile/i)||navigator.userAgent.match(/WPDesktop/i)||navigator.userAgent.match(/Windows Phone/i));};function timestamp(){return(Date.now()/1000|0);}
function min2sec(min){return min*60;}
function sec2min(sec){return sec/60;}
function timeConvert(UNIX_timestamp,no_time){if(empty(UNIX_timestamp)){return String(UNIX_timestamp);}
var a;if(UNIX_timestamp instanceof Date){a=moment(UNIX_timestamp);}else{a=moment(UNIX_timestamp*1000);}
var format=no_time?'MMM DD YYYY':'MMM DD YYYY hh:mm:ss a';if(CURRENT_USER){var ret=a.utcOffset(USER_TZ_OFFSET).format(format);if(!no_time){ret+=" "+USER_TZ_ABRV;}
return ret;}else{return a.format(format)}}
function human_timing(time,suffix){suffix=suffix||null;if(time instanceof Date)
time=time/1000|0;if(time===null)
return null;if(time<=0)
return'never';time=Math.floor(Date.now()/1000)-time;suffix=isset(suffix)?suffix:(time<0?'from now':'ago');time=Math.abs(time);if(time<=1)
return'Just now';var tokens=[[31536000,'year'],[2592000,'month'],[604800,'week'],[86400,'day'],[3600,'hour'],[60,'minute'],[1,'second']];var ret=null;forEach(tokens,function(token){var unit=token[0];var text=token[1];if(time<unit)
return;var numberOfUnits=Math.floor(time/unit);ret=(numberOfUnits+' '+text+((numberOfUnits>1)?'s':''))+' '+suffix;return false;});return ret;};function is_int(value){var x;if(isNaN(value)){return false;}
x=parseFloat(value);return(x|0)===x;}
function is_function(o){return typeof o==='function';}
function is_null(o){return o===null;}
function is_undefined(o){return typeof o==='undefined';}
function is_undef(o){return typeof o==='undefined';}
function is_bool(o){return o===true||o===false||o.toString()==='[object Boolean]';}
function bool(o){return!!o;}
function is_truthy(o){return!!o===true;}
function is_falsey(o){return!!o===false;}
function is_promise(o){return o&&(Promise.resolve(o)===o||(o instanceof Promise)||Object.prototype.toString.call(o)==="[object Promise]"||(typeof o.then==='function'));};function is_object(o){return o!==null&&typeof o==='object';}
function empty(o){if(!isset(o)){return true;}
if(n.isNode(o)){return false;}
if(is_object(o)){if(o.constructor===Object&&Object.keys(o).length===0){return true;}}
if(o.hasOwnProperty('length')&&o.length===0){return true;}
return o==false;}
function not_empty(o){return!empty(o);}
function isset(o){var a=arguments,l=a.length,i=0,undef;if(l===0){return false;}
while(i!==l){if(a[i]===undef||a[i]===null){return false;}
i++;}
return true;}
function not_set(){return!isset.apply(null,arguments);}
function forEach(obj,callback){if(is_string(obj)){obj=document.querySelectorAll(obj);}
if(is_object(obj)){if('length'in obj&&(obj.length!==0&&'0'in obj)){if(obj.length==1){callback(obj[0],0,1);return;}
for(var i=0;i<obj.length;i++){var ret=callback(obj[i],i,obj.length);if(ret===false){break;}}}else{var keys=Object.keys(obj);for(var i=0,len=keys.length;i<len;i++){if(callback(keys[i],obj[keys[i]],i)===false){break;}}}}}
function map(o,fn){if(!isset(o)){return[];}else if(is_array_like(o)){var ret=[];forEach(o,function(v){ret.push(fn(v));});return ret;}else if(is_object(o)){var ret={};forEach(o,function(k,v){ret[k]=fn(k,v);});return ret;}else{return fn(o);}}
function filter(o,predicate){if(not_set(predicate)){predicate=isset;}
var pred_is_func=is_function(predicate);if(!isset(o)){return[];}else if(is_array_like(o)){var ret=[];forEach(o,function(v){if(pred_is_func?predicate(v)===true:predicate===v)
ret.push(v);});return ret;}else if(is_object(o)){var ret={};forEach(o,function(k,v){if(pred_is_func?predicate(k,v)===true:predicate===v)
ret[k]=v;});return ret;}else{return[];}}
function reject(o,predicate){if(not_set(predicate)){predicate=not_set;}
var pred_is_func=is_function(predicate);if(!isset(o)){return[];}else if(is_array_like(o)){var ret=[];forEach(o,function(v){if(pred_is_func?predicate(v)!==true:predicate!==v)
ret.push(v);});return ret;}else if(is_object(o)){var ret={};forEach(o,function(k,v){if(pred_is_func?predicate(k,v)!==true:predicate!==v)
ret[k]=v;});return ret;}else{return[];}}
function find(o,predicate){if(not_set(predicate)){predicate=isset;}
var pred_is_func=is_function(predicate);var ret=undefined;if(!isset(o)){return ret;}else if(is_array_like(o)){forEach(o,function(v){if(pred_is_func?predicate(v)===true:predicate===v){ret=v;return false;}});}else if(is_object(o)){forEach(o,function(k,v){if(pred_is_func?predicate(k,v)===true:predicate===v){ret=k;return false;}});}
return ret;}
function contains(haystack,needle){if(typeof haystack==='undefined'||haystack==null){return false;}
if(is_string(haystack)){return haystack.indexOf(needle)>-1;}
else if(is_array_like(haystack)){return in_array(haystack,needle);}
else if(n.isNodeList(haystack)&&n.isNode(needle)){for(var i=0;i<haystack.length;i++){if(needle==haystack[i]){return true;}}
return false;}
else if(n.isNode(haystack)&&n.isNode(needle)){return haystack.contains(needle);}
else if(is_object(haystack)){return haystack.hasOwnProperty(needle);}
return false;}
function reduce(obj,callback,memo){var acc=typeof memo!=='undefined'?memo:(is_array(obj)?obj.shift():{});forEach(obj,function(){var res=callback.apply(null,[acc].concat([].slice.call(arguments)));if(!is_undef(res)){acc=res;}});return acc;}
function reduceToString(obj,callback){return reduce(obj,callback,'');}
function reduceToArray(obj,callback){return reduce(obj,callback,[]);}
function reduceToMap(obj,callback){return reduce(obj,callback,{});}
function copy(o){var to;if(n.isNode(o)){if(n.clone){to=n.clone(o);}else{to=o.cloneNode(true);}}else if(is_object(o)){if(o.constructor!=Object&&o.constructor!=Array)return o;if(o.constructor==Date||o.constructor==RegExp||o.constructor==Function||o.constructor==String||o.constructor==Number||o.constructor==Boolean)
return new o.constructor(o);to=new o.constructor();for(var name in o){to[name]=typeof(to[name])==='undefined'?copy(o[name]):to[name];}}else{to=o;}
return to;}
function extend(obj,other){if(is_array(obj)){obj.extend(other);}else if(is_object(obj)||is_function(obj)){for(var k in other){if(other.hasOwnProperty(k)){obj[k]=other[k];}}}
return obj;};function is_string(obj){return typeof obj==='string'||obj instanceof String;}
function startsWith(str,needle){return str.length>=needle.length&&str.substring(0,needle.length)===needle;}
function endsWith(str,needle){return str.length>=needle.length&&str.substring(str.length-needle.length)===needle;}
function replace_prefix(str,prefix,replacement){replacement=replacement||'';if(str.slice(0,prefix.length)==prefix){str=replacement+str.slice(prefix.length);}
return str;}
function remove_prefix(str,prefix){return replace_prefix(str,prefix);}
function replace_suffix(str,suffix,replacement){replacement=replacement||'';if(str.slice(-suffix.length)==suffix){str=str.slice(0,-suffix.length)+replacement;}
return str;}
function remove_suffix(str,suffix){return replace_suffix(str,suffix);}
function to_lower(o){return map(o,function(v){return is_string(v)?v.toLowerCase():v;});}
function to_upper(o){return map(o,function(v){return is_string(v)?v.toUpperCase():v;});}
function split(o,ch,keep_empty){if(is_array(ch)){ch=new RegExp(map(ch,escape_regex).join('|'));}
var ret=o.split(ch||' ');return keep_empty?ret:filter(ret,function(v){if(v.length==0)return false;});}
function escape_html(unsafe){var text=document.createTextNode(unsafe);var div=document.createElement('div');div.appendChild(text);return div.innerHTML;}
function escape_regex(string){return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g,"\\$1");}
function validate_regex(pattern,options){try{new RegExp(pattern,options||'');return true;}catch(e){return false;}}
function string_replace(str,find,replace){function replace_fn(find,replace){str=str.replace(new RegExp(escape_regex(find),'g'),replace);return str;}
if(arguments.length!=1){return replace_fn(find,replace);}else{return{replace:function(find,replace){do{str=replace_fn(find,replace);}while(contains(str,find));return this;},trim:function(chmask){str=trim(str,chmask);return this;},replaceNewLines:function(replace){this.replace("\r\n","\n").replace("\n\r","\n").replace("\r","\n").replace("\n",replace);return this;},get:function(){return str;},set:function(new_string){str=new_string;return this;},toString:function(){return str;}};}}
function string_insert(str,idx,insert,remove){return str.slice(0,idx)+insert+str.slice(idx+(Math.abs(remove)||0));}
function linecount(str){return str.split(/\r*\n/).length;}
function ucfirst(str){return str.charAt(0).toUpperCase()+str.slice(1);}
function ucwords(str){return map(split(str,' ',true),ucfirst).join(' ');}
function trim(str,char_mask,mode){if(typeof str!=='string'){str+='';}
var l=str.length,i=0;if(!l)return'';if(char_mask){char_mask=char_mask+'';if(!char_mask.length)return str;}else{char_mask=trim.whitespace;}
mode=mode||(1|2);if(mode&1){for(i=0;i<l;i++){if(char_mask.indexOf(str.charAt(i))===-1){str=str.substring(i);break;}}
if(i==l)return'';}
if(mode&2){for(i=l-1;i>=0;i--){if(char_mask.indexOf(str.charAt(i))===-1){str=str.substring(0,i+1);break;}}
if(i==-1)return'';}
return str;}
trim.whitespace=[' ','\n','\r','\t','\f','\x0b','\xa0','\u2000','\u2001','\u2002','\u2003','\u2004','\u2005','\u2006','\u2007','\u2008','\u2009','\u200a','\u200b','\u2028','\u2029','\u3000'].join('');function ltrim(str,char_mask){return trim(str,char_mask,1);}
function rtrim(str,char_mask){return trim(str,char_mask,2);};function Counter(){return{map:{},add:function(key){if(this.map.hasOwnProperty(key)){this.map[key]=this.map[key]+1;}else{this.map[key]=1;}},subtract:function(key){if(this.map.hasOwnProperty(key)){this.map[key]=Math.max(this.map[key]-1,0);}else{this.map[key]=1;}},set:function(key,value){this.map[key]=parseInt(value||1);},get:function(key){return this.map[key];},length:function(){return Object.keys(this.map).length;},};}
function Dict(obj,pass){obj=obj||{};return{put:function(key,value){obj[key]=value;},get:function(key){return obj[key];},remove:function(key){delete obj[key];},keys:function(){return Object.keys(obj);},values:function(){return Object.values(obj);},length:function(){return Object.keys(obj).length;},has:function(key){return obj.hasOwnProperty(key);},containsAll:function(keys){for(var i=0,len=keys.length;i<len;i++){if(!this.contains(keys[i])){return false;}}
return true;},contains:function(key){return key in obj;},forEach:function(fn){var keys=Object.keys(obj);for(var i=0,len=keys.length;i<len;i++){if(fn(keys[i],obj[keys[i]],i,pass)===false){break;}}}};};var n=function(){return n.create.apply(n,arguments);};n.find=function(){var el,selector;if(arguments.length==1){el=document;selector=arguments[0];}else if(arguments.length==2){el=arguments[0];selector=arguments[1];}else{return false;}
if(/(^\s*|,\s*)>/.test(selector)){var id,removeId=false;if(!el.hasAttribute('id')){el.setAttribute('id',id='ID_'+new Date().getTime());removeId=true;}else{id=el.getAttribute('id');}
selector=selector.replace(/(^\s*|,\s*)>/g,'$1#'+id+' >');var result=document.querySelectorAll(selector);if(removeId){el.removeAttribute('id');}
return result;}else{return el.querySelectorAll(selector);}};n.select=function(){var elements;var o=null,source=document;if(arguments.length==1){o=arguments[0];}else if(arguments.length==2){source=arguments[0];o=arguments[1];}else{return false;}
if(is_string(o)&&o.length){try{elements=n.find(source,o);}catch(ex){return false;}}else if(n.isNode(o)){elements=[o];}else if(is_array_like(o)){elements=o;}else{return false;}
var self={_back:null,_select:function(){var x=n.select.apply(null,arguments);x._back=this;return x;},back:function(amount){amount=amount||1;if(amount==1){return this._back;}
var x=this;do{x=x._back;}while(x!==null&&amount-->0);return x;},then:function(fn){if(!fn){return this._select(document.body);}
fn.apply(null,flatten(to_array(arguments).slice(1)));return this;},select:function(selector){if(is_string(selector)){try{var result=n.find(elements[0],selector);if(elements.length>1){for(var i=1;i<elements.length;i++){var nextResult=n.find(elements[i],selector);result=n.mergeNodeList(result,nextResult);}}
return this._select(result);}catch(ex){return false;}}else if(n.isNode(selector)||is_array_like(selector)){return this._select(selector);}else if(is_int(selector)&&selector>=0&&selector<elements.length){return this._select(elements[selector]);}else{return false;}},forEach:function(callback){forEach(elements,callback);return self;},filter:function(callback){var new_list=[];forEach(elements,function(element,i,len){if(callback(element,i,len)){new_list.push(element);}});return this._select(new_list);},map:function(callback){var results=[];forEach(elements,function(element,i,len){var ret=callback(element,i,len);if(ret!==undefined)
results.push(ret);});return results;},sort:function(compareFunction){if(!isset(compareFunction)){return self;}
elements=Array.prototype.slice.call(elements);elements.sort(compareFunction);return self;},reduce:function(callback,initial_value){if(elements.length==0){return initial_value||null;}
var acc=initial_value,skip_first=false;if(!isset(acc)){acc=elements[0];skip_first=true;}
forEach(elements,function(element,i,len){if(i==0&&skip_first){return;}
acc=callback(acc,element,i,elements);});return acc;},reduceToString:function(callback){return this.reduce(callback,'');},reduceToArray:function(callback){return this.reduce(callback,[]);},allMatches:function(callback){var result=true;forEach(elements,function(element,i,len){if(callback(element,i,len)===false){result=false;return false;}});return result;},anyMatches:function(callback){var result=false;forEach(elements,function(element,i,len){if(callback(element,i,len)===true){result=true;return false;}});return result;},length:elements.length,previousElement:function(){if(elements.length)
return elements[0].previousElementSibling;return null;},nextElement:function(){if(elements.length)
return elements[0].nextElementSibling;return null;},previous:function(){if(elements.length)
return elements[0].previousSibling;return null;},next:function(){if(elements.length)
return elements[0].nextSibling;return null;},addClass:function(cls){n.addClass(elements,cls);return self;},removeClass:function(cls){n.removeClass(elements,cls);return self;},hasClass:function(cls){return n.hasClass(elements,cls);},setClass:function(cls,state){n.setClass(elements,cls,state);return self;},swapClass:function(prev,next){n.removeClass(elements,prev);n.addClass(elements,next);return self;},hide:function(){n.hide(elements);return self;},show:function(){n.show(elements);return self;},setSelection:function(start,end){forEach(elements,function(element){element.setSelectionRange(start||0,end||element.value.length)});},frag:function(){var result=n.frag(elements[0]);if(result.firstChild==result.lastChild){result=result.firstChild;}
return this._select(result);},html:function(new_html){if(typeof new_html==='undefined'||new_html==null){return n.html(elements);}else{n.html(elements,new_html);return self;}},value:function(new_value){if(typeof new_value==='undefined'||new_value==null){return n.value(elements);}else{n.value(elements,new_value);return self;}},text:function(new_text){if(typeof new_text==='undefined'||new_text==null){return n.text(elements);}else{n.text(elements,new_text);return self;}},closest:function(sel){return this._select(n.closest(elements,sel));},parent:function(){return this._select(elements[0].parentNode);},remove:function(){n.remove(elements);return self;},removeAllChildren:function(){n.removeAllChildren(elements);return self;},focus:function(){forEach(elements,function(element){element.focus();});return self;},blur:function(){forEach(elements,function(element){element.blur();});return self;},click:function(){forEach(elements,function(element){element.click();});return self;},on:function(event,handler){n.on(elements,event,handler);return self;},off:function(event,handler){n.off(elements,event,handler);return self;},insertBefore:function(node){n.insertBefore(node,elements);return self;},insertAfter:function(node){n.insertAfter(node,elements);return self;},append:function(node){n.appendChild(node,elements);return self;},appendTo:function(node){forEach(elements,function(element){n.appendChild(element,node);});return self;},prepend:function(node){n.prependChild(node,elements);return self;},prependTo:function(node){forEach(elements,function(element){n.prependChild(element,node);});return self;},replace:function(node){n.replaceNode(node,elements);return self;},attr:function(attrKey,attrVal){if(typeof attrVal==='undefined'||attrVal==null){return n.getAttribute(elements,attrKey);}
n.setAttribute(elements,attrKey,attrVal);return self;},hasAttr:function(attrKey){return n.hasAttribute(elements,attrKey);},removeAttr:function(attrKey){n.removeAttribute(elements,attrKey);return self;},id:function(new_id){if(typeof new_id==='undefined'||new_id==null){return n.getAttribute(elements[0],'id');}else{n.setAttribute(elements[0],'id',new_id);return self;}},modify:function(){var args=[].slice.apply(arguments);args.unshift(null);for(var i=0;i<elements.length;i++){args[0]=elements[i];n.create.apply(null,args);}
return self;},mod:function(){self.modify.apply(self,[].slice.apply(arguments));return self;},};for(var i=0;i<elements.length;i++){self[i]=elements[i];}
return self;};n.isNode=function(o){return(typeof Node==="object"?o instanceof Node:o&&typeof o==="object"&&typeof o.nodeType==="number"&&typeof o.nodeName==="string");};n.isElement=function(o){return(typeof HTMLElement==="object"?o instanceof HTMLElement:o&&typeof o==="object"&&o!==null&&o.nodeType===1&&typeof o.nodeName==="string");};n.isFragment=function(o){return(typeof DocumentFragment==="object"?o instanceof DocumentFragment:o&&typeof o==="object"&&o!==null&&o.nodeType===11&&typeof o.nodeName==="string");};n.isNodeList=function(nodes){var stringRepr=Object.prototype.toString.call(nodes);return typeof nodes==='object'&&/^\[object (HTMLCollection|NodeList|Object)\]$/.test(stringRepr)&&(typeof nodes.length==='number')&&(nodes.length===0||(typeof nodes[0]==="object"&&nodes[0].nodeType>0));};n.mergeNodeList=function(a,b){var slice=Array.prototype.slice;return slice.call(a).concat(slice.call(b));};n.isNodeLike=function(o){return n.isNode(o)||n.isNodeList(o);};n.frag=function(html){if(!isset(html)){return document.createDocumentFragment();}
if(is_function(html)){html=html();}
if(n.isElement(html)&&html.tagName.toUpperCase()=='TEMPLATE'){html=html.innerHTML.trim();}
var frag=document.createDocumentFragment();if(is_array_like(html)){function recursiveAppend(childList){forEach(childList,function(child){if(is_function(child)){child=child();}
if(is_array_like(child)){recursiveAppend(child);return;}
n.appendChild(child,frag);});}
recursiveAppend(html);}else{var div=n.isNode(html)?html:document.createElement("div");if(is_string(html)){div.innerHTML=html;}
var child;while((child=div.firstChild)){frag.appendChild(child);}}
return frag;};(function(){const LISTENERS={};var next_id=0;function get_listener_id(element,no_issue){var my_id=null;if(empty(element)||!n.isElement(element)){}else if(element.hasAttribute('data-evtid')){my_id=parseInt(element.getAttribute('data-evtid'));}else if(!no_issue){my_id=next_id++;element.setAttribute('data-evtid',my_id);}
return isNaN(my_id)?null:my_id;}
n.getListeners=function(element,eventName){var id=get_listener_id(element,true);if(!isset(id)){if(eventName)return[];return{};}
if(eventName){if(LISTENERS[id][eventName]){return LISTENERS[id][eventName];}else{return[];}}
return LISTENERS[id];};n.clone=function(o,deep,copy_listeners){deep=is_undef(deep)?true:deep;copy_listeners=is_undef(copy_listeners)?true:copy_listeners;var id=get_listener_id(o,true);var node=o.cloneNode();if(n.isElement(o)&&o.hasAttribute('data-evtid')){node.removeAttribute('data-evtid');}
if(copy_listeners&&isset(id)&&n.isElement(node)&&LISTENERS[id]){forEach(LISTENERS[id],function(eventName,handlerList){forEach(handlerList,function(eventHandler){n.on(node,eventName,eventHandler);});});}
if(deep&&o.childNodes.length){for(var i=0,len=o.childNodes.length;i<len;i++){node.appendChild(n.clone(o.childNodes[i],deep,copy_listeners));}}
return node;};n.off=function(o,eventName,eventHandler){o=is_string(o)?document.querySelectorAll(o):o;if(empty(o)){return;}
function detach_listener(el){var id=get_listener_id(el,true);if(!eventHandler){if(!LISTENERS[id])
return false;if(!eventName){forEach(LISTENERS[id],function(eventName,handlerList){forEach(handlerList,function(eventHandler){el.removeEventListener(eventName,eventHandler);});});LISTENERS[id]={};}else if(LISTENERS[id][eventName]){var handlerList=LISTENERS[id][eventName];forEach(handlerList,function(eventHandler){el.removeEventListener(eventName,eventHandler);});LISTENERS[id][eventName]=[];}else{return false;}}else{if(LISTENERS[id]&&LISTENERS[id][eventName]){var index=LISTENERS[id][eventName].indexOf(eventHandler);if(index>-1){LISTENERS[id][eventName].splice(index,1);}}
el.removeEventListener(eventName,eventHandler);}
return true;}
if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){detach_listener(o[i]);}}else{detach_listener(o);}};n.on=function(o,eventName,eventHandler){o=is_string(o)?document.querySelectorAll(o):o;if(empty(o)){return;}
var multi_evt=eventName.split(' ');if(multi_evt.length>=2){for(var i=0,len=multi_evt.length;i<len;i++){n.on(o,multi_evt[i],eventHandler);}
return;}
if(eventName==='enter'){eventName='keyup';var real_handler=eventHandler;eventHandler=function(event){if(event.key==='Enter'){real_handler.call(this,event);}else if(event.code==='Enter'){real_handler.call(this,event);}else if(event.which===13){real_handler.call(this,event);}else if(event.keyCode===13){real_handler.call(this,event);}};}
function attach_listener(el){var id=get_listener_id(el);if(!LISTENERS[id]){LISTENERS[id]={};}
if(!LISTENERS[id][eventName]){LISTENERS[id][eventName]=[];}
LISTENERS[id][eventName].push(eventHandler);el.addEventListener(eventName,eventHandler);}
if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){attach_listener(o[i]);}}else{attach_listener(o);}};})();n.insertBefore=function(newNode,ref){ref=is_string(ref)?document.querySelector(ref):ref;if(empty(newNode)||empty(ref)){return;}
if(is_string(newNode)){newNode=n.frag(newNode);}
if(is_object(ref)&&ref.length&&ref.length==1&&ref.hasOwnProperty(0))
ref=ref[0];if(is_array_like(ref)){for(var i=0,len=ref.length;i<len;i++){n.insertBefore(n.clone(newNode),ref[i]);}
return;}
ref.parentNode.insertBefore(newNode,ref);};n.insertAfter=function(newNode,ref){ref=is_string(ref)?document.querySelector(ref):ref;if(empty(newNode)||empty(ref)){return;}
if(is_string(newNode)){newNode=n.frag(newNode);}
if(is_object(ref)&&ref.length&&ref.length==1&&ref.hasOwnProperty(0))
ref=ref[0];if(is_array_like(ref)){for(var i=0,len=ref.length;i<len;i++){n.insertAfter(n.clone(newNode),ref[i]);}
return;}
if(ref.nextSibling)
ref.parentNode.insertBefore(newNode,ref.nextSibling);else
ref.parentNode.appendChild(newNode);};n.appendChild=function(newNode,ref){ref=is_string(ref)?document.querySelector(ref):ref;if(empty(newNode)||empty(ref)){return;}
if(is_string(newNode)){newNode=n.frag(newNode);}
if(is_object(ref)&&ref.length&&ref.length==1&&ref.hasOwnProperty(0))
ref=ref[0];if(is_array_like(ref)){for(var i=0,len=ref.length;i<len;i++){n.appendChild(n.clone(newNode),ref[i]);}
return;}
ref.appendChild(newNode);};n.prependChild=function(newNode,ref){ref=is_string(ref)?document.querySelector(ref):ref;if(empty(newNode)||empty(ref)){return;}
if(is_string(newNode)){newNode=n.frag(newNode);}
if(is_object(ref)&&ref.length&&ref.length==1&&ref.hasOwnProperty(0))
ref=ref[0];if(is_array_like(ref)){for(var i=0,len=ref.length;i<len;i++){n.prependChild(n.clone(newNode),ref[i]);}
return;}
if(ref.firstChild){ref.insertBefore(newNode,ref.firstChild);}else{ref.appendChild(newNode);}};n.removeNode=function(el){el.parentNode.removeChild(el);};n.replaceNode=function(el,newNode){if(empty(el)||empty(newNode)){return;}
if(is_string(newNode)){newNode=n.frag(newNode);}
if(is_array_like(el)){for(var i=0,len=el.length;i<len;i++){n.insertBefore(n.clone(newNode),el[i]);}
return;}
el.parentNode.replaceChild(newNode,el);};n.remove=function(o){if(empty(o)){return;}
o=is_string(o)?document.querySelectorAll(o):o;if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){if(!o[i]||!o[i].parentNode){return;}
o[i].parentNode.removeChild(o[i]);}}else{if(!o||!o.parentNode){return;}
o.parentNode.removeChild(o);}};n.removeAllChildren=function(o){if(empty(o)){return;}
o=is_string(o)?document.querySelectorAll(o):o;if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){while(o[i].firstChild){o[i].removeChild(o[i].firstChild);}}}else{while(o.firstChild){o.removeChild(o.firstChild);}}};n.matches=(function(){var matchesFn;return function(el,selector){if(!matchesFn){var vendor=['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector','oMatchesSelector'];for(var i=0;i<vendor.length;i++){if(typeof document.body[vendor[i]]==='function'){matchesFn=vendor[i];break;}}}
return el&&el[matchesFn](selector);};})();n.closestDescendent=function(el,selector){if(is_array_like(el)){el=el[0];}
if(!el){return false;}
if(n.matches(el,selector)){return el;}
return el.querySelector(selector);};n.closest=function(el,selector){if(is_array_like(el)){el=el[0];}
if(n.matches(el,selector)){return el;}
var parent;while(el){parent=el.parentElement;if(n.matches(parent,selector)){return parent;}
el=parent;}
return false;};n.text=function(o,new_text){o=is_string(o)?document.querySelectorAll(o):o;if(!isset(new_text)){if(is_array_like(o)){o=o[0];}
return o.textContent;}
if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){o[i].textContent=new_text;}}else{o.textContent=new_text;}
return o;};n.html=function(o,new_html){o=is_string(o)?document.querySelectorAll(o):o;if(empty(new_html)){if(is_array_like(o)){o=o[0];}
return o.innerHTML;}
if(is_function(new_html)){new_html=new_html();}
if(is_string(new_html)){if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){o[i].innerHTML=new_html;}}else{o.innerHTML=new_html;}
return;}
n.removeAllChildren(o);if(is_array_like(new_html)){function recursiveAppend(childList){forEach(childList,function(child){if(is_function(child)){child=child();}
if(is_array_like(child)){recursiveAppend(child);return;}
n.appendChild(child,o);});}
recursiveAppend(new_html);}else{n.appendChild(new_html,o);}
return o;};n.value=function(o,new_value){o=is_string(o)?document.querySelectorAll(o):o;if(!isset(new_value)){if(is_array_like(o)){o=o[0];}
return o.value;}
if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){o[i].value=new_value;}}else{o.value=new_value;}
return o;};n.popValue=function(o){o=is_string(o)?document.querySelector(o):o;if(empty(o))
return null;var value=n.value(o);n.value(o,'');return value;};n.disabled=function(o,new_state){o=is_string(o)?document.querySelectorAll(o):o;if(typeof new_state==='undefined'||new_state==null){if(is_array_like(o)){o=o[0];}
return o.disabled;}
if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){o[i].disabled=new_state;}}else{o.disabled=new_state;}
return o;};n.setAttribute=function(o,attr_key,attr_val){if(attr_val===false){return n.removeAttribute(o,attr_key);}
o=is_string(o)?document.querySelectorAll(o):o;attr_val=(attr_val||'');attr_val=(attr_val===true)?'':attr_val;if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){o[i].setAttribute(attr_key,attr_val);}}else{o.setAttribute(attr_key,attr_val);}
return o;};n.popAttribute=function(o,attr_key){var value=n.getAttribute(o,attr_key);n.removeAttribute(o,attr_key);return value;};n.getAttribute=function(o,attr_key){o=is_string(o)?document.querySelector(o):o;if(is_array_like(o))
o=o[0];return o.getAttribute(attr_key);};n.hasAttribute=function(o,attr_key){o=is_string(o)?document.querySelector(o):o;if(is_array_like(o))
o=o[0];return o.hasAttribute(attr_key);};n.removeAttribute=function(o,attr_key){o=is_string(o)?document.querySelectorAll(o):o;if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){o[i].removeAttribute(attr_key);}}else{o.removeAttribute(attr_key);}
return o;};n.hide=function(o){o=is_string(o)?document.querySelectorAll(o):o;if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){o[i].style.display='none';}}else{o.style.display='none';}
return o;};n.show=function(o){o=is_string(o)?document.querySelectorAll(o):o;if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++){o[i].style.display='';}}else{o.style.display='';}
return o;};n.addClass=function(o,className){if(empty(o)||empty(className))
return;o=is_string(o)?document.querySelectorAll(o):o;function do_stuff(el){if(el.classList){if(contains(className,' ')){forEach(className.split(' '),function(item){el.classList.add(item);});}else{el.classList.add(className);}}else if(!n.hasClass(el,className)){el.className+=' '+className;}}
if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++)
do_stuff(o[i]);}else do_stuff(o);return o;};n.removeClass=function(o,className){if(empty(o)||empty(className))
return;o=is_string(o)?document.querySelectorAll(o):o;function do_stuff(el){if(el.classList){if(contains(className,' ')){forEach(className.split(' '),function(item){el.classList.remove(item);});}else{el.classList.remove(className);}}else{var regExp=new RegExp('(?:^|\\s)'+className+'(?!\\S)','g');el.className=el.className.replace(regExp);}}
if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++)
do_stuff(o[i]);}else do_stuff(o);return o;};n.toggleClass=function(o,className){if(empty(o)||empty(className))
return;o=is_string(o)?document.querySelectorAll(o):o;function do_stuff(el){if(el.classList){el.classList.toggle(className);}else{if(n.hasClass(el,className)){n.removeClass(el,className);}else{el.className+=' '+className;}}}
if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++)
do_stuff(o[i]);}else do_stuff(o);return o;};n.hasClass=function(o,className){if(empty(o)||empty(className))
return false;o=is_string(o)?document.querySelectorAll(o):o;function do_stuff(el){if(el.classList){if(contains(className,' ')){forEach(className.split(' '),function(item){if(!el.classList.contains(item)){return false;}});return true;}else{return el.classList.contains(className);}}else{var regExp=new RegExp('(?:^|\\s)'+className+'(?!\\S)','g');return el.className.match(regExp);}}
if(is_array_like(o)){for(var i=0,len=o.length;i<len;i++)
if(!do_stuff(o[i]))
return false;}else return do_stuff(o);return true;};n.setClass=function(o,className,state){if(state){n.addClass(o,className);}else{n.removeClass(o,className);}
return o;};n.copyTextToClipboard=function(text){var textArea=document.createElement("textarea");textArea.style.position='fixed';textArea.style.top=0;textArea.style.left=0;textArea.style.width='2em';textArea.style.height='2em';textArea.style.padding=0;textArea.style.border='none';textArea.style.outline='none';textArea.style.boxShadow='none';textArea.style.background='transparent';textArea.value=text;document.body.appendChild(textArea);textArea.select();var successful=true;try{successful=document.execCommand('copy');}catch(err){successful=false;}
document.body.removeChild(textArea);return successful;};n.isTextSelected=function(input){if(typeof input.selectionStart=="number"){return input.selectionStart==0&&input.selectionEnd==input.value.length;}else if(typeof document.selection!="undefined"){input.focus();return document.selection.createRange().text==input.value;}};n.elementInViewport=function(el){var top=el.offsetTop;var left=el.offsetLeft;var width=el.offsetWidth;var height=el.offsetHeight;while(el.offsetParent){el=el.offsetParent;top+=el.offsetTop;left+=el.offsetLeft;}
return(top<(window.pageYOffset+window.innerHeight)&&left<(window.pageXOffset+window.innerWidth)&&(top+height)>window.pageYOffset&&(left+width)>window.pageXOffset);};;n.gen=function(selector){if(n.isNode(selector))
return selector;selector=selector.replace(/\n/g,' ');var callbacks=Array.prototype.slice.call(arguments,1);var result_node=n.gen.walk(selector,0,callbacks).node;if(result_node==null){return n.frag();}
return n.gen.count(result_node,callbacks);};n.gen1=function(selector){if(n.isNode(selector))
return selector;selector=selector.replace(/\n/g,' ');var callbacks=Array.prototype.slice.call(arguments,1);var result_node=n.gen.create(selector,0,callbacks).node;if(result_node==null){return n.frag();}
return n.gen.count(result_node,callbacks);};n.genTag=function(tagName){if(n.isNode(tagName))
return tagName;return document.createElement(tagName);};n.simple_gen=function(element,attrList){if(typeof element=="string"){element=document.createElement(element);}
if(!isset(attrList)){return element;}
for(var attrKey in attrList){if(attrList.hasOwnProperty(attrKey)){var lkey=attrKey.toLowerCase();var value=attrList[attrKey];if(typeof value=='undefined'||attrList[attrKey]==null){continue;}
if(lkey=='textcontent'||lkey=='text'){element.textContent=value;continue;}
if(lkey=='innerhtml'||lkey=='html'){if(typeof value==='function'){value=value(element);}
n.html(element,value);continue;}
if(startsWith(lkey,'on')){n.on(element,attrKey.substring(2),value);continue;}
if(typeof value==='boolean'){if(value){element.setAttribute(attrKey,'');}else{element.removeAttribute(attrKey);}
continue;}
if(startsWith(lkey,'+')&&isset(element.getAttribute(attrKey=attrKey.slice(1)))){element.setAttribute(attrKey,element.getAttribute(attrKey)+' '+value);}else{element.setAttribute(attrKey,value);}}}
return element;};n.create=function(){var a=[].slice.apply(arguments),l=a.length,el,attrList={},children=[],a_offset=1;if(l==0){return n.genTag('div');}
el=a[0];if((is_object(el)||is_array(el))&&!n.isNode(el)){el=null;a_offset=0;}else if(is_string(el)){if(/^[a-zA-Z][a-zA-Z\-]+[a-zA-Z]$/.test(el)){el=n.genTag(el);}else{el=n.gen1(el);}}
var component=null;for(var i=a_offset;i<l;i++){var tmp=a[i];if(typeof tmp==='function'){tmp=tmp(el);}
if(is_array(tmp)||n.isNode(tmp)||is_string(tmp)||n.isFragment(tmp)){children.push(tmp);}else if(is_object(tmp)){if(tmp.hasOwnProperty('render')){component=copy(tmp);}else{extend(attrList,tmp);}}}
if(!component){el=n.simple_gen(el||n.genTag('div'),attrList);}else{el=el||document.createDocumentFragment();if(component.hasOwnProperty('init')){component.init.call(component,attrList);}
children=component.render.call(component,attrList);}
if(children){if(typeof children==='function'){children=children(el);}
if(is_array(children)&&children.length==1){children=children[0];}
if(is_array(children)){function f(child){if(typeof child==='function'){child=child(el);}
if(is_array(child)){forEach(flatten(child),f);return;}
if(is_string(child)){child=document.createTextNode(child);}
if(!n.isNode(child)&&!n.isFragment(child)){return;}
el.appendChild(child);}
forEach(children,f);}else if(n.isNode(children)){el.appendChild(children);}else if(n.isFragment(children)){el.appendChild(children);}else if(is_string(children)){if(contains(n.create.text_elements,el.tagName.toLowerCase())){el.appendChild(document.createTextNode(children));}else{el.appendChild(n.frag(children));}}else{return false;}}
if(isset(component)&&n.isFragment(el)&&el.firstChild===el.lastChild){return el.firstChild;}
return el;};n.create.text_elements=['a','u','s','i','b','em','sup','sub','strong','span','small','code','h1','h2','h3','h4','h5','h6'];n.create.text=function(element,text){return n.create(element,{'text':text});};n.gen.cattr='data-n-counter';n.gen.cstart='{';n.gen.cend='}';n.gen.count=function(node,callbacks){var list=Array.prototype.slice.call(node.querySelectorAll('['+n.gen.cattr+']')),indices={};if(!(node instanceof DocumentFragment)&&node.hasAttribute(n.gen.cattr))
list.unshift(node);if(!list.length)
return node;callbacks=n.gen.count.convert_callbacks(callbacks);var len_count={};function run_callback(id,el,idx,token){if(!contains(len_count,token)){var tmp=node.querySelectorAll('['+n.gen.cattr+'~="'+token+'"]').length;if(!(node instanceof DocumentFragment)&&node.hasAttribute(n.gen.cattr)&&contains(node.getAttribute(n.gen.cattr),token))
tmp++;len_count[token]=tmp;}
if(startsWith(id,'$'))
return n.gen.run_special_callback(el,idx,len_count[token],id);if(id in callbacks){if(typeof callbacks[id]=='function')
return callbacks[id](el,idx,len_count[token]);else
return callbacks[id];}
return null;}
for(var i=0,len=list.length;i<len;i++){var el=list[i];var data=el.getAttribute(n.gen.cattr).split(' ');for(var j=0;j<data.length;j++){var token=data[j],cdata=token.split(':'),id=cdata[0],subject=cdata[1],idx;if(contains(indices,token)){idx=(indices[token]=indices[token]+1);}else{idx=(indices[token]=0);}
var val=run_callback(id,el,idx,token);if(typeof val==='undefined'||val===null){val='';}
if(subject=='attr'){n.gen.count.check(subject,id,el,val,cdata[2]);}else{n.gen.count.check(subject,id,el,val);}}
el.removeAttribute(n.gen.cattr);}
return node;};n.gen.value_only=function(id,callbacks){callbacks=n.gen.count.convert_callbacks(callbacks);if(typeof callbacks[id]=='function')
return callbacks[id]();else
return callbacks[id];};n.gen.count.convert_callbacks=function(callbacks){var tmp={};for(var i=0,j=0,len=callbacks.length;i<len;i++){var callback=callbacks[i];if(typeof callback==='function'){tmp[j++]=callback;}else if(is_string(callback)){tmp[j++]=callback;}else if(typeof callback=='object'){for(var propName in callback){if(callback.hasOwnProperty(propName)){tmp[propName]=callback[propName];}}}else{tmp[j++]=callback;}}
return tmp;};n.gen.count.check=function(subject,id,el,val,attr_key){id=n.gen.cstart+id+n.gen.cend;switch(subject){case'attr':if(contains(attr_key,n.gen.cstart)){var new_attr_key=string_replace(attr_key,id,val);el.setAttribute(new_attr_key,el.getAttribute(attr_key));attr_key=new_attr_key;}
case'class':case'id':var attr_name=subject=='attr'?attr_key:subject;el.setAttribute(attr_name,string_replace(el.getAttribute(attr_name),id,val));break;case'text':el.textContent=string_replace(el.textContent,id,val);}};n.gen.counters={ascend:function(el,idx,len){return idx+1;},ascendOffset:function(el,idx,len,offset){return idx+offset;},descend:function(el,idx,len){return len-(idx);},descendOffset:function(el,idx,len,offset){return offset-idx;},alphaLower:function(el,idx,len){return String.fromCharCode(97+idx);},alphaUpper:function(el,idx,len){return String.fromCharCode(65+idx);},};n.gen.run_special_callback=function(el,idx,len,id){var action=id.substring(1);action=action.substring(action.indexOf('$')+1);if(action==''||action=='+'){return n.gen.counters.ascend(el,idx,len);}else if(startsWith(action,'+')){return n.gen.counters.ascendOffset(el,idx,len,parseInt(action.substring(1)));}
if(action=='-'){return n.gen.counters.descend(el,idx,len);}else if(startsWith(action,'-')){return n.gen.counters.descendOffset(el,idx,len,parseInt(action.substring(1)));}
if(action=='a'){return n.gen.counters.alphaLower(el,idx,len);}else if(action=='A'){return n.gen.counters.alphaUpper(el,idx,len);}};n.gen.walk=function(selector,nextCounter,callbacks){if(!selector||!selector.length)
return document.createElement('div');nextCounter=nextCounter||0;callbacks=callbacks||[];var container=document.createDocumentFragment(),action='>',startOffset=0,workingNode=container,multiply_list=[],remove_list=[];function paren_match(paren_offset){for(var j=paren_offset,k=0,len=selector.length;j<len;j++){var c=selector.charAt(j);if(c=='(')
k++;if(c==')'){if(k==0)return j;k--;}}
return-1;}
while(true){var result=n.gen.create(selector,startOffset,nextCounter),newIndex=result.index+1,nextCounter=result.nextCounter;if(result.next=='('){var groupSelector=selector.substring(newIndex,newIndex=paren_match(newIndex)),group_walk_result=n.gen.walk(groupSelector,nextCounter,callbacks);result.node=group_walk_result.node;nextCounter=result.nextCounter=group_walk_result.nextCounter;var walk_del_result=n.gen.walk_to_delimiter(selector,newIndex+1);result.index=walk_del_result.index;result.next=walk_del_result.next;newIndex=result.index+1;}
if(result.next=='*'){var walk_del_result=n.gen.walk_to_delimiter(selector,newIndex);result.index=walk_del_result.index;result.next=walk_del_result.next;var factor=selector.substring(newIndex,result.index).trim(),multiply_times=parseInt(factor),is_frag=n.isFragment(result.node);if(startsWith(factor,'{')&&endsWith(factor,'}')){var mult_callback_name=factor.substring(1,factor.length-1);if(mult_callback_name.length==0){mult_callback_name=nextCounter++;}
multiply_times=n.gen.value_only(mult_callback_name,callbacks);}
if(multiply_times&&multiply_times>1){multiply_list.push({node:is_frag?result.node.cloneNode(true):result.node,refnode:is_frag?result.node.lastElementChild:result.node,isfrag:is_frag,times:multiply_times,});}
if(multiply_times!=null&&multiply_times==0){if(is_frag){for(var k=0;k<result.node.children.length;k++){remove_list.push(result.node.children[k]);}}else{remove_list.push(result.node);}}
newIndex=result.index+1;}
switch(action){case'^':workingNode=workingNode.parentNode;case'+':if(result.node){n.insertAfter(result.node,workingNode);if(n.isFragment(result.node)){workingNode=workingNode.parentNode.lastElementChild;}else{workingNode=result.node;}}
break;case'>':if(result.node){workingNode.appendChild(result.node);if(n.isFragment(result.node)){workingNode=workingNode.lastElementChild;}else{workingNode=result.node;}}
break;}
if((action=result.next)==null){break;}
startOffset=newIndex;}
for(var j=0;j<multiply_list.length;j++){var item=multiply_list[j],times=item.times-1,ref=item.refnode;for(var k=0;k<times;k++){n.insertAfter(item.node.cloneNode(true),ref);}}
for(var j=0;j<remove_list.length;j++){n.removeNode(remove_list[j]);}
return{node:container.firstElementChild==container.lastElementChild?container.firstElementChild:container,nextCounter:nextCounter,endWorkingNode:workingNode};};n.gen.walk_to_delimiter=function(selector,startOffset){var next_step=null;for(var len=selector.length;startOffset<len;startOffset++){var c=selector.charAt(startOffset);if(n.gen.is_delimiter(c)){next_step=c;break;}}
return{next:next_step,index:startOffset,}};n.gen.is_delimiter=function(c,subject){if(subject=='attr'||subject=='text')
return false;switch(c){case'>':case'^':case'+':case'(':case'*':return true;}};n.gen.create=function(selector,startOffset,nextCounter){var el=null,sb='',c=null,i=startOffset||0,next_step=null,subject='tag',subject_map={'.':'class','#':'id','[':'attr-or-text',']':'null',};nextCounter=nextCounter||0;function remove_quotes(str){str=str.trim();if(startsWith(str,'"')||startsWith(str,"'"))
return str.substring(1,str.length-1);return str;}
function is_attr_text(){return(subject=='attr-or-text'||subject=='attr'||subject=='text');}
function subject_end(){switch(subject){case'tag':el=document.createElement(sb.length?sb:'div');break;case'class':n.addClass(el,attach_counter(sb));break;case'id':el.setAttribute('id',attach_counter(sb));break;case'attr':case'attr-or-text':var isTextContent=false;sb=sb.trim();if(startsWith(sb,'"')||startsWith(sb,"'")){if(contains(sb,'=')){var tmp=sb.split('=')[0].trim();isTextContent=!(endsWith(tmp,"'")||endsWith(tmp,'"'));}else{isTextContent=true;}}
if(isTextContent){subject='text';el.textContent=attach_counter(sb=remove_quotes(sb));}else{subject='attr';var data=sb.split('='),attr_val,attr_key=attach_counter(data[0],null,true);attr_key=attach_counter(attr_key,attr_key);attr_val=attach_counter(typeof data[1]=='undefined'?'':remove_quotes(data[1]),attr_key);el.setAttribute(attr_key,attr_val);}
break;case'text':el.textContent=attach_counter(sb=remove_quotes(sb));break;}
sb='';subject=subject_map[c];}
function is_symbol(){if(subject_map.hasOwnProperty(c)){return!(subject_map[c]!='null'&&is_attr_text());}
return false;}
function attach_counter(text,attr_key,skipTagging,startOffset){if(!contains(text,n.gen.cstart))
return text;var counterId=null,startOffset=startOffset||0,startIdx=text.indexOf(n.gen.cstart,startOffset),endIdx=text.indexOf(n.gen.cend,startIdx);if(startIdx+1!=endIdx){counterId=text.substring(startIdx+1,endIdx).trim();if(startsWith(counterId,'$')){var origCounterId=counterId;counterId=('$'+(nextCounter++));text=string_insert(text,startIdx+1,counterId);counterId=counterId+origCounterId;}}else{counterId=nextCounter++;text=string_insert(text,startIdx+1,counterId);}
var nextIdx=text.indexOf(n.gen.cstart,endIdx);if(nextIdx!=-1){text=attach_counter(text,attr_key,skipTagging,nextIdx);}
if(skipTagging)
return text;counterId=counterId+':'+subject+(subject=='attr'?':'+attr_key:'');if(el.hasAttribute(n.gen.cattr)){el.setAttribute(n.gen.cattr,el.getAttribute(n.gen.cattr)+' '+counterId);}else{el.setAttribute(n.gen.cattr,counterId);}
return text;}
function append_char(){if(c!=' '||(c==' '&&is_attr_text()))sb+=c;}
for(var len=selector.length;i<len;i++){c=selector.charAt(i);if(n.gen.is_delimiter(c,subject)&&!is_attr_text()){next_step=c;break;}
if(is_symbol())
subject_end();else
append_char();}
subject_end();return{node:startOffset==i?null:el,next:next_step,index:i,nextCounter:nextCounter,};};;const app={};app.forms={};const PREREQUISITE_INIT_NAME='LoadInitOpts';const init=(function(){var initopts={};var STATUS_PENDING={};var STATUS_COMPLETE={};var AWAIT_PREQREQ=[];return function(initName,handler){if(STATUS_PENDING[initName]||!is_string(initName)){return;}else{STATUS_PENDING[initName]=true;}
if(!STATUS_COMPLETE[PREREQUISITE_INIT_NAME]&&initName!==PREREQUISITE_INIT_NAME){STATUS_PENDING[initName]=false;AWAIT_PREQREQ.push(function(){init(initName,handler);}.bind(null));return;}
var options=initopts[initName]||{};console.log('init: '+initName);function handle_result(result){if(initName===PREREQUISITE_INIT_NAME){initopts=result;}
STATUS_COMPLETE[initName]=true;console.log('init-complete: '+initName);if(initName===PREREQUISITE_INIT_NAME){forEach(AWAIT_PREQREQ,function(fn){fn();});}}
if(/comp|inter|loaded/.test(document.readyState)){handle_result(handler(options));}else{document.addEventListener('DOMContentLoaded',function(){handle_result(handler(options));},false);}};})();init(PREREQUISITE_INIT_NAME,function(){var element=document.head.querySelector('meta[name="x-init-opts"]'),loaded_opts={};if(!element||this.loaded){return loaded_opts;}else{element.parentNode.removeChild(element);this.loaded=true;}
var safename_list=element.getAttribute('content').split(',');forEach(safename_list,function(safe_name){var safe_data=element.getAttribute('data-'+safe_name);if(!safe_data)
return;var opt_data=JSON.parse(safe_data),src_path=safe_name.split('-'),initName=src_path.shift(),opt_name=src_path.pop(),source;if(initName==='var'){source=reduce(src_path,function(acc,item){return acc[item];},(src_path[0]==='app')?src_path.shift()&&app:window);}else{source=loaded_opts[initName]||(loaded_opts[initName]={});}
source[opt_name]=opt_data;});return loaded_opts;});init('Basic',function(opts){if(SITE_IS_STAGING){var ael=Node.prototype.addEventListener,rel=Node.prototype.removeEventListener;Node.prototype.addEventListener=function(a,b,c){console.log('Listener','added','('+a+')',this,b,c);ael.apply(this,arguments);};Node.prototype.removeEventListener=function(a,b,c){console.log('Listener','removed','('+a+')',this,b,c);rel.apply(this,arguments);};}else{if(!console)window.console={};console.log=function(){};}
app.tempSetNightMode=function(state){if(state){document.body.classList.add('nightmode');document.body.classList.remove('daymode');}else{document.body.classList.add('daymode');document.body.classList.remove('nightmode');}};function updateCurrentHashGlobal(first_call){if(!window.location.hash)
return;forEach('.add-hash',function(el){var default_url=el.getAttribute('data-default')||el.getAttribute('href')||'';el.setAttribute('href',default_url+window.location.hash);if(first_call){el.setAttribute('data-default',default_url);}});}
updateCurrentHashGlobal(true);window.onhashchange=function(){updateCurrentHashGlobal();};const PUSH_STATE=window.history.pushState;const REPLACE_STATE=window.history.replaceState;function updateCurrentURLGlobal(){window['CURRENT_URL']=window.location.href;}
window.history.pushState=function(){PUSH_STATE.apply(window.history,arguments);updateCurrentURLGlobal();};window.history.replaceState=function(){REPLACE_STATE.apply(window.history,arguments);updateCurrentURLGlobal();};window.onpopstate=function(){updateCurrentURLGlobal();};if(window.Vue){new Vue({el:'#website'});if(SITE_IS_STAGING){Vue.config.devtools=true;}}});;function r(){var d={},f,args=Array.prototype.slice.call(arguments);(f=function(a){forEach(a,function(v){if(is_string(v)){if(contains(app.requests.methods,v.toUpperCase())){d['method']=v;}else{d['url']=v;}}else if(is_object(v)){if(is_array(v)){f(v);}else{forEach(v,function(vk,vv){if(vk==='data'){vk='params';}
d[vk]=vv;});}}});})(args);return app.requests.send.call(null,d);}
extend(r,{get:function(){r.apply(null,Array.prototype.slice.call(arguments).extend('GET'));},post:function(){r.apply(null,Array.prototype.slice.call(arguments).extend('POST'));},put:function(){r.apply(null,Array.prototype.slice.call(arguments).extend('PUT'));},delete:function(){r.apply(null,Array.prototype.slice.call(arguments).extend('DELETE'));}});init('AppRequests',function(opts){var AUTH_TOKEN=opts.CSRF_TOKEN||null;var next_req_id=0;var running_requests={};app.requests={methods:['GET','POST','PUT','PATCH','DELETE','OPTIONS','HEAD'],getRunningCount:function(){return Object.keys(running_requests).length;},getRunningCountObj:function(){return running_requests;},prepare:{buildURI:function(uri,params){var parts=[];if(uri.charAt(0)=='/'){parts.push(SITE_API);}
parts.push(uri);parts.push(uri.indexOf('?')!==-1?'&':'?');parts.push(app.requests.prepare.encodeQueryData(params));return parts.join('');},encodeQueryData:function(data){var ret=[];for(var d in data){if(data.hasOwnProperty(d)){ret.push(d+'='+encodeURIComponent(data[d]));}}
return ret.join('&');},},send:function(data){var http_method=data['method']||'GET',no_auth=data['no_auth']||false,is_passive=data['is_passive']||false,params=data['params']||{},url=app.requests.prepare.buildURI(data['url'],params),req_id=next_req_id++;var request=new XMLHttpRequest();request.open(http_method,url,true);request.setRequestHeader('X-Requested-With','XMLHttpRequest');if(!no_auth&&!empty(AUTH_TOKEN)){request.setRequestHeader('X-CSRF-Token',AUTH_TOKEN);}
function run_callback(callback_name,args_array){var ret=false;if(is_function(data[callback_name])){ret=data[callback_name].apply(data,args_array||[]);}else if(data[callback_name]===false){return;}else if(callback_name==='status_error'){return;}
if(ret===false){switch(callback_name){case'start':case'receive':case'success':break;case'error':case'status_error':case'parse_error':if(args_array[0]==="NOT_AUTHORIZED"&&args_array[2]&&args_array[2].moreinfo){if(args_array[2].moreinfo[0]==="IP_CMP_FAIL"){app.dialog("Network changed (you might've connected to or been "+
"disconnected from a Wi-Fi network). "+
"Refresh the page and try again.",app.DIALOG_ERROR);}else if(args_array[2].moreinfo[0]==="TOKEN_EXPIRED"){app.dialog("Authentication expired. Refresh the page and try again.",app.DIALOG_ERROR);}else{app.dialog("Authentication failed. Try logging in and back out.",app.DIALOG_ERROR);}}else{app.dialog("Sorry, an internal error occurred. "+
"Please try again later.",app.DIALOG_ERROR);}
break;case'network_error':app.dialog("A network error occurred, please check your internet "+
"connection and try again later.",app.DIALOG_ERROR);break;}}}
var handlers={on_response:function(){delete running_requests[req_id];var is_success=request.status>=200&&request.status<300,label,info,response_token=request.getResponseHeader('X-CSRF-Token'),content_type=request.getResponseHeader('Content-Type'),raw_data=request.responseText,res;console.log('Response: '+request.status+' '+content_type);run_callback('receive',[request]);if(!is_success){run_callback(label='status_error',[request]);}
if(startsWith(raw_data,'while(1);')){raw_data=raw_data.slice(9);}
if(response_token){AUTH_TOKEN=response_token;console.log('New Token: '+AUTH_TOKEN.slice(0,6)+'..');}
try{switch(content_type){case'application/json':res=JSON.parse(raw_data);if(res&&res.error){run_callback(label='error',info=[res.error||null,res.error_description||null,res.error_detail||null]);}
break;default:res=raw_data;break;}
if(is_success){run_callback(label='success',info=[res]);}}catch(e){console.log(e);console.log(raw_data);run_callback(label='parse_error',info=[raw_data]);}
run_callback('done',[label,request.status].concat(info));},on_network_error:function(){delete running_requests[req_id];run_callback('receive',[request]);run_callback('network_error');run_callback('done',['network_error',request]);},on_start:function(){console.log(http_method+': '+url);if(!is_passive)
running_requests[req_id]=true;},on_end:function(){delete running_requests[req_id];},};request.onloadstart=handlers.on_start;request.onloadend=handlers.on_end;request.onerror=handlers.on_network_error;request.onload=handlers.on_response;run_callback('start',[request]);request.send();},};});;