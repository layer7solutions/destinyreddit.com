<?php
namespace app\lib\util;

use app\lib\clans\Application;
use app\lib\util\Message;

class DiscordChannelMessage extends Message {

    public function send(Application $app = null) {
        if (empty($this->to) || empty(trim($this->body))) {
            return;
        }

        discord_mod()->post_message($this->to, $this->body);
    }

    public static function create($channel_id, $body, $sent_by = null, $addBlank = false) {
        if (empty($channel_id) || empty($body))
            return null;
        if ($addBlank)
            $body .= "\n<:blank:364964889770590208>";
        return new self($channel_id, $body, $sent_by);
    }

}