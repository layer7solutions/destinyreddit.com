<?php
function chkbodycls($classes, $no_print = false) {
    foreach ((array) $classes as $cls) {
        if (body_class_exists('page--'.$cls)) {
            if (!$no_print)
                echo ' selected';
            return true;
        }
    }
    return false;
}
?>
<div id="top"></div>
<noscript>
    <div class="error-notice">
        <p><strong>This website does not work without JavaScript.</strong></p>
    </div>
</noscript>
<header id="nav" class="valign">
    <div id="nav-main" class="valign">
        <div class="site-title">
            <a class="site-title-a" href="<?php echo SITE_URL ?>"><h1><span>Destiny</span><span class="sep">&nbsp;</span><span>Reddit</span></h1></a>
        </div>
        <ul id="nav-links" class="nav-menu">
            <li class="nav-item for-reddit<?php $hascls = chkbodycls('reddit') ?>">
                <a><span>Reddit</span><?php
                    echo "<span>";
                    if (chkbodycls('flair', true)) {
                        echo "Flair Selector";
                    }
                    echo "</span>";
                ?></a>
                <ul class="nav-menu">
                    <li class="nav-item<?php chkbodycls('flair') ?>">
                        <a href="<?php echo SITE_URL ?>flair">Flair Selector</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item for-discord<?php $hascls = chkbodycls('discord') ?>">
                <a><span>Discord</span><?php
                    echo "<span>";
                    if (chkbodycls('clans_recruitform', true)) {
                        echo "Clan Recruitment";
                    } else if (chkbodycls('clans_applyform', true)) {
                        echo "Clan Recruitment";
                    }
                    echo "</span>";
                ?></a>
                <ul class="nav-menu">
                    <li class="nav-item<?php chkbodycls('clans_recruitform') ?>">
                        <a href="<?php echo SITE_URL ?>clan-recruitment">Clan Recruitment</a>
                    </li>
                    <!-- <li class="nav-item<?php chkbodycls('clans_applyform') ?>">
                        <a href="<?php echo SITE_URL ?>clan-apply">Apply to our Discord clan</a>
                    </li>
                    -->
                </ul>
            </li>
            <?php if (reddit_user()->is_admin() || discord_user()->is_admin()): ?>
            <li class="nav-item for-admin<?php $hascls = chkbodycls('admin') ?>">
                <a><span>Admin Panel</span><?php
                    echo "<span>";
                    if (chkbodycls('admin_stylesheet', true)) {
                        echo "Stylesheet CTL";
                    } else if (chkbodycls('admin_clans', true)) {
                        echo "Clans ACP";
                    }
                    echo "</span>";
                ?></a>
                <ul class="nav-menu">
                    <?php if (reddit_user()->is_admin()): ?>
                        <li class="nav-item-header">Reddit Admin</li>
                        <li class="nav-item<?php chkbodycls('admin_stylesheet') ?>">
                            <a href="<?php echo SITE_URL ?>admin/stylesheet">Stylesheet CTL</a>
                        </li>
                    <?php endif; ?>
                    <?php if (discord_user()->is_admin()): ?>
                        <li class="nav-item-header">Discord Admin</li>
                        <li class="nav-item<?php chkbodycls('admin_clans') ?>">
                            <a href="<?php echo SITE_URL ?>admin/clans">Clans ACP</a>
                        </li>
                    <?php endif; ?>
                </ul>
            <li>
            <?php endif; ?>
        </ul>
    </div>
    <div id="nav-user-area" class="valign grow">
        <div class="user user--reddit valign <?php echo 'logged-' . (reddit_user()->logged_in() ? 'in' : 'out') ?>">
            <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 24 24">
                <path fill="#000" d="M24 11.779c0-1.459-1.192-2.645-2.657-2.645-.715 0-1.363.286-1.84.746-1.81-1.191-4.259-1.949-6.971-2.046l1.483-4.669 4.016.941-.006.058c0 1.193.975 2.163 2.174 2.163 1.198 0 2.172-.97 2.172-2.163s-.975-2.164-2.172-2.164c-.92 0-1.704.574-2.021 1.379l-4.329-1.015c-.189-.046-.381.063-.44.249l-1.654 5.207c-2.838.034-5.409.798-7.3 2.025-.474-.438-1.103-.712-1.799-.712-1.465 0-2.656 1.187-2.656 2.646 0 .97.533 1.811 1.317 2.271-.052.282-.086.567-.086.857 0 3.911 4.808 7.093 10.719 7.093s10.72-3.182 10.72-7.093c0-.274-.029-.544-.075-.81.832-.447 1.405-1.312 1.405-2.318zm-17.224 1.816c0-.868.71-1.575 1.582-1.575.872 0 1.581.707 1.581 1.575s-.709 1.574-1.581 1.574-1.582-.706-1.582-1.574zm9.061 4.669c-.797.793-2.048 1.179-3.824 1.179l-.013-.003-.013.003c-1.777 0-3.028-.386-3.824-1.179-.145-.144-.145-.379 0-.523.145-.145.381-.145.526 0 .65.647 1.729.961 3.298.961l.013.003.013-.003c1.569 0 2.648-.315 3.298-.962.145-.145.381-.144.526 0 .145.145.145.379 0 .524zm-.189-3.095c-.872 0-1.581-.706-1.581-1.574 0-.868.709-1.575 1.581-1.575s1.581.707 1.581 1.575-.709 1.574-1.581 1.574z"/>
            </svg>
            <div class="user-panel">
            <?php if (reddit_user()->logged_in()): ?>
                <span class="username"><span>u/</span><?php xecho(reddit_user()->get_username()) ?></span>
                <a href="<?php echo SITE_URL ?>reddit-logout?cont=<?php echo safe_current_url() ?>">Logout</a>
            <?php else: ?>
                <a href="<?php echo SITE_URL ?>reddit-login?cont=<?php echo safe_current_url() ?>">Login</a>
            <?php endif; ?>
            </div>
        </div>
        <div class="user user--discord valign <?php echo 'logged-' . (discord_user()->logged_in() ? 'in' : 'out') ?>">
            <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 48 48">
                <path fill="#000" d="M 40 12 C 40 12 35.414063 8.410156 30 8 L 29.511719 8.976563 C 34.40625 10.175781 36.652344 11.890625 39 14 C 34.953125 11.933594 30.960938 10 24 10 C 17.039063 10 13.046875 11.933594 9 14 C 11.347656 11.890625 14.019531 9.984375 18.488281 8.976563 L 18 8 C 12.320313 8.535156 8 12 8 12 C 8 12 2.878906 19.425781 2 34 C 7.160156 39.953125 15 40 15 40 L 16.640625 37.816406 C 13.855469 36.847656 10.714844 35.121094 8 32 C 11.238281 34.449219 16.125 37 24 37 C 31.875 37 36.761719 34.449219 40 32 C 37.285156 35.121094 34.144531 36.847656 31.359375 37.816406 L 33 40 C 33 40 40.839844 39.953125 46 34 C 45.121094 19.425781 40 12 40 12 Z M 17.5 30 C 15.566406 30 14 28.210938 14 26 C 14 23.789063 15.566406 22 17.5 22 C 19.433594 22 21 23.789063 21 26 C 21 28.210938 19.433594 30 17.5 30 Z M 30.5 30 C 28.566406 30 27 28.210938 27 26 C 27 23.789063 28.566406 22 30.5 22 C 32.433594 22 34 23.789063 34 26 C 34 28.210938 32.433594 30 30.5 30 Z "/>
            </svg>
            <div class="user-panel">
            <?php if (discord_user()->logged_in()): ?>
                <span class="username"><span>@</span><?php xecho(discord_user()->get_username()) ?></span>
                <a href="<?php echo SITE_URL ?>discord-logout?cont=<?php echo safe_current_url() ?>">Logout</a>
            <?php else: ?>
                <a href="<?php echo SITE_URL ?>discord-login?cont=<?php echo safe_current_url() ?>">Login</a>
            <?php endif; ?>
            </div>
        </div>
        <!--
        <div class="user user--bungie valign <?php echo 'logged-' . (bungie_user()->logged_in() ? 'in' : 'out') ?>">
            <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="28" height="28" xml:space="preserve"
                viewBox="0 0 619.14667 575.06665">
                <g transform="matrix(1.3333333,0,0,-1.3333333,0,575.06667)">
                    <g transform="scale(0.1)">
                        <path style="fill:#000;fill-opacity:1;fill-rule:nonzero;stroke:none" d="m 1583.15,3948.86 c 4.09,-336.88 0,-661.13 0,-1028.2 0,-109 -5.45,-218.27 0,-320.2 26.41,-493.42 491,-854.21 992.12,-671.94 242.7,88.26 441.08,299.55 478.03,590.76 23.26,183.18 4.51,461.46 4.51,708.03 0,164.87 0,350.95 0,536.66 0,51.32 -5.18,141.96 0,175.86 0,0 134.17,73.67 162.36,90.2 163.21,95.71 294.31,201.77 491.55,257.05 323.83,90.76 580.52,-74.39 726.05,-225.48 160.45,-166.56 267.18,-455.88 166.86,-753.13 C 4520.11,3058.03 4315.6,2943.61 4113.09,2803.4 3721.46,2532.27 3420.71,2167.31 3233.69,1685 3143.94,1453.52 3091.64,1148.36 3062.33,868.75 3045.06,703.98 3036.5,567.059 2985.66,444.832 2893.74,223.859 2706.21,42.4414 2426.46,7.40234 1951.75,-52.0508 1662.77,254.082 1601.19,661.309 1577.92,815.199 1571.98,967.762 1551.58,1112.27 1450.89,1825.89 1113.59,2345.4 649.648,2708.7 370.57,2927.23 -11.3242,3073.2 0.257813,3556.52 3.875,3707.57 52.5352,3841.2 112.996,3939.83 c 119.598,195.12 327.586,367.65 640.379,369.8 369.905,2.54 549.735,-223.9 829.775,-360.77" />
                        <path style="fill:#000;fill-opacity:1;fill-rule:nonzero;stroke:none" d="m 2300.19,3930.82 c 243.98,15.59 452.82,-159.49 500.57,-378.82 27.23,-125.01 9.03,-363.83 9.03,-496.06 0,-157.72 13.01,-354.7 -4.52,-500.57 -19.52,-162.43 -108.83,-281.76 -207.44,-347.26 -300.64,-199.65 -728.73,-21.61 -766.64,365.29 -17.58,179.45 0,353.34 0,532.15 0,173.93 -16.88,381.33 27.04,505.09 57.55,162.01 216.85,305.79 441.96,320.18" />
                    </g>
                </g>
            </svg>
            <div class="user-panel">
            <?php if (bungie_user()->logged_in()): ?>
                <span class="username"><span>@</span><?php xecho(bungie_user()->get_username()) ?></span>
                <a href="<?php echo SITE_URL ?>bungie-logout?cont=<?php echo safe_current_url() ?>">Logout</a>
            <?php else: ?>
                <a href="<?php echo SITE_URL ?>bungie-login?cont=<?php echo safe_current_url() ?>">Login</a>
            <?php endif; ?>
            </div>
        </div>
        -->
    </div>
</header>
<main><?php render_content_id(); ?></main>
<footer id="footer-parent">
    <div id="footer-banner">
        <div id="footer-info">
            <h3>Destiny Reddi<span>t</span></h3>
            <p>&copy; <a href="https://layer7.solutions">Layer 7 Solutions</a> 2018. This site was
            made and is maintained by the <a>/r/DestinyTheGame</a> moderation team. This site is not
            affiliated with Reddit Inc., Bungie Inc., or Activision Publishing Inc.</p>
        </div>
        <div id="footer-links">
            <?php render_template('templates.banner_links', ['link_fill' => '#fff']); ?>
        </div>
    </div>
</footer>