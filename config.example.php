<?php
// BASIC
// --------------------------------------------------------------------------------
const SITE_NAME = 'destinyreddit';
const SITE_HOST = 'destinyreddit.com';
const SITE_URL = 'https://destinyreddit.com/';
const SITE_API = 'https://destinyreddit.com/';
const SITE_TITLE = 'DestinyReddit';
const SITE_STATIC = 'https://static.layer7.solutions/';
const SITE_IS_STAGING = false;

// ASSETS HTTP LOCATIONS
// --------------------------------------------------------------------------------
const SITE_ASSETS = 'https://destinyreddit.com/assets/';
const SITE_JS = 'https://destinyreddit.com/assets/js/';
const SITE_CSS = 'https://destinyreddit.com/assets/css/';
const SITE_IMAGES = 'https://destinyreddit.com/assets/images/';

// ASSETS FILE LOCATIONS
// --------------------------------------------------------------------------------
const ASSETS_ROOT = '/var/www/staging.destinyreddit.com/public/assets/';
const JS_ROOT = '/var/www/staging.destinyreddit.com/public/assets/js/';
const CSS_ROOT = '/var/www/staging.destinyreddit.com/public/assets/css/';
const IMAGES_ROOT = '/var/www/staging.destinyreddit.com/public/assets/images/';
const APP_ROOT = '/var/www/staging.destinyreddit.com/app/';
const ROOT_PATH = '/var/www/staging.destinyreddit.com/';
const BOOTSTRAP_ROOT = '/var/www/beta.layer7.solutions/bootstrap/';

const AUTOLOADERS = [
    '/var/www/staging.destinyreddit.com/vendor/autoload.php',
];

// DEFAULT VIEWS
// --------------------------------------------------------------------------------
const VIEWS_ROOT = '/var/www/staging.destinyreddit.com/app/views/';
const VIEWS_500 = '/var/www/staging.destinyreddit.com/app/views/net/error/500.html';
const VIEWS_LAYOUT = 'net.layout';

// LAYER7
// --------------------------------------------------------------------------------
const LAYER7_SITE_NAME = 'www';
const LAYER7_SITE_ASSETS = 'https://layer7.solutions/assets/';
const LAYER7_SITE_JS = 'https://layer7.solutions/assets/js/';
const LAYER7_SITE_CSS = 'https://layer7.solutions/assets/css/';
const LAYER7_SITE_IMAGES = 'https://layer7.solutions/assets/images/';

// SECURITY
// --------------------------------------------------------------------------------
const CACERT_FILE = '/var/www/cacert.pem';

// REDDIT OAUTH
// --------------------------------------------------------------------------------
const OAUTH_AUTHORIZE_URL = 'https://ssl.reddit.com/api/v1/authorize';
const OAUTH_ACCESS_TOKEN_URL = 'https://ssl.reddit.com/api/v1/access_token';

const OAUTH_CLIENT_ID = '<REDACTED>';
const OAUTH_CLIENT_SECRET = '<REDACTED>';
const OAUTH_USER_AGENT = '<REDACTED>';
const OAUTH_REDIRECT_URL = 'http://destinyreddit.com/reddit-login';

const OAUTH_MODERATOR_USERNAME = '<REDACTED>';
const OAUTH_MODERATOR_PASSWORD = '<REDACTED>';
const OAUTH_MODERATOR_ID = '<REDACTED>';
const OAUTH_MODERATOR_SECRET = '<REDACTED>';
const OAUTH_MODERATOR_USER_AGENT = '<REDACTED>';
const OAUTH_MODERATOR_REDIRECT_URL = '<REDACTED>';

// BUNGIE OAUTH
// --------------------------------------------------------------------------------
const OAUTH_BUNGIE_AUTHORIZE_URL = 'https://www.bungie.net/en/oauth/authorize';
const OAUTH_BUNGIE_ACCESS_TOKEN_URL = 'https://www.bungie.net/platform/app/oauth/token/';

const OAUTH_BUNGIE_CLIENT_ID = '<REDACTED>';
const OAUTH_BUNGIE_CLIENT_SECRET = '<REDACTED>';
const OAUTH_BUNGIE_USER_AGENT = '<REDACTED>';
const OAUTH_BUNGIE_REDIRECT_URL = 'http://destinyreddit.com/bungie-login';

// DISCORD OAUTH
// --------------------------------------------------------------------------------
const OAUTH_DISCORD_AUTHORIZE_URL = 'https://discordapp.com/api/oauth2/authorize';
const OAUTH_DISCORD_ACCESS_TOKEN_URL = 'https://discordapp.com/api/oauth2/token';
const OAUTH_DISCORD_CLIENT_ID = '<REDACTED>';
const OAUTH_DISCORD_CLIENT_SECRET = '<REDACTED>';
const OAUTH_DISCORD_USER_AGENT = '<REDACTED>';
const OAUTH_DISCORD_REDIRECT_URL = 'http://destinyreddit.com/discord-login';

const OAUTH_DISCORDMOD_CLIENT_ID = '<REDACTED>';
const OAUTH_DISCORDMOD_CLIENT_SECRET = '<REDACTED>';
const OAUTH_DISCORDMOD_TOKEN = '<REDACTED>';
const OAUTH_DISCORDMOD_USER_AGENT = '<REDACTED>';

// DTG MAIN Guild (production):
//   (GUILD ID):                    157728722999443456
//   #channel-recruitment:          369994284067454976
//   #mod-only:                     255099898897104908
//   #admin-alerts:                 439942708702871552
//   #dtg-clan-leader-chat          498841783942381580
// DTG CLAN Guild (production)
//   (GUILD ID):                    500754541042466847
//   #clan-alerts                   508844301053067284
// Sweeper Bot Development Guild (testing)
//   (GUILD ID):                    305133671776649216
//   #all-bots-testing:             305138344021590016

const DISCORD_GUILD_ID = '157728722999443456'; // DTG MAIN
const DISCORD_CLAN_RECRUITMENT_CHANNEL_ID = '369994284067454976'; // #clan-recruitment (DTG MAIN)
const DISCORD_ADMIN_ALERTS_CHANNEL_ID = '439942708702871552'; // #admin-alerts (DTG MAIN)

const DISCORD_CLAN_GUILD_ID = '500754541042466847'; // DTG CLAN
const DISCORD_CLAN_ALERTS_CHANNEL_ID = '508844301053067284'; // #clan-alerts (DTG CLAN)

const DISCORD_MOD_ID = '328345434110164994';
const DISCORD_MOD_PING = '<@'.DISCORD_MOD_ID.'>';

const DISCORD_CLAN_MOD_ID = '500763966369431582';
const DISCORD_CLAN_MOD_PING = '<@'.DISCORD_CLAN_MOD_ID.'>';

const DISCORD_EXCLUDE_ACTIVITY_CHANNELS = [
    '169428490091757568',
    '354627968770768896',
    '354628278088237056',
    '369352134807912448',
    '369354772786184202',
    '235123818803363841',
    '235123673181192192',
    '615700834940092436',
    '615701208849580032',
    '615701271650762843',
    '615701384607825930',
    '615701424340336738',
    '615701358007287818'
];

// DATABASE
// --------------------------------------------------------------------------------
const PGSQL_SERVER = '<REDACTED>';
const PGSQL_USERNAME = '<REDACTED>';
const PGSQL_PASSWORD = '<REDACTED>';
const DB_DEFAULT_DRIVER = '<REDACTED>';

// MEMCACHED
// --------------------------------------------------------------------------------
const MEMCACHED_HOST = '<REDACTED>';
const MEMCACHED_PORT = '<REDACTED>';
const MEMCACHED_PREFIX = 'www_';

// MISC
// --------------------------------------------------------------------------------
const CSS_REPO_DIR = '<REDACTED>/dtg-css/';
