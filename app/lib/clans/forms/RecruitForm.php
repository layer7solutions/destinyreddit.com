<?php
namespace app\lib\clans\forms;

use app\lib\DiscordMod;
use app\lib\clans\Application;
use app\lib\clans\Blacklist;
use app\lib\clans\forms\AbstractForm;
use app\lib\clans\forms\RecruitFormResult;

class RecruitForm extends AbstractForm {
    const FIELDS = [
        'bungie_profile',
        'clan_name',
        'platform_pc',
        'platform_playstation',
        'platform_xbox',
        'platform_stadia',
        'RegionOption',
        'clan_desc',
        'clan_link',
    ];

    public static function test($foo, $bar) {
        echo $foo;
        var_dump($bar)."\n";
        return false;
    }

    public function generateApplication(array $data): Application {
        $checkDesc = empty($data['clan_desc']) ? '' : trim(str_replace(array("\n", "\r"), '', $data['clan_desc']));

        $data = (
            !discord_user()->logged_in()
            || empty($data)
            || empty($data = array_intersect_key($data, array_flip(self::FIELDS)))
            || count($data) !== count(self::FIELDS)
            || empty($data = AbstractForm::normalizePlatforms(['platform_pc', 'platform_playstation', 'platform_xbox', 'platform_stadia'], $data))
            || empty($data = AbstractForm::normalizeRegionOption('RegionOption', $data))
            || !AbstractForm::checkBungieLink('bungie_profile', $data)
            || !AbstractForm::checkBungieLink('clan_link', $data)
            || empty($data['clan_name'])
            || empty($checkDesc)
            || empty($data['clan_link'])
            || strlen($data['clan_name']) > 50 || strlen($data['clan_name']) < 3
            || strlen($checkDesc) > 400 || strlen($checkDesc) < 25
            || str_contains($checkDesc, 'discord.gg', true)
            || str_contains($checkDesc, 'discordapp.com/invite', true)
            || !($data['clan_id'] = AbstractForm::getClanIdFromLink($data['clan_link']))
        ) ? null : $data;

        if (empty($data))
            return Application::createInvalid(Application::TYPE_RECRUIT);

        return Application::fromData(Application::TYPE_RECRUIT, [
            'clan' => [
                'name'              => trim($data['clan_name']),
                'id'                => $data['clan_id'],
                'desc'              => trim($data['clan_desc']),
                'link'              => trim($data['clan_link']),
                'regions'           => $data['RegionOption'],
                'platforms'         => $data['platforms'],
                'is_blacklisted'    => Blacklist::clan_is_blacklisted($data['clan_id'], true),
            ],
            'application' => [
                'submission_time' => time(),
                'last_submission_time' =>
                    db('SweeperBot')->queryFirstRow(
                    'SELECT * FROM "ClanRecruitmentApps" WHERE clan_id=%s AND accepted=true
                    ORDER BY submitted_utc DESC LIMIT 1',
                        $data['clan_id'])['submitted_utc'] ?? null,
                'last_user_submission_time' =>
                    db('SweeperBot')->queryFirstRow(
                    'SELECT * FROM "ClanRecruitmentApps" WHERE d_user_id=%s AND accepted=true
                    ORDER BY submitted_utc DESC LIMIT 1',
                        discord_user()->get_id())['submitted_utc'] ?? null,
            ],
            'd_user' => discord_user()->get_info($data)
        ]);
    }

    public function processApplication(Application $app): Application {
        if (!$app->isValid()) {
            return $app;
        }

        $app->store();

        if (Application::hasPendingApp($app->type, $app->data['d_user']['id'])) {
            $app->applyResult(RecruitFormResult::ALREADY_PENDING());
            return $app;
        }

        if (!discord_user()->is_admin() && ($app->data['d_user']['activity_14d'] < 10)) {
            $app->applyResult(RecruitFormResult::DENIED_ACTIVITY());
            return $app;
        }

        if ($app->data['d_user']['is_blacklisted'] || $app->data['clan']['is_blacklisted']) {
            $app->applyResult(RecruitFormResult::BLACKLISTED());
            return $app;
        }

        if (!discord_user()->is_admin() && (time() - ($app->data['application']['last_submission_time'] ?? 0)) < 604800) {
            $app->applyResult(RecruitFormResult::COOLDOWN());
            return $app;
        }

        if (!discord_user()->is_admin() && (time() - ($app->data['application']['last_user_submission_time'] ?? 0)) < 604800) {
            $app->applyResult(RecruitFormResult::COOLDOWN());
            return $app;
        }

        if ($app->data['d_user']['actioned']['Infractions'] > 0) {
            $app->applyResult(RecruitFormResult::SENT_TO_MANUAL());
            return $app;
        }

        $app->applyResult(RecruitFormResult::ACCEPTED());
        return $app;
    }

}
