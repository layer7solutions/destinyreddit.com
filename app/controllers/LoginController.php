<?php
namespace app\controllers;

class LoginController extends \Controller {

    protected function reddit_login(array $params) {
        reddit_user()->authorize();
    }

    protected function reddit_logout(array $params) {
        reddit_user()->exit();
        redirect_cont();
    }

    protected function discord_login(array $params) {
        discord_user()->authorize();
    }

    protected function discord_logout(array $params) {
        discord_user()->exit();
        redirect_cont();
    }

    protected function bungie_login(array $params) {
        bungie_user()->authorize();
    }

    protected function bungie_logout(array $params) {
        bungie_user()->exit();
        redirect_cont();
    }

    protected function real_logout(array $params) {
        $_SESSION = [];
        session_unset();
        session_destroy();
        redirect_cont();
    }

    protected function failed(array $params) {
        if ($params['error'] == 'access_denied') {
            redirect_cont();
        }

        view('net.loginfailed')
            ->meta('Login failed', 'page--login_failed')
            ->stylesheets('inlogin')
            ->using([
                'error'         => $params['error'],
                'relogin_link'  => $params['relogin_link'],
            ])->dispatchAndExit();
    }


}