init('FlairSelector', function (opts) {
    (window.FlairSelector = {
        state: {
            previous_selected_element: null,
            disabled: opts.disabled,
            documents: map(document.querySelectorAll('.flair-table .flair-option'), function (el) {
                return {
                    name: el.getAttribute('data-name'),
                    display_name: el.getAttribute('data-displayname'),
                    category: el.getAttribute('data-category'),
                }
            }),
            search_index: null,
            category_filter: 'all',
        },
        select: function (element, flair_class) {
            if (element == this.state.previous_selected_element || this.state.disabled === true) {
                return;
            } else if (n.hasClass(element, 'flair-option-disabled')) {
                return;
            }

            if (this.state.previous_selected_element) {
                n.removeClass(this.state.previous_selected_element, 'selected');
            }

            n.value('[name=flair_class]', flair_class);
            n.html('.flair-display-cell', n.addClass(element.cloneNode(true), 'flair-option-disabled'));
            n.disabled('.flair-submit-button', false);
            n.addClass(element, 'selected');

            this.state.previous_selected_element = element;
        },
        clear_search: function () {
            this._listeners.searchbox.do_search('');
        },
        run_search: function (search_text) {
            this._listeners.searchbox.do_search(search_text || n.value('#flair-search-text'));
        },
        _listeners: {
            categoryfilter: {
                el: '#flair-category-filter',
                ev: 'change',
                fn: function (event) {
                    FlairSelector.state.category_filter = this.value;
                    FlairSelector.run_search();
                }
            },
            searchbox: {
                el: '#flair-search-text',
                ev: 'keyup',
                fn: function (event) {
                    if (this.timeout) {
                        clearTimeout(this.timeout);
                        this.timeout = null;
                    }

                    var search_text = this.value;

                    if (!FlairSelector.state.search_index) {
                        return;
                    }

                    this.timeout = setTimeout(function () {
                        FlairSelector._listeners.searchbox.do_search(search_text);
                    }, 200);
                },
                timeout: null,
                previous_result_elements: [],
                do_search: function (search_text) {
                    var table = document.querySelector('.flair-table'),
                        noRes = document.querySelector('.flair-searchbox__NoResults'),
                        query = '';

                    n.hide(noRes);
                    n.show(table);

                    // ----- Clear Previous Results
                    n.removeAttribute(this.previous_result_elements, 'style');
                    this.previous_result_elements = [];

                    // ----- Exit if no query
                    if (!search_text && FlairSelector.state.category_filter === 'all') {
                        n.removeClass(table, 'flair-search-active');
                        return;
                    }

                    // ----- Build query
                    if (search_text) {
                        query += rtrim(search_text, '*') + '^2 '
                        query += rtrim(search_text, '*') + '*' + ' '
                        query += ltrim(search_text, '*') + '~1';
                    } else {
                        query += "category:" + FlairSelector.state.category_filter;
                    }

                    // ----- Execute query
                    var results;

                    try {
                        console.log("Search: " + query);
                        results = FlairSelector.state.search_index.search(query);
                    } catch (error) {
                        results = [];
                    }

                    // ----- Show results
                    var orderInc = 0;
                    var checkCateg = FlairSelector.state.category_filter !== "all";

                    this.previous_result_elements = reduceToArray(results, function (acc, item) {
                        var element = document.getElementById('FlairOption--' + item.ref);

                        if (checkCateg && element.getAttribute('data-category') !== FlairSelector.state.category_filter) {
                            return;
                        }

                        element.setAttribute('style', 'display:inherit;order:' + (orderInc++));

                        acc.push(element);
                    });

                    if (!this.previous_result_elements.length) {
                        n.show(noRes);
                        n.hide(table);
                    } else {
                        n.addClass(table, 'flair-search-active')
                    }
                }
            },
            checkform: {
                el: '#FlairSelectorForm',
                ev: 'submit',
                fn: function (event) {
                    if (!document.getElementsByName('flair_class')[0].value.length) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                },
            },
            selectoption: {
                el: '.flair-option',
                ev: 'click',
                fn: function (event) {
                    FlairSelector.select(this, this.getAttribute('data-flairclass'));
                },
            },
        },
        init: function () {
            forEach(this._listeners, function (key, data) {
                n.on(data.el, data.ev, data.fn);
            });
            this.state.search_index = lunr(function () {
                var instance = this;

                this.ref('name');
                this.field('display_name');
                this.field('category');

                forEach(window.FlairSelector.state.documents, function (doc) {
                    instance.add(doc);
                }, this);
            });
        }
    }).init();
});