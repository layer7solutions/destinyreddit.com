<?php
namespace app\lib;

use app\lib\util\SimpleOAuthClient;

class BungieClient extends SimpleOAuthClient {

    public function __construct() {
        parent::__construct([
            // CLIENT INFO
            'prefix'            => 'bungie_',
            'client_id'         => OAUTH_BUNGIE_CLIENT_ID,
            'client_secret'     => OAUTH_BUNGIE_CLIENT_SECRET,
            'user_agent'        => OAUTH_BUNGIE_USER_AGENT,
            'grant_types'       => null,

            'custom_headers' => [
                'X-API-Key' => OAUTH_BUNGIE_API_KEY,
            ],

            // AUTH INFO
            'auth_type'         => \OAuth2Client\Client::AUTH_TYPE_AUTHORIZATION_BASIC,
            'access_token'      => null,

            // AUTH REQUESTING
            'authorize_url'     => OAUTH_BUNGIE_AUTHORIZE_URL,
            'redirect_url'      => OAUTH_BUNGIE_REDIRECT_URL,
            'access_token_url'  => OAUTH_BUNGIE_ACCESS_TOKEN_URL,

            'authorization_code_callback' => function($code) {
                return [
                    "authorization_code",
                    [
                        "code"          => $code,
                        "redirect_uri"  => OAUTH_BUNGIE_REDIRECT_URL
                    ],
                    [
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ]
                ];
            }
        ]);
    }

    public function create(array $oauth) {
        $id = $oauth['access_token_response']['membership_id'];
        $url = 'https://www.bungie.net/Platform/User/GetBungieNetUserById/'.$id.'/';
        $res = $oauth['client']->fetch($url);

        if (empty($res['result']) || empty($res['code']) || $res['code'] < 200 || $res['code'] >= 300) {
            $this->dispatch_auth_fail_page(
                "we were unable to retrieve your user info from the Bungie.net API " .
                "(API request failed)");
        } else {
            $res = $res['result'];
        }

        if (!empty($res['ErrorCode']) && $res['ErrorCode'] !== 0 && $res['ErrorCode'] !== 1) {
            $errcode = xssafe($res['ErrorCode']);
            $this->dispatch_auth_fail_page(
                "we were unable to retrieve your user info from the Bungie.net API " .
                "(API error $errcode)");
        }
        if (empty($res['Response']) || empty($res['Response']['uniqueName'])) {
            $this->dispatch_auth_fail_page(
                "we were unable to retrieve your user info from the Bungie.net API " .
                "(empty response)");
        }

        // Initialize the session
        // ~~~~~~~~~~~~~~~~~~~~~~

        session_regenerate_id();

        $this->property('id', $id);
        $this->property('logged_in', true);
        $this->property('username', from($res['Response'], 'uniqueName'));
        $this->property('uniqueName', from($res['Response'], 'uniqueName'));
        $this->property('displayName', from($res['Response'], 'displayName'));
        $this->property('userTitle', from($res['Response'], 'userTitle'));
        $this->property('userTitleDisplay', from($res['Response'], 'userTitleDisplay'));
    }

    public function is_admin() {
        if (!$this->logged_in() || empty($this->get_username())) {
            return false;
        }
        return in_array($this->get_username(), SITE_ADMINS);
    }

}