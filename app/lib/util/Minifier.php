<?php
namespace app\lib\util;

use MatthiasMullie\Minify;

class Minifier {

    /**
     * Minify some CSS.
     *
     * @return string minified CSS
     */
    public static function minify_css(string $css): string {
        $minifier = new Minify\CSS();
        $minifier->add($css);
        return $minifier->minify();
    }
}