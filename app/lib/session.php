<?php

function session_init() {
    session_configure();
    register_shutdown_function('session_write_close');
    session_start();

    if (!isset($_SESSION['token']))
        $_SESSION['token'] = base64_encode(openssl_random_pseudo_bytes(32));
}

function session_csrf_token() {
    return $_SESSION['token'] ?? 'null';
}

function session_configure() {
    if (version_compare(PHP_VERSION, '5.3.0', '>=')) {
        if (in_array('sha512', hash_algos())) {
            ini_set('session.hash_function', 'sha512');
        } else if (in_array('sha256', hash_algos())) {
            ini_set('session.hash_function', 'sha256');
        }
    } else { ini_set('session.hash_function', 1); }

    ini_set('session.hash_bits_per_character', 6);
    ini_set('session.use_trans_sid', 0);
    ini_set('session.cookie_lifetime', 0);

    if (is_ssl()) {
        ini_set('session.cookie_secure', 1); // only on https
    }

    ini_set('session.cookie_httponly', 1);
    ini_set('session.use_cookies', 1);
    ini_set('session.use_only_cookies', 1);
    ini_set('session.cache_expire', 30);
    ini_set('default_socket_timeout', 60);

    ini_set('session.entropy_file', '/dev/urandom');
    ini_set('session.entropy_length', 256);

    ini_set('session.gc_maxlifetime', 2678400);

    $session_name = session_name(SITE_NAME.'sessid');
    session_set_cookie_params(0, '/', SITE_HOST);
}