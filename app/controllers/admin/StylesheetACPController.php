<?php
namespace app\controllers\admin;

use app\lib\StylesheetCTL;
use app\controllers\admin\AdminMiscEndpointController;

class StylesheetACPController extends AdminMiscEndpointController {

    public function __construct() {
        $this->middleware(function() {
            if (!reddit_user()->is_admin()) {
                return $this->error(403);
            }
        });
    }

    protected function check_token($no_headers = false) {
        $token_received  = from($_SERVER, 'HTTP_X_CSRF_TOKEN') ?? from($_REQUEST, 'token');
        $invalid_headers =
            from($_SERVER, 'HTTP_X_REQUESTED_WITH') !== 'XMLHttpRequest' ||
            parse_url(from($_SERVER, 'HTTP_REFERER'), PHP_URL_HOST) !== SITE_HOST;

        if (($invalid_headers && !$no_headers) || empty($token_received) || $token_received === 'null') {
            return;
        }

        return $token_received === from($_SESSION, 'token');
    }

    protected function action(array $params) {
        if (!$this->check_token()) {
            die('Not authorized');
        }

        if ($params['action'] == 'new_module') {
            dispatch_html(render_to_string('admin.stylesheet_module', $_REQUEST));
        } else if ($params['action'] == 'deploy_package') {
            // CHECK IF PARAMETERS EXIST
            // ~~~~~~~~~~~~~~~~~~~~~~~~~
            if (empty($_REQUEST['package'])) {
                dispatch_json([
                    'error' => 400,
                    'error_description' => 'missing parameters',
                ], 400);
            }

            // CHECK IF PARAMETERS ARE VALID
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            $package = from($_REQUEST, 'package');
            $changes = json_decode(from($_REQUEST, 'changes'), true);
            $order   = json_decode(from($_REQUEST, 'order'), true);

            if (!isset($changes) || !isset($order)) {
                $changes = [];
                $order = [];
            }

            // SAVE CHANGES & ORDER
            // ~~~~~~~~~~~~~~~~~~~~
            $save_only = bool_unstr(from($_REQUEST, 'save_only'));
            $save_res  = StylesheetCTL::save($package, $changes, $order);
            $save_res0 = array_filter($save_res, function($item) { return $item !== null; });
            $save_res1 = array_filter($save_res0);

            if ($save_only) {
                dispatch_json([
                    'success' => count($save_res0) === count($save_res1),
                    'op' => 'save',
                ]);
            }

            if (count($save_res0) !== count($save_res1)) {
                dispatch_json([
                    'error' => 400,
                    'error_description' => 'save failure',
                ], 400);
            }

            // DEPLOY TO SUBREDDIT STYLESHEET
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            $css_only = from($_REQUEST, 'css_only') === "true";

            $result = StylesheetCTL::deploy($package, $css_only);

            if ($css_only) {
                header('Content-Type: text/css');
                echo $result;
                die;
            }

            dispatch_json([
                'success' => $result['code'] === 200,
                'op' => 'save_deploy',
                'result' => $result,
            ]);
        } else {
            dispatch_json(['query_params' => $_REQUEST]);
        }
    }

    protected function page(array $params) {
        list($mod_tree, $mod_names) = StylesheetCTL::get_modules();

        return view('admin.stylesheet')
            ->meta('Stylesheet CTL | DestinyReddit', 'page--admin page--admin_stylesheet')
            ->usinglib('zmdi')
            ->stylesheets('admin/stylesheet')
            ->scripts(['velocity', 'admin/stylesheet'])
            ->using([
                'mod_tree' => $mod_tree,
                'mod_names' => $mod_names,
            ])
            ->addInitOpt('StylesheetACP', 'module_names', $mod_names);
    }

}