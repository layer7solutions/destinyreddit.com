<?php
namespace app\lib\util;

use app\lib\clans\Application;
use app\lib\util\Message;

class HTMLResponse extends Message {

    public function __construct(string $html_content) {
        parent::__construct(null, $html_content);
    }

    public function send(Application $app = null) {
        $app->html_response = $this->body;
    }

    public static function create($html_content) {
        return new self($html_content);
    }

}