<div id="banner">
    <div id="banner__gradient"></div>
    <canvas id="banner__stars"></canvas>
    <div id="banner__wrap" class="wrap">
        <h1 id="banner__title">
            <span>Destiny</span>
            <span>&nbsp;</span>
            <span>Reddi</span>
            <span>t</span>
        </h1>
        <div id="banner__linksWrap">
            <?php render_template('templates.banner_links', ['link_fill' => '#fff']); ?>
        </div>
        <div id="banner__panels">
            <div class="panel snap-in-from-left">
                <h2><i class="icon spacer10-right fa fa-angle-right"></i>Other Chats</h2>
                <ul class="content">
                    <li><a href="https://discord.gg/DestinyReddit">Discord</a></li>
                    <li><a href="https://www.reddit.com/r/DestinyTheGame/wiki/irc">r/DestinyTheGame IRC</a></li>
                    <li><a href="https://app.orangechat.io/#/r/DestinyTheGame">Orange Chat</a></li>
                </ul>
            </div>
            <div class="panel snap-in-from-left spacer10-top" style="animation-delay: 300ms">
                <h2><i class="icon spacer10-right fa fa-angle-right"></i>Quick Links</h2>
                <ul class="content">
                    <li><a href="<?php echo SITE_URL ?>flair">Get subreddit flair</a></li>
                    <li><a href="<?php echo SITE_URL ?>clan-recruitment">Advertise <small>your</small> clan <small>on</small> DTG Discord</a></li>
                    <li><a href="<?php echo SITE_URL ?>clan-apply">Apply to the DTG Discord Clan</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="area" class="wrap">
    <div id="grid">
        <a href="https://discord.gg/DestinyReddit" class="panel promo promo--dtg_discord shine shine--on_hover shine--lighter snap-in-from-bottom">
            <div class="top-content">
                <h4>Join the official Destiny server for</h4>
                <h3>R/DESTINYTHEGAME</h3>
            </div>
            <div class="bottom-content valign">
                <h4>
                    <div>Discord</div>
                    <div>Official Partner</div>
                </h4>
                <h3>
                    <div>The largest Destiny Discord community</div>
                    <div>30,000+ Guardians</div>
                </h3>
            </div>
        </a>
        <a href="https://www.reddit.com/r/DestinyTheGame" class="panel promo promo--subreddit shine shine--on_hover snap-in-from-top">
            <div class="top-content">
                <h4>Join the largest Reddit community for Destiny at</h4>
                <h3>R/DESTINYTHEGAME</h3>
                <h5>560,000+ Guardians</h5>
            </div>
            <div class="middle-content valign">
                <svg class="icon icon--reddit" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24">
                    <path style="fill:<?php echo x_stash('link_fill'); ?>;" d="M24 11.779c0-1.459-1.192-2.645-2.657-2.645-.715 0-1.363.286-1.84.746-1.81-1.191-4.259-1.949-6.971-2.046l1.483-4.669 4.016.941-.006.058c0 1.193.975 2.163 2.174 2.163 1.198 0 2.172-.97 2.172-2.163s-.975-2.164-2.172-2.164c-.92 0-1.704.574-2.021 1.379l-4.329-1.015c-.189-.046-.381.063-.44.249l-1.654 5.207c-2.838.034-5.409.798-7.3 2.025-.474-.438-1.103-.712-1.799-.712-1.465 0-2.656 1.187-2.656 2.646 0 .97.533 1.811 1.317 2.271-.052.282-.086.567-.086.857 0 3.911 4.808 7.093 10.719 7.093s10.72-3.182 10.72-7.093c0-.274-.029-.544-.075-.81.832-.447 1.405-1.312 1.405-2.318zm-17.224 1.816c0-.868.71-1.575 1.582-1.575.872 0 1.581.707 1.581 1.575s-.709 1.574-1.581 1.574-1.582-.706-1.582-1.574zm9.061 4.669c-.797.793-2.048 1.179-3.824 1.179l-.013-.003-.013.003c-1.777 0-3.028-.386-3.824-1.179-.145-.144-.145-.379 0-.523.145-.145.381-.145.526 0 .65.647 1.729.961 3.298.961l.013.003.013-.003c1.569 0 2.648-.315 3.298-.962.145-.145.381-.144.526 0 .145.145.145.379 0 .524zm-.189-3.095c-.872 0-1.581-.706-1.581-1.574 0-.868.709-1.575 1.581-1.575s1.581.707 1.581 1.575-.709 1.574-1.581 1.574z"/>
                </svg>
                <div class="icon icon--plus"></div>
                <svg class="icon icon--tricorn" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 105.04 97.36">
                    <defs><style>.cls-1{fill:#fff;}</style></defs>
                    <path class="cls-1" d="M109.26,23.17C106.7,9.34,94.45,7.6,90.26,8.36l-.26,0a25.76,25.76,0,0,0-8.53,3.55c-6.19,4-7.92,4.47-7.92,4.47V47.06c0,6.46-6.11,16-16.63,16s-16.63-9.58-16.63-16V16.41s-1.73-.52-7.92-4.47a25.76,25.76,0,0,0-8.53-3.55l-.26,0c-4.19-.75-16.43,1-19,14.81a16.42,16.42,0,0,0,5.81,15c2.11,1.63,4,2.88,6.16,4.32a57.48,57.48,0,0,1,9.81,8.66c13,15.67,13.58,30.84,14,37.46,1.24,17.79,16.52,17,16.52,17s15.28.8,16.52-17c.46-6.62,1-21.79,14-37.46a57.48,57.48,0,0,1,9.81-8.66c2.17-1.44,4-2.68,6.16-4.32A16.42,16.42,0,0,0,109.26,23.17Z" transform="translate(-4.41 -8.21)"/>
                    <rect class="cls-1" x="41.42" y="8.59" width="22.2" height="40.81" rx="11.03" ry="11.03"/>
                </svg>
                <div class="icon icon--equals"></div>
                <div class="icon icon--heart">
                    <svg class="hexagon" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 40 46">
                        <g fill="<?php echo x_stash('link_fill'); ?>" fill-rule="nonzero">
                            <path d="M.474 34.873l19.058 11.003a.93.93 0 0 0 .936 0l19.058-11.003a.936.936 0 0 0 .467-.81V12.057a.936.936 0 0 0-.467-.81L20.468.244a.935.935 0 0 0-.936 0L.474 11.247a.935.935 0 0 0-.467.81v22.006c0 .334.178.643.467.81zm1.404-22.276L20 2.134l18.122 10.463v20.926L20 43.986 1.878 33.523V12.597z"/>
                        </g>
                    </svg>
                    <i class="fa fa-heart"></i>
                </div>
            </div>
            <div class="bottom-content">
                <button>Enter</button>
            </div>
        </a>
        <div class="area area--tweets fade-in" style="animation-delay:200ms">
            <div class="tweets__header panel">
                <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 612 612">
                    <g>
                        <path fill="#fff" d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411
                            c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513
                            c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101
                            c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104
                            c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194
                            c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485
                            c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z"/>
                    </g>
                </svg>
                <h2>Check out our Twitter</h2>
            </div>
            <?php render_template('templates.tweets', ['tweets' => u_stash('tweets')]); ?>
            <a href="https://twitter.com/DestinyReddit" class="tweets__seeMore panel">
                <span>See More</span>
            </a>
        </div>
        <a href="<?php echo SITE_URL ?>flair" class="panel promo promo-3 promo--subreddit_flair snap-in-from-bottom shine shine--on_hover"
                data-shine-element=".promo--subreddit_flair .faux-comment">
            <div class="faux-comment">
                <div class="faux-comment-inner">
                    <div class="faux-comment-flair">
                        <div class="faux-comment-flair-arrows">
                            <div class="faux-comment-flair-arrows-1"></div>
                            <div class="faux-comment-flair-arrows-2"></div>
                        </div>
                        <div class="faux-comment-flair-inner">
                            <i class="icon fa fa-diamond"></i>
                        </div>
                    </div>
                    <div class="faux-comment-body">
                        <div class="faux-comment-tagline">
                            <div class="faux-comment-tagline-username"><?php if (reddit_user()->logged_in()) {
                                xecho(reddit_user()->get_username());
                            } else {
                                echo "YourUsername";
                            }
                            ?></div>
                            <div class="faux-comment-tagline-points">31337 points</div>
                        </div>
                        <div class="faux-comment-text">
                            <div class="faux-comment-text-stripe"></div>
                            <div class="faux-comment-text-stripe"></div>
                            <div class="faux-comment-text-stripe"></div>
                        </div>
                    </div>
                </div>
            </div>
            <h2>Get subreddit flair</h2>
        </a>
        <a href="<?php echo SITE_URL ?>clan-apply" class="panel promo promo-2 promo--dtgd snap-in-from-right">
            <h3>Want to join our Discord clan?</h3>
            <h2>The Official Clan for /r/DestinyTheGame Discord</h2>
            <p>It doesn't matter if you're a newbie looking for help or a pro looking for a
            challenge; we're open to everybody who is looking to have a great time in the company
            of fantastic friends.</p>
            <button>Join Today</button>
        </a>
        <a href="<?php echo SITE_URL ?>clan-recruitment" class="panel promo promo-2 promo--clanrecruit snap-in-from-left">
            <h3>Looking to advertise your clan<br/>on our Discord?</h3>
            <h4>Click here</h4>
        </a>
        <a href="https://app.orangechat.io/#/r/DestinyTheGame" class="panel promo promo-3 promo-4 promo--orangechat">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 230.72 230.72">
                <defs><style>.cls-1{fill:#fff;stroke:#fff;stroke-miterlimit:10;stroke-width:5px;}</style></defs>
                <ellipse class="cls-1" cx="136.73" cy="21.25" rx="25.33" ry="7.8" transform="translate(25.02 102.91) rotate(-45)"/>
                <ellipse class="cls-1" cx="96.3" cy="26.91" rx="6.37" ry="15.63" transform="translate(3.19 63.35) rotate(-36.99)"/>
                <path class="cls-1" d="M115.36,49.49A89.38,89.38,0,0,0,37.5,182.74L37,182.22,38.35,223l39.57-1.35-3.31-3.19A89.37,89.37,0,1,0,115.36,49.49Z"/>
            </svg>
            <h2>orangechat</h2>
        </a>
        <a href="https://www.reddit.com/r/DestinyTheGame/wiki/irc" class="panel promo promo-3 promo-4 promo--irc">
            <i class="icon fa fa-hashtag"></i>
            <h2>IRC</h2>
        </a>
        <div class="panel promo promo-3 promo-5 promo--usefulsites">
            <ul>
                <li><a href="https://www.bungie.net/">Bungie.net</a></li>
                <li><a href="https://www.reddit.com/r/DestinyTheGame/comments/8gim21/so_you_havent_played_destiny_since_curse_of_osiris/">Returning Players Guide</a></li>
                <li><a href="https://www.bungie.net/en/Explore/Category?category=Updates">Bungie Patch Notes Index</a></li>
                <li><a href="https://www.bungie.net/en/Help/Article/46168">Destiny 2: known issues</a></li>
                <li><a href="https://www.reddit.com/r/DestinyTheGame/wiki/reportlfg">Report LFG issues</a></li>
                <li><a href="https://www.destinythegame.com/">DestinyTheGame.com</a></li>
                <li><a href="https://www.reddit.com/r/DestinyTheGame/comments/4j0m9a/best_websites_in_destiny/">And more...</a></li>
            </ul>
            <h2>Useful Links</h2>
        </div>
        <div class="panel promo promo-3 promo-5 promo--this-week">
            <div class="promo-inner promo--this-week__inner">
                <ul>
                    <li><a href="http://redd.it/926p6o">This Week at Bungie</a></li>
                    <li><a href="http://redd.it/92tjsh">This Week in /r/DestinyTheGame history</a></li>
                    <li><a href="http://redd.it/92ulmv">Daily Questions</a></li>
                    <li><a href="http://redd.it/91j7nv">Daily Reset Thread</a></li>
                </ul>
            <h2>This Week</h2>
            </div>
            <div class="promo--this-week__spacer"></div>
        </div>
        <a href="https://www.reddit.com/r/DestinyTheGame/wiki/destinylore" class="panel promo promo-3 promo--destinylore">
            <h2>Destiny Lore</h2>
        </a>
        <!--
        light blue: #009bee
        yellow: #fc0
        red: #f63b3b
        orange: #ff9000
        info-area: rgba(79,134,191,0.9)
        -->
    </div>
</div>