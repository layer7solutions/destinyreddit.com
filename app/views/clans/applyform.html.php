<div class="banner">
    <h1>
        <img class="icon" src="<?php img_src('DTG_Discord/Avatar-TransparentEdge.png') ?>"/>
        <span>Want to Join our Discord Clan?</span>
    </h1>
</div>
<aside>
    <h3>Recent Applications</h3>
    <?php if (empty(stash('previous_apps'))): ?>
    <p>None</p>
    <?php else: ?>
    <div class="ClanApplyRecentAppList">
        <?php foreach (a_stash('previous_apps') as $app): ?>
        <div class="ClanApplyRecentApp">
            <div class="ClanApplyRecentApp__header fsplit">
                <span class="ClanApplyRecentApp__id">App #<?php xecho($app['app_id']); ?></span>
                <span class="ClanApplyRecentApp__time">on <?php xecho(timeConvert($app['submitted_utc'])); ?></span>
            </div>
            <div class="ClanApplyRecentApp__status valign">
            <?php
            if ($app['in_acp']) {
                echo '<strong class="ClanApplyRecentApp__statusLabel yellow">Pending</strong>';
            } else {
                echo $app['accepted'] ?
                    '<strong class="ClanApplyRecentApp__statusLabel green">Accepted</strong>' :
                    '<strong class="ClanApplyRecentApp__statusLabel red">Denied</strong>';
                echo '<div class="ClanApplyRecentApp__statusTime">' . timeConvert($app['acp_time']) . '</div>';
            }
            ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
</aside>
<form id="ClanApplyForm" method="POST" class="form <?php if (!discord_user()->logged_in()): ?>not-logged-in<?php endif; ?>">
    <div class="form__head">
        <p>This form is for applying to the /r/DestinyTheGame Discord clan.</p>
    </div>
    <section class="form__userlogin">
        <?php if (!discord_user()->logged_in()): ?>
        <div class="field">
            <div class="field__head">
                <h3>You must identify your Discord account to submit the form.</h3>
            </div>
            <div class="field__input">
                <a class="discord-login-button"
                    href="<?php echo SITE_URL ?>discord-login?cont=/clan-apply">Confirm Your Discord</a>
            </div>
        </div>
        <?php else: ?>
        <div class="field" style="padding:8px 10px 5px">
            <h3>Welcome <?php xecho(discord_user()->get_ping()) ?>
                <a class="discord-logout-button"
                    href="<?php echo SITE_URL ?>discord-logout?cont=/clan-apply">Logout</a>
            </h3>
            <div class="field__input hide">
                <input name="discord_username" type="hidden"
                    value="<?php xecho(discord_user()->get_username()) ?>" />
                <input name="discord_id" type="hidden"
                    value="<?php xecho(discord_user()->get_id()) ?>" />
                <input name="discord_discriminator" type="hidden"
                    value="<?php xecho(discord_user()->get_discriminator()) ?>" />
            </div>
        </div>
        <?php endif; ?>
    </section>
    <?php if (is_stashed('app') && u_stash('app')->isValid() && u_stash('app')->isStored()): ?>
    <section class="form__main">
        <div class="field">
            <div class="form__submitResponse"><?php echo u_stash('app')->getHTMLResponse() ?></div>
        </div>
    </section>
    <?php else: ?>
    <section class="form__main" <?php if (!discord_user()->logged_in()): ?>style="opacity:0.5"<?php endif; ?>>
        <?php if (is_stashed('app')): ?>
        <div class="field" style="color:red">The form was not filled out correctly.</div>
        <?php endif; ?>
        <div class="field" data-for="bungie_profile">
            <div class="field__head">
                <h3>Please link us your own Bungie.net profile.</h3>
                <span>This must be your own Bungie.net profile.
                Example: https://www.bungie.net/en/Profile/1/4611686018448072979</span>
            </div>
            <div class="field__input">
                <input name="bungie_profile" type="url" <?php if (!discord_user()->logged_in()) echo "disabled" ?> />
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">This field is required</span>
                <span class="field_error" data-errname="BungieURL">Must be a Bungie.net URL.</span>
            </div>
        </div>
        <div class="field" data-for="platform_option">
            <div class="field__head">
                <h3>Which platform(s) are you applying for?</h3>
            </div>
            <div class="field__input">
                <label for="platform_pc" class="spacer-right">
                    <input type="hidden" name="platform_pc" value="">
                    <input id="platform_pc" name="platform_pc" class="platform_option"
                            type="checkbox" value="PC"
                        <?php if (!discord_user()->logged_in()) echo "disabled" ?>>
                    <span>PC</span>
                </label>
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">Must select at least one platform.</span>
            </div>
        </div>
        <div class="field" data-for="agree">
            <div class="field__input talign">
                <input type="checkbox" id="agree" name="agree" class="spacer5-top spacer5-left" value="1" />
                <label for="agree" class="spacer-left">
                Do you agree to follow the rules of the Discord and the Bungie clan, act in the best
                interests of the community, and abide by decisions made by moderators and those
                appointed by moderators? In addition do you agree that if you do not maintain an
                active presence in-game with the clan you may be removed?</label>
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">You must agree to this to be able
                to join the clan.</span>
            </div>
        </div>
        <?php if (discord_user()->logged_in()): ?>
        <div class="form__bottom">
            <input type="submit" />
        </div>
        <?php endif; ?>
    </section>
    <?php endif; ?>
</form>
