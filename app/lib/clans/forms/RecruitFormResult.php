<?php
namespace app\lib\clans\forms;

use app\lib\clans\Application;
use app\lib\clans\ApplicationEntryUpdate;
use app\lib\clans\forms\Result;
use app\lib\util\Message;
use app\lib\util\DiscordChannelMessage;
use app\lib\util\DiscordDirectMessage;
use app\lib\util\HTMLResponse;

class RecruitFormResult extends Result {

    public static function SILENT_ENQUEUE(string $acp_reason = null, array $extra = []) {
        return new RecruitFormResult(
            function(Application $app) use ($acp_reason) {
                $payload = ['in_acp' => true];

                if (!empty($acp_reason)) {
                    $payload['acp_reason'] = $acp_reason;
                }

                return [
                    new ApplicationEntryUpdate($payload),
                ];
            },
            $extra);
    }

    public static function SILENT_DEQUEUE(string $acp_reason = null, array $extra = []) {
        return new RecruitFormResult(
            function(Application $app) use ($acp_reason) {
                $payload = ['in_acp' => false];

                if (!empty($acp_reason)) {
                    $payload['acp_reason'] = $acp_reason;
                }

                return [
                    new ApplicationEntryUpdate($payload),
                ];
            },
            $extra);
    }

    public static function ACCEPTED(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new RecruitFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => true,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#6175',
                        'acp_time'      => time(),
                        'acp_reason'    => $acp_reason ?: 'Automatic validation passed.'
                    ]),
                    HTMLResponse::create(
                        "Your clan recruitment application has been accepted."
                    ),
                    DiscordChannelMessage::create(
                        DISCORD_CLAN_RECRUITMENT_CHANNEL_ID,
                        "**Name:** {$app->data['clan']['name']}\n".
                        "**Contact:** <@{$app->data['d_user']['id']}>\n".
                        "**Platforms:** {$app->data['clan']['platforms']}\n".
                        "**Region:** {$app->data['clan']['regions']}\n".
                        "**Description:** {$app->data['clan']['desc']}\n".
                        "**Link:** <{$app->data['clan']['link']}>\n".
                        "<:blank:364964889770590208>",
                        '@Sweeper Bot#6175'
                    ),
                    DiscordDirectMessage::create($app->entry['d_user_id'], $DM_message, $acp_user, true),
                ];
            },
            $extra);
    }

    public static function ACCEPTED_BY_MODERATOR(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        $acp_reason = 'Accepted by moderator' . (isset($acp_reason) ? ' ('.$acp_reason.')' : '');
        return self::ACCEPTED($acp_reason, $acp_user, $DM_message, $extra);
    }

    public static function SENT_TO_MANUAL(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new RecruitFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'in_acp' => true,
                    ]),
                    HTMLResponse::create(
                        "Your clan recruitment application has been directed to manual approval. " .
                        "Please wait 48 hours for a moderator to review your application."
                    ),
                    DiscordChannelMessage::create(
                        DISCORD_ADMIN_ALERTS_CHANNEL_ID,
                        "There's a new item in the clan-recruitment manual queue!\n" .
                        "<https://destinyreddit.com/admin/clans/recruit-queue>",
                        '@Sweeper Bot#6175'
                    ),
                    DiscordDirectMessage::create($app->entry['d_user_id'], $DM_message, $acp_user, true),
                ];
            },
            $extra);
    }

    public static function ALREADY_PENDING(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new RecruitFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => false,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#6175',
                        'acp_time'      => time(),
                        'acp_reason'    => $acp_reason ?: 'User has pending app',
                    ]),
                    HTMLResponse::create(
                        "Your clan already has a recruitment application pending moderator review."
                    ),
                    DiscordDirectMessage::create($app->entry['d_user_id'], $DM_message, $acp_user, true),
                ];
            },
            $extra);
    }

    public static function DENIED_BY_MODERATOR(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new RecruitFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => false,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#6175',
                        'acp_time'      => time(),
                        'acp_reason'    =>
                            'Denied by moderator' . (isset($acp_reason) ? ' ('.$acp_reason.')' : ''),
                    ]),
                    DiscordDirectMessage::create($app->entry['d_user_id'], $DM_message, $acp_user, true),
                ];
            },
            $extra);
    }

    public static function DENIED_ACTIVITY(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new RecruitFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => false,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#9940',
                        'acp_time'      => time(),
                        'acp_reason'    => $acp_reason ?: 'Failed activity requirement (10 messages in past 14d)',
                    ]),
                    HTMLResponse::create(
                        "<p>Sorry, but you do not meet the activity requirements.</p>

                        <p>An active member is someone who participates in discussions with other users,
                        ideally multiple times a week. We aren’t going to give specifics, but the
                        requirement isn't difficult to meet. We encourage you to interact with and get to
                        know some of the other folks around here. Engage in a handful of conversations
                        over text over the next few days outside of LFG & statsville.</p>

                        <p>We don’t count LFG as we consider people to be active server members when they
                        participate in discussions within the server, not just as an LFG site. Statsville
                        is interacting with a bot rather than other users which is why it isn't considered.
                        We do not count voice activity as there's not a reliable way to qualify/quantify
                        those interactions.</p>

                        <p>This activity ruling is put in place to ensure people are not just joining to
                        use our resources and abuse them. If you have questions please DM
                        @Sweeper Bot#6175.</p>"
                    ),
                    DiscordDirectMessage::create($app->entry['d_user_id'], $DM_message, $acp_user, true),
                ];
            },
            $extra);
    }

    public static function BLACKLISTED(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new RecruitFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => false,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#6175',
                        'acp_time'      => time(),
                        'acp_reason'    => $acp_reason ?: 'The ' .
                            ($app->data['d_user']['is_blacklisted'] ? 'discord user' : 'clan') .
                            ' is blacklisted',
                    ]),
                    HTMLResponse::create(
                        "Sorry, but your clan is not allowed to be posted. " .
                        "If you have questions please DM @Sweeper Bot#6175."
                    ),
                    DiscordDirectMessage::create($app->entry['d_user_id'], $DM_message, $acp_user, true),
                ];
            },
            $extra);
    }

    public static function COOLDOWN(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new RecruitFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => false,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#6175',
                        'acp_time'      => time(),
                        'acp_reason'    => $acp_reason ?: 'Hit rate limit (1 posting per 7d)',
                    ]),
                    HTMLResponse::create(
                        "Your clan recruitment application has been declined. " .
                        "We allow only 1 posting max per 7 days."
                    ),
                    DiscordDirectMessage::create($app->entry['d_user_id'], $DM_message, $acp_user, true),
                ];
            },
            $extra);
    }

}