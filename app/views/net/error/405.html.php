<div id="http-error" class="wrapper">
    <p id="tagline">405 Method not allowed</p>
    <p>The web server received an invalid request from the client browser.</p>
</div>
