<?php
namespace app\lib\clans;

use app\lib\clans\ApplicationEntryUpdate;
use app\lib\util\Message;
use app\lib\clans\forms\Result;

class Application {

    const TYPE_APPLY = "ClanApplyApps";
    const TYPE_RECRUIT = "ClanRecruitmentApps";

    /**
     * @var int|null
     * The unique id for this application. If this is a new application and has not been stored yet,
     * then this will be null.
     */
    public $id;

    /**
     * @var string
     * The type of this Application from either TYPE_APPLY or TYPE_RECRUIT, also doubles as the
     * name of the database table.
     */
    public $type;

    /**
     * @var array
     * The entry/row data in the database, the 'data' field will remain in stringified form within
     * this field
     */
    public $entry;

    /**
     * @var array
     * The decoded JSON of the 'data' field in `$this->entry`
     */
    public $data;

    /**
     * @var bool
     * Will be true if this application is valid, will be false if the application data fails
     * validation. Applications that fail validation should not be expected to be stored or have an
     * id.
     */
    public $isValid;

    /**
     * @var string
     * HTML response shown to user after form is submitted
     */
    public $html_response;

    private function __construct(string $type) {
        $this->id = null;
        $this->type = $type;
        $this->entry = [];
        $this->data = [];
        $this->isValid = true;
        $this->html_response = null;
    }

    // OBJECT FUNCTIONS
    // ------------------------------------------------------------------------------------------

    public function isValid(): bool {
        return $this->isValid;
    }

    public function setValid(bool $value): void {
        $this->isValid = $value;
    }

    public function isStored(): bool {
        return isset($this->id) && is_int($this->id) && $this->id >= 0;
    }

    public function getHTMLResponse(): ?string {
        return $this->html_response;
    }

    /**
     *
     * You can also directly modify the 'data' field on this `Application` object and then call
     * `store()`, but this would be faster if you're making smaller changes.
     *
     * **Note:** this method only works for existing Applications, not applications that have not
     * yet been stored.
     *
     * @param array $modData new data to merge (not overwrite) into current data
     * @return bool
     */
    public function updateData(array $modData = null): bool {
        if (empty($this->id))
            return false;

        if (isset($modData))
            $this->data = array_merge($this->data, $modData);

        db('SweeperBot')->update($this->type, array(
            'data' => json_encode($this->data)
        ), "app_id=%i", $this->id);

        return true;
    }

    /**
     * You can also directly modify the 'entry' field on this `Application` object and then call
     * `store()`, but this would be faster if you're making smaller changes.
     *
     * **Note:** this method only works for existing Applications, not applications that have not
     * yet been stored.
     *
     * @param array|ApplicationEntryUpdate $modEntry new entry to merge (not overwrite) into current entry
     * @return bool
     */
    public function updateEntry($modEntry): bool {
        if (empty($this->id))
            return false;

        if ($modEntry instanceof ApplicationEntryUpdate) {
            $modEntry = $modEntry->entry;
        }

        if (!is_array($modEntry) || empty($modEntry)) {
            return false;
        }

        $this->entry = array_merge($this->entry, $modEntry);

        db('SweeperBot')->update($this->type, $modEntry, "app_id=%i", $this->id);

        return true;
    }

    /**
     * Stores the application in the database and returns the app id and updates the `id` field
     * within this `Application` object. If the application has not been stored yet, a new row/entry
     * will be inserted, otherwise the existing application row/entry will be updated.
     *
     * @return int the app id for this application, the primary key in the database
     */
    public function store(): int {
        $isFirstStore = !isset($this->id);

        $this->entry['data'] = json_encode($this->data);

        if ($isFirstStore) {
            $this->entry['submitted_utc'] = time();

            if ($this->type === self::TYPE_APPLY) {
                $this->entry['d_user_id'] = $this->data['d_user']['id'];
            } else if ($this->type === self::TYPE_RECRUIT) {
                $this->entry['d_user_id'] = $this->data['d_user']['id'];
                $this->entry['clan_id'] = $this->data['clan']['id'];
            }
        }


        if ($isFirstStore) {
            db('SweeperBot')->insert($this->type, $this->entry);
            $this->id = intval(db('SweeperBot')->insertId());
        } else {
            db('SweeperBot')->insertUpdate($this->type, $this->entry);
        }

        return $this->id;
    }

    public function resyncFromDatabase() {
        if (!isset($this->id)) {
            return false;
        }

        $this->entry = db('SweeperBot')->queryFirstRow('SELECT * FROM %b WHERE app_id=%s',
            $this->type, $this->id);

        if (empty($this->entry)) {
            $this->setValid(false);
        } else {
            $this->data = json_decode($this->entry['data'], true);
        }

        return true;
    }

    public function applyResult(?Result $result) {
        if (empty($result))
            return;

        $this->_applyResultItems($result->items);
    }

    private function _applyResultItems(array $resultItems) {
        foreach ($resultItems as $item) {
            if (is_callable($item))
                $item = $item($this);

            if ($item instanceof Message) {
                $item->send($this);
            } else if ($item instanceof ApplicationEntryUpdate) {
                $this->updateEntry($item);
            } else if (is_array($item)) {
                $this->_applyResultItems($item);
            }
        }
    }

    // STATIC FUNCTIONS
    // ------------------------------------------------------------------------------------------

    public static function createInvalid(string $type, array $data = []): Application {
        $app = new self($type);
        $app->data = $data;
        $app->setValid(false);
        return $app;
    }

    public static function fromData(string $type, array $data): Application {
        $app = new self($type);
        $app->data = $data;
        return $app;
    }

    public static function fromId(string $type, string $id): Application {
        $app = new self($type);
        $app->id = $id;
        $app->resyncFromDatabase();
        return $app;
    }

    public static function getApps(string $type, string $user_id) {
        return db('SweeperBot')->query('SELECT * FROM %b WHERE d_user_id=%s ORDER BY submitted_utc DESC', $type, $user_id);
    }

    public static function hasPendingApp(string $type, string $user_id) {
        return 0 !== db('SweeperBot')->queryFirstField(
            'SELECT count(*) FROM %b WHERE in_acp=true AND d_user_id=%s', $type, $user_id);
    }
}