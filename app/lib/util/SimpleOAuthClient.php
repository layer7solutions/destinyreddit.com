<?php
namespace app\lib\util;

abstract class SimpleOAuthClient_AbstractStorage {
    protected $prefix;

    public function __construct(string $prefix = null) {
        $this->prefix = $prefix ?: "";
    }

    public abstract function get_all();
    public abstract function get_property(string $prop);
    public abstract function has_property(string $prop);
    public abstract function set_property(string $prop, $value);
    public abstract function exit();
}

class SimpleOAuthClient_SessionStorage extends SimpleOAuthClient_AbstractStorage {
    public function __construct(string $prefix = null) {
        parent::__construct($prefix);
    }
    public function get_property(string $prop) {
        return from($_SESSION, $this->prefix . $prop);
    }
    public function has_property(string $prop) {
        return isset($_SESSION[$this->prefix . $prop]);
    }
    public function set_property(string $prop, $value) {
        $_SESSION[$this->prefix . $prop] = $value;
    }
    public function get_all() {
        $ret = [];
        foreach ($_SESSION as $key => $value) {
            if (startsWith($key, $this->prefix)) {
                $ret[remove_prefix($key, $this->prefix)] = $value;
            }
        }
        return $ret;
    }
    public function exit() {
        // Remove SESSION (persistent storage) variables whose keys that match our prefix
        foreach ($_SESSION as $key => $value) {
            if (startsWith($key, $this->prefix)) {
                unset($_SESSION[$key]);
            }
        }
        session_regenerate_id();
    }
}

class SimpleOAuthClient_MemoryStorage extends SimpleOAuthClient_AbstractStorage {
    protected $data = [];

    public function __construct(string $prefix = null) {
        parent::__construct($prefix);
    }
    public function get_property(string $prop) {
        return from($this->data, $prop);
    }
    public function has_property(string $prop) {
        return isset($this->data[$prop]);
    }
    public function set_property(string $prop, $value) {
        $this->data[$prop] = $value;
    }
    public function get_all() {
        return $this->data;
    }
    public function exit() {
        return; // no cleanup necessary (memory is not persistent)
    }
}

/**
 * SimpleOAuthClient: handles authorization and access token retrieval for OAuth; can support
 * multiple OAuth client instances in a single PHP session with the meta['prefix] property which
 * will be used to prefix all $_SESSION variables created in order to prevent conflicting with other
 * session variables.
 *
 * Internally it's actually a little complicated but it's called "simple" because
 * it's meant to be simple to use.
 */
abstract class SimpleOAuthClient {
    /**
     * @var array
     */
    protected $meta;
    /**
     * @var array
     */
    protected $cache = [];
    /**
     * @var SimpleOAuthClient_AbstractStorage
     */
    protected $store;

    /**
     * Construct the SimpleOAuthClient.
     */
    public function __construct(array $meta, SimpleOAuthClient_AbstractStorage $store=null) {
        $this->meta = $meta + [
            // ~~~ DEFAULT VALUES FOR META ~~~

            // CLIENT INFO
            'prefix'                        => null,
            'client_id'                     => null,
            'client_secret'                 => null,
            'user_agent'                    => null,
            'grant_types'                   => null,
            'custom_headers'                => [],

            // AUTH INFO
            'auth_type'                     => \OAuth2Client\Client::AUTH_TYPE_AUTHORIZATION_BASIC,
            'access_token'                  => null,
            'access_token_type'             => null,
            'scopes'                        => null,
            'duration'                      => null,

            // AUTH REQUESTING
            'authorize_url'                 => null,
            'redirect_url'                  => null,
            'access_token_url'              => null,
            'access_token_callback'         => null,
            'authorization_code_callback'   => null,
        ];
        $this->store = $store ?: new SimpleOAuthClient_SessionStorage($this->meta['prefix']);
    }

    // PROPERTIES
    // ------------------------------------------------------------------------------------------

    public function exit() {
        $this->store->exit();
    }

    public function properties() {
        return $this->store->get_all();
    }

    public function property(string $prop, $value = null) {
        if (func_num_args() == 2) {
            $this->store->set_property($prop, $value);
            return $value;
        } else {
            return $this->store->get_property($prop);
        }
    }

    public function has_property(string $prop): bool {
        return $this->store->has_property($prop);
    }

    // PAGE VISIT PROPERTIES
    // ------------------------------------------------------------------------------------------

    public function visit_property(string $prop, $value = null) {
        if (func_num_args() == 2) {
            $this->cache[$prop] = $value;
        } else {
            return from($this->cache, $prop);
        }
    }

    public function has_visit_property(string $prop): bool {
        return isset($this->cache[$prop]);
    }

    // GENERAL STATE
    // ------------------------------------------------------------------------------------------

    public function logged_in() {
        if (isset($this->meta['access_token'])) {
            return true;
        }
        return $this->property('logged_in') === true;
    }

    public function get_username() {
        return $this->property('username');
    }

    // AUTHORIZATION
    // ------------------------------------------------------------------------------------------

    protected abstract function create(array $oauth);

    public function authorize() {
        if ($this->logged_in()) {
            return;
        }

        if (!isset($this->meta['authorize_url']) || !isset($this->meta['redirect_url'])
                || !isset($this->meta['redirect_url'])) {
            throw new LogicException();
        }

        if (isset($_REQUEST['error'])) {
            $this->dispatch_auth_fail_page(strval($_REQUEST['error']));
        }

        if (isset($_REQUEST['cont'])) {
            set_cookie('cont_after_login', strval($_REQUEST['cont']), 3600);
        }

        // 1. Check if we have the authorization code
        //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        if (!isset($_REQUEST["code"])) {
            $params = [
                "state"     => safe_token(),
                "duration"  => $this->meta['duration'] ?? "permanent",
            ];

            if (!empty($this->meta['scopes'])) {
                if (is_array($this->meta['scopes'])) {
                    $params['scope'] = implode(' ', $this->meta['scopes']);
                } else {
                    $params['scope'] = $this->meta['scopes']; // assume string
                }
            }

            redirect_ext($this->createOAuthClientObject()['client']->getAuthenticationUrl(
                $this->meta['authorize_url'],
                $this->meta['redirect_url'],
                $params
            ));
            exit;
        }

        // 2. Retrieve access and refresh token w/ authorization code
        //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        $access_token_getopts = call_user_func_array(
            $this->meta['authorization_code_callback'], array($_REQUEST["code"]));

        $oauth = $this->createOAuthClientObject($access_token_getopts);

        if (empty($oauth['client']) || empty($oauth['access_token']) || empty($oauth['refresh_token'])) {
            $this->dispatch_auth_fail_page(
                "We were unable to retrieve authentication tokens from Discord. Try again later.");
        }

        // 3. Initialize the user session
        //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~

        $this->create($oauth);

        // 4. Redirect the user to where they want to go
        //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        $cont = get_cookie('cont_after_login');

        if (!empty($cont)) {
            delete_cookie('cont_after_login');
            redirect($cont);
        }

        redirect();
    }

    // TOKENS
    // ------------------------------------------------------------------------------------------

    public function getAccessToken() {
        if (!$this->logged_in()) {
            return null;
        }

        if (isset($this->meta['access_token'])) {
            return $this->meta['access_token'];
        }

        $tok = strval($this->property('access_token'));
        $exp = intval($this->property('access_token_expiry'));

        if (empty($tok) || $exp < time()) {
            // return null if expired
            return null;
        }

        return $tok;
    }

    public function get_client() {
        if (isset($this->cache['client'])) {
            return $this->cache['client'];
        }

        if (!$this->logged_in()) {
            return null;
        }

        return $this->cache['client'] = $this->refresh();
    }

    public function refresh() {
        $access_token = $this->getAccessToken();

        if (!empty($access_token)) {
            $oauth = $this->createOAuthClientObject($access_token);
            $this->on_refresh($oauth);
            return $oauth['client'];
        }

        if (!$this->has_property('refresh_token')) {
            $this->exit();
            return null;
        }

        $oauth = $this->createOAuthClientObject([
            'refresh_token',
            [
                "refresh_token" => $this->property('refresh_token'),
            ]
        ]);

        $this->property('access_token', $oauth['access_token']);
        $this->property('access_token_expiry', $oauth['expiry'] ?? (time() + 3300));
        $this->on_refresh($oauth);

        return $oauth['client'];
    }

    public function on_refresh($oauth) {
        return;
    }

    // HELPER FUNCTIONS
    // ------------------------------------------------------------------------------------------

    public function dispatch_auth_fail_page(string $error) {
        \App::runController('Login:failed', [
            'error' => $error,
            'relogin_link' => $this->meta['redirect_url'],
        ]);
    }

    /**
     * A helper function for creating the OAuth2 Client object.
     *
     * @param string|array  $access_token_getopts string for the acess token itself or an array
     *                      in the format [$grant_type, array $parameters, array $extra_headers]
     * @return array        ['client' => \OAuth2Client\Client,
     *                       'access_token' => string,
     *                       'refresh_token' => string,
     *                       'expiry' => int,
     *                       'token_type' => string
     *                      ]
     */
    protected function createOAuthClientObject($access_token_getopts = null): array {
        if (isset($this->meta['grant_types']))
            autoload_oauth2_client($this->meta['grant_types']);
        else
            autoload_oauth2_client();

        $client = new \OAuth2Client\Client(
            $this->meta['client_id'],
            $this->meta['client_secret'],
            $this->meta['auth_type'],
            CACERT_FILE
        );
        $client->setCurlOption(CURLOPT_USERAGENT, $this->meta['user_agent']);

        if (!empty($this->meta['custom_headers']))
            $client->setCustomHeaders($this->meta['custom_headers']);

        $result = [
            'client' => $client,
            'access_token' => null,
            'refresh_token' => null,
            'expiry' => null,
            'token_type' => null,
        ];

        if (empty($access_token_getopts)) {
            return $result;
        }

        if (is_string($access_token_getopts)) {
            // if $access_token_getopts is a string, then assume it is the actual access token
            $result['access_token'] = $access_token_getopts;
        }

        else if (is_array($access_token_getopts) && isset($this->meta['access_token_url'])) {
            $response = $client->retrieveAccessToken($this->meta['access_token_url'], ... $access_token_getopts);

            if (empty($response['result']['access_token'])) {
                return $result;
            }

            // The response should always have the access_token, whether or not it has the
            // refresh_token depends on the grant type passed through $access_token_getopts:
            // 'authorization_code' will respond with an access token and 'refresh_token' will not.

            $result['access_token'] = $response['result']['access_token'];
            $result['refresh_token'] = $response['result']['refresh_token'] ?? null;
            $result['expiry'] = $response['result']['expires_in'] ?? null;
            $result['token_type'] = $response['result']['token_type'];
            $result['access_token_response'] = $response['result'];

            if (isset($result['expiry'])) {
                $result['expiry'] = time() + $result['expiry'];
            }

            $this->property('refresh_token', $result['refresh_token']);
            $this->property('access_token', $result['access_token']);
            $this->property('access_token_expiry', $result['expiry'] ?? (time() + 3300));

            if (isset($this->meta['access_token_callback'])) {
                if (is_callable($this->meta['access_token_callback'])) {
                    call_user_func_array($this->meta['access_token_callback'], [$result]);
                }
            }
        }

        if (isset($result['access_token'])) {
            $client->setAccessToken($result['access_token']);
            $client->setAccessTokenType($result['token_type'] ?: $this->meta['access_token_type']);
        }

        return $result;
    }

}