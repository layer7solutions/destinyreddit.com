<?php
namespace app\controllers;

class HomepageController extends \Controller {
    const TWEETS_CACHE_KEY = 'latest_destinyreddit_tweets_10';
    const TWEETS_CACHE_TIME = 1800; // 30 minutes in seconds
    const TWEETS_CACHE_ENABLED = true;

    protected function default_action(array $params) {
        $tweets = $this->get_tweets();

        // get profile image URL to add to CSP
        $avi_url = isset($tweets[0]) ? $tweets[0]['user']['profile_image_url_https'] : [];

        return view('net.homepage')
            ->meta('DestinyReddit', 'page--homepage no-js')
            ->stylesheets('homepage')
            ->usinglib('jquery')
            ->scripts(['jquery-easing', 'jquery-vgrid', 'homepage'])
            ->addCSP('img-src', $avi_url)
            ->usinglib('fontawesome')
            ->addInitOpt('Homepage_BannerStars', 'num_stars', 115)
            ->using([
                'tweets' => $tweets,
            ]);
    }

    public function expire_and_fetch_tweets() {
        cached_delete(self::TWEETS_CACHE_KEY); // expire
        $this->get_tweets(); // re-fetch
    }

    protected function get_tweets() {
        if (self::TWEETS_CACHE_ENABLED && cached_exists(self::TWEETS_CACHE_KEY)) {
            return cached(self::TWEETS_CACHE_KEY);
        }

        $tweets = $this->fetch_tweets();

        if ($tweets === false || !is_array($tweets)) {
            return [];
        }

        cached(self::TWEETS_CACHE_KEY, $tweets, self::TWEETS_CACHE_TIME);

        return $tweets;
    }

    protected function fetch_tweets() {
        $twitter = new \TwitterAPIExchange([
            'oauth_access_token'        => TWITTER_ACCESS_TOKEN,
            'oauth_access_token_secret' => TWITTER_ACCESS_TOKEN_SECRET,
            'consumer_key'              => TWITTER_CONSUMER_KEY,
            'consumer_secret'           => TWITTER_CONSUMER_SECRET
        ]);

        $tw_user = 'DestinyReddit';
        $tw_count = 5;

        $json = json_decode($twitter
            ->setGetfield("?screen_name={$tw_user}&count={$tw_count}")
            ->buildOauth("https://api.twitter.com/1.1/statuses/user_timeline.json", 'GET')
            ->performRequest(true, [
                CURLOPT_SSL_VERIFYHOST => 2,
                CURLOPT_SSL_VERIFYPEER => 1,
                CURLOPT_CAINFO => CACERT_FILE
            ]), true);

        if (empty($json) || !is_array($json) || empty($json[0]) || isset($json['errors'][0]['message'])) {
            return false;
        }

        $tweets = [];

        // get only information we want to minimize amount of stuff to be cached
        foreach ($json as $tweet) {
            unset($tweet['in_reply_to_status_id']);
            unset($tweet['in_reply_to_status_id_str']);
            unset($tweet['in_reply_to_user_id']);
            unset($tweet['in_reply_to_user_id_str']);

            $tweet['user'] = [
                'name'                      => $tweet['user']['name'],
                'screen_name'               => $tweet['user']['screen_name'],
                'location'                  => $tweet['user']['location'],
                'description'               => $tweet['user']['description'],
                'followers_count'           => $tweet['user']['followers_count'],
                'created_at'                => $tweet['user']['created_at'],
                'profile_image_url_https'   => $tweet['user']['profile_image_url_https'],
            ];

            unset($tweet['geo']);
            unset($tweet['coordinates']);
            unset($tweet['place']);
            unset($tweet['contributors']);
            unset($tweet['is_quote_status']);
            unset($tweet['favorited']);
            unset($tweet['retweeted']);
            unset($tweet['possibly_sensitive']);
            unset($tweet['lang']);

            if (!empty($tweet['entities']['urls']) || !empty($tweet['entities']['user_mentions'])) {
                $html = xssafe($tweet['text']);

                foreach ($tweet['entities']['urls'] as $url) {
                    if (!isset($url['url'], $url['display_url'], $url['expanded_url'])) {
                        continue;
                    }

                    $html = str_replace(xssafe($url['url']),
                        '<a target="_blank" rel="noopener noreferrer" '.
                            'href="'.encodeURI($url['expanded_url']).'">'
                            . xssafe($url['display_url']) . '</a>', $html);
                }

                foreach ($tweet['entities']['user_mentions'] as $mention) {
                    if (!isset($mention['screen_name'])) {
                        continue;
                    }

                    $html = str_replace('@'.$mention['screen_name'],
                        '<a target="_blank" rel="noopener noreferrer" '.
                            'href="https://twitter.com/' . xssafe($mention['screen_name']) . '">@'
                            . encodeURI($mention['screen_name']).'</a>', $html);
                }

                $tweet['safe_html'] = $html;
            } else {
                $tweet['safe_html'] = xssafe($tweet['text']);
            }

            $tweet['created_timestamp'] = strtotime($tweet['created_at']);
            $tweet['safe_html'] = str_replace('\n', '<br/>', $tweet['safe_html']);

            unset($tweet['entities']);
            $tweets[] = $tweet;
        }

        return $tweets;
    }

}