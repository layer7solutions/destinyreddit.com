<?php
namespace app\lib;

use app\lib\util\SimpleOAuthClient;
use app\lib\util\DiscordDataPulls;

class DiscordClient extends SimpleOAuthClient {

    public function __construct() {
        parent::__construct([
            // CLIENT INFO
            'prefix'            => 'discord_',
            'client_id'         => OAUTH_DISCORD_CLIENT_ID,
            'client_secret'     => OAUTH_DISCORD_CLIENT_SECRET,
            'user_agent'        => OAUTH_DISCORD_USER_AGENT,
            'grant_types'       => null,

            // AUTH INFO
            'auth_type'         => \OAuth2Client\Client::AUTH_TYPE_FORM,
            'access_token'      => null,
            'scopes'            => 'identify', //'identify guilds.join',
            'duration'          => 'permanent',

            // AUTH REQUESTING
            'authorize_url'     => OAUTH_DISCORD_AUTHORIZE_URL,
            'redirect_url'      => OAUTH_DISCORD_REDIRECT_URL,
            'access_token_url'  => OAUTH_DISCORD_ACCESS_TOKEN_URL,

            'authorization_code_callback' => function($code) {
                return [
                    "authorization_code",
                    [
                        "code"          => $code,
                        "redirect_uri"  => OAUTH_DISCORD_REDIRECT_URL
                    ],
                    [
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ]
                ];
            },
        ]);
    }

    protected function create(array $oauth) {
        // Retrieve Discord username
        // ~~~~~~~~~~~~~~~~~~~~~~~~~
        $userdata = $oauth['client']->fetch("https://discordapp.com/api/users/@me");

        if (!isset($userdata['result']['id']) || !isset($userdata['result']['username'])) {
            $this->dispatch_auth_fail_page("we were unable to retrieve your username/id from the Discord API");
        }

        // Initialize the session
        // ~~~~~~~~~~~~~~~~~~~~~~

        session_regenerate_id();

        $this->property('id', $userdata['result']['id']);
        $this->property('username', $userdata['result']['username']);
        $this->property('discriminator', $userdata['result']['discriminator']);
        $this->property('logged_in', true);
        $this->property('is_admin', null);

        $this->on_refresh($oauth);
    }

    public function on_refresh($oauth) {
        db('SweeperBot')->insertUpdate("DiscordUserTokens", [
            "UserID" => $this->get_id(),
            "RefreshToken" => $this->property('refresh_token'),
            "AccessToken" => $this->property('access_token'),
            "AccessTokenExpiry" => $this->property('access_token_expiry'),
        ]);
    }

    /**
     * Get the discord ID for this user.
     * @return string
     */
    public function get_id(): ?string {
        return $this->property('id');
    }

    /**
     * Get the discord descriminator for this user.
     * @return string
     */
    public function get_discriminator(): ?string {
        return $this->property('discriminator');
    }

    /**
     * Get discord ping string for this user.
     * @return string
     */
    public function get_ping(): string {
        return  '@' . $this->get_username() . '#' . $this->get_discriminator();
    }

    /**
     * Get discord user info related to our /r/DTG guilds.
     *
     * @return array associative array of property to value
     */
    public function get_info(array $data = []): array {
        return DiscordDataPulls::get_user_info($data);
    }

    /**
     * Check if this discord user is in a certain server.
     *
     * @return bool true if in server, false otherwise
     */
    public function in_server(string $serverId): bool {
        if (!$this->logged_in() || empty($this->get_username())) {
            return false;
        }
        if ($this->has_visit_property('in_server_'.$serverId)) {
            return $this->visit_property('in_server_'.$serverId);
        }

        $state = discord_mod()->in_server($serverId, $this->get_id());
        $this->visit_property('in_server_'.$serverId, $state);
        return $state;
    }

    /**
     * Check if this discord user is mod on main/general or clan server.
     *
     * @return bool true if admin, false otherwise
     */
    public function is_admin() {
        return $this->is_general_admin() || $this->is_clan_admin();
    }

    /**
     * Check if this discord user is mod on main/general server.
     *
     * @return bool true if admin, false otherwise
     */
    public function is_general_admin(): bool {
        if (!$this->logged_in() || empty($this->get_username())) {
            return false;
        }
        if ($this->has_visit_property('is_admin')) {
            return $this->visit_property('is_admin');
        }

        // $state = (true === db('SweeperBot')->queryFirstField(
        //     'SELECT is_mod FROM "Users" WHERE "UserID"=%s_usrid AND "ServerID"=%s_srvid',
        //     [
        //         'usrid' => $this->get_id(),
        //         'srvid' => DISCORD_GUILD_ID
        //     ]
        // ));
        $a = db('SweeperBot')->queryFirstField(
            'SELECT COUNT(*) FROM serveradminrels WHERE "user_id"=(select id from "user" where discord_id=%l) AND "server_id"=(select id from "server" where discord_id=%l)', $this->get_id(), DISCORD_GUILD_ID) || 0;
        $b = db('SweeperBot')->queryFirstField(
            'SELECT COUNT(*) FROM servermodrels WHERE "user_id"=(select id from "user" where discord_id=%l) AND "server_id"=(select id from "server" where discord_id=%l)', $this->get_id(), DISCORD_GUILD_ID) || 0;

        $state = $a > 0 || $b > 0;

        $this->visit_property('is_admin', $state);

        return $state;
    }

    /**
     * Check if this discord user is mod on clan server.
     *
     * @return bool true if admin, false otherwise
     */
    public function is_clan_admin(): bool {
        if (!$this->logged_in() || empty($this->get_username()))
            return false;
        if ($this->get_id() === '171510147393257472') { // kwwxis (dev bypass)
            return true;
        }
        return $this->in_server(DISCORD_CLAN_GUILD_ID)
            && $this->has_role(DISCORD_CLAN_GUILD_ID, DISCORD_CLAN_MOD_ID);
    }

    /**
     * Get list of roles this Discord user has in the specified guild.
     *
     * @param string $guild_id
     * @return array
     */
    public function get_roles(string $guild_id): array {
        if ($this->has_visit_property('discord_roles_'.$guild_id)) {
            return $this->visit_property('discord_roles_'.$guild_id);
        }

        $roles = discord_mod()->get_roles($guild_id, $this->get_id());
        $this->visit_property('discord_roles_'.$guild_id, $roles);
        return $roles;
    }

    /**
     * Check if the Discord user has a specific role in the specified guild.
     *
     * @param string $guild_id
     * @param string $role_id
     * @return bool
     */
    public function has_role(string $guild_id, string $role_id): bool {
        return in_array($role_id, $this->get_roles($guild_id));
    }

    // /**
    //  * Get the date the Discord user joined the specified guild and left it. The return result will
    //  * always be 2-element array, the first element being the join UNIX timestamp and the second
    //  * element being the part (leave) UNIX timestamp. If either element is not applicable, then it
    //  * will be null.
    //  *
    //  * @param string $guild_id
    //  * @return int[]
    //  */
    // public function get_joinpart(string $guild_id): array {
    //     if ($this->has_visit_property('joinpart_'.$guild_id)) {
    //         return $this->visit_property('joinpart_'.$guild_id);
    //     }

    //     $joinpart = discord_mod()->get_user_joinpart($guild_id, $this->get_id());
    //     $this->visit_property('joinpart_'.$guild_id, $joinpart);
    //     return $joinpart;
    // }

    /**
     * Get the UNIX timestamp of when the Discord user joined the specified guild, or returns null
     * if not applicable.
     *
     * Note: this is the identical to the first element of the return result of get_joinpart()
     *
     * @param string $guild_id
     * @param int|null
     */
    public function get_join_timestamp(string $guild_id): ?int {
        return $this->get_joinpart($guild_id)[0];
    }

    /**
     * Get the UNIX timestamp of when the Discord user left the specified guild, or returns null
     * if not applicable.
     *
     * Note: this is the identical to the second element of the return result of get_joinpart()
     *
     * @param string $guild_id
     * @param int|null
     */
    public function get_part_timestamp(string $guild_id): ?int {
        return $this->get_joinpart($guild_id)[1];
    }

}