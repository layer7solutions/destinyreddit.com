<?php
namespace app\lib\clans\forms;

use app\lib\clans\Application;
use app\lib\clans\ApplicationEntryUpdate;
use app\lib\clans\forms\Result;
use app\lib\util\Message;
use app\lib\util\DiscordChannelMessage;
use app\lib\util\DiscordDirectMessage;
use app\lib\util\HTMLResponse;

class ApplyFormResult extends Result {

    public static function SILENT_ENQUEUE(string $acp_reason = null, array $extra = []) {
        return new ApplyFormResult(
            function(Application $app) use ($acp_reason) {
                $payload = ['in_acp' => true];

                if (!empty($acp_reason)) {
                    $payload['acp_reason'] = $acp_reason;
                }

                return [
                    new ApplicationEntryUpdate($payload),
                ];
            },
            $extra);
    }

    public static function SILENT_DEQUEUE(string $acp_reason = null, array $extra = []) {
        return new ApplyFormResult(
            function(Application $app) use ($acp_reason) {
                $payload = ['in_acp' => false];

                if (!empty($acp_reason)) {
                    $payload['acp_reason'] = $acp_reason;
                }

                return [
                    new ApplicationEntryUpdate($payload),
                ];
            },
            $extra);
    }

    public static function ACCEPTED(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new ApplyFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => true,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#6175',
                        'acp_time'      => time(),
                        'acp_reason'    => $acp_reason ?: 'Automatic validation passed.'
                    ]),
                    DiscordDirectMessage::create(
                        $app->entry['d_user_id'],
                        $DM_message ?:
                        "Hey, your application to join the DTG Reddit Discord clan has been accepted and " .
                        "you'll receive an invite via Bungie.net within 48 hours. In addition we also have " .
                        "our own Discord Server to coordinate events. **Please join the server here:** " .
                        "https://discord.gg/7bJjTqR.\n\n" .

                        "If you have any questions please reply to this message and it'll get to our" .
                        "Mod Team.",
                        empty($DM_message) ? '@Sweeper Bot#6175' : $acp_user
                    ),
                ];
            },
            $extra);
    }

    public static function ACCEPTED_BY_MODERATOR(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        $acp_reason = 'Accepted by moderator' . (isset($acp_reason) ? ' ('.$acp_reason.')' : '');
        return self::ACCEPTED($acp_reason, $acp_user, $DM_message, $extra);
    }

    public static function SENT_TO_MANUAL(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new ApplyFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'in_acp' => true,
                    ]),
                    HTMLResponse::create(
                        "We've received your request to join the DTG Reddit Discord clan. " .
                        "Your application is currently in our queue for manual processing which will " .
                        "happen within the next 7 days. <b>While you are waiting for your application to " .
                        "be processed please join our clan server here: " .
                        "<a href=\"https://discord.gg/7bJjTqR\">https://discord.gg/7bJjTqR</a>.</b> " .
                        "This is required so that we can let you know when your application has been " .
                        "processed. After it has been approved, you will be given the Clan Member role " .
                        "and full access to the server. If you have any questions, please send " .
                        "@Sweeper Bot#6175 a message and it'll be sent to our Mod Team."
                    ),
                    DiscordDirectMessage::create(
                        $app->entry['d_user_id'],
                        $DM_message ?:
                        "Hey there. We've received your request to join the DTG Reddit Discord clan. " .
                        "Your application is currently in our queue for manual processing which will " .
                        "happen within the next 7 days. **While you are waiting for your application to " .
                        "be processed please join our clan server here: https://discord.gg/7bJjTqR.** " .
                        "This is required so that we can let you know when your application has been " .
                        "processed. After it has been approved, you will be given the Clan Member role " .
                        "and full access to the server. If you have any questions, please send " .
                        DISCORD_MOD_PING ." a message and it'll be sent to our Mod Team.",
                        empty($DM_message) ? '@Sweeper Bot#6175' : $acp_user
                    ),
                    DiscordChannelMessage::create(
                        DISCORD_CLAN_ALERTS_CHANNEL_ID,
                        "There's a new request to join the DTG Discord Clan.\n" .
                        "\n" .
                        "**Applicant:** <@{$app->data['d_user']['id']}>\n" .
                        "**Platform(s):** {$app->data['application']['platforms']}\n" .
                        "**Profile:** {$app->data['d_user']['bungie_profile']}\n" .
                        "\n" .
                        "<https://destinyreddit.com/admin/clans/apply-queue>\n" .
                        "<:blank:364964889770590208>",
                        '@Sweeper Bot#6175'
                    ),
                ];
            },
            $extra);
    }

    public static function ALREADY_PENDING(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new ApplyFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => false,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#6175',
                        'acp_time'      => time(),
                        'acp_reason'    => $acp_reason ?: 'User has pending app',
                    ]),
                    HTMLResponse::create(
                        "You've already submitted an application that is still pending moderator review. " .
                        "If you have any questions please DM @Sweeper Bot#6175."
                    ),
                    DiscordDirectMessage::create(
                        $app->entry['d_user_id'],
                        $DM_message ?:
                        "You've already submitted an application that is still pending moderator review. " .
                        "If you have any questions please DM ".DISCORD_MOD_PING.".",
                        empty($DM_message) ? '@Sweeper Bot#6175' : $acp_user
                    ),
                ];
            },
            $extra);
    }

    public static function DENIED_BY_MODERATOR(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new RecruitFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => false,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#6175',
                        'acp_time'      => time(),
                        'acp_reason'    =>
                            'Denied by moderator' . (isset($acp_reason) ? ' ('.$acp_reason.')' : ''),
                    ]),
                    DiscordDirectMessage::create(
                        $app->entry['d_user_id'],
                        $DM_message,
                        $acp_user,
                        true
                    ),
                ];
            },
            $extra);
    }

    public static function DENIED_ACTIVITY(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new ApplyFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => false,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#9940',
                        'acp_time'      => time(),
                        'acp_reason'    => $acp_reason ?:
                            'Failed activity requirement (25 messages in past 7d)',
                    ]),
                    DiscordDirectMessage::create(
                        $app->entry['d_user_id'],
                        $DM_message ?:
                        "Hey, your application to join the DTG Reddit Discord clan has been declined as " .
                        "you currently don't meet the activity requirements (LFG/voice channel activity " .
                        "does not count). We look into user's activity on the server when they apply to " .
                        "get a general idea of whether they're involved within the Discord community here. " .
                        "We'd advise you to build up your activity over the next few days and then re-apply " .
                        "again. If you have any questions, please send " . DISCORD_MOD_PING ." a message " .
                        "and it'll be sent to our Mod Team.",
                        empty($DM_message) ? '@Sweeper Bot#6175' : $acp_user
                    ),
                ];
            },
            $extra);
    }

    public static function DENIED_INFRACTIONS(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new ApplyFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => false,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#6175',
                        'acp_time'      => time(),
                        'acp_reason'    => $acp_reason ?:
                            'Discord user is in poor standing.',
                    ]),
                    DiscordDirectMessage::create(
                        $app->entry['d_user_id'],
                        $DM_message ?:
                        "Hey, your application to join the DTG Reddit Discord clan has been declined as " .
                        "currently you're in poor standing with our server. If you're to remain within " .
                        "good standing over the next 14 days, you can re-submit your application and we'll " .
                        "reconsider our decision. If you have any questions, please send " . DISCORD_MOD_PING .
                        " a message and it'll be sent to our Mod Team.",
                        empty($DM_message) ? '@Sweeper Bot#6175' : $acp_user
                    ),
                ];
            },
            $extra);
    }

    public static function BLACKLISTED(string $acp_reason = null, string $acp_user = null,
            string $DM_message = null, array $extra = []) {
        return new ApplyFormResult(
            function(Application $app) use ($acp_reason, $acp_user, $DM_message) {
                return [
                    new ApplicationEntryUpdate([
                        'accepted'      => false,
                        'in_acp'        => false,
                        'acp_user'      => $acp_user ?: '@Sweeper Bot#6175',
                        'acp_time'      => time(),
                        'acp_reason'    => $acp_reason ?: 'Discord user is blacklisted',
                    ]),
                    HTMLResponse::create(
                        "Sorry, but you are not allowed to apply for this. If you have questions " .
                        "please DM @Sweeper Bot#6175."
                    ),
                    DiscordDirectMessage::create(
                        $app->entry['d_user_id'],
                        $DM_message ?:
                        "Sorry, but you are not allowed to apply for this. If you have questions " .
                        "please DM " . DISCORD_MOD_PING . ".",
                        empty($DM_message) ? '@Sweeper Bot#6175' : $acp_user
                    ),
                ];
            },
            $extra);
    }

}