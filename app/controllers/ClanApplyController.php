<?php
namespace app\controllers;

use app\lib\clans\Application;
use app\lib\clans\forms\ApplyForm;

class ClanApplyController extends \Controller {

    protected function default_action(array $params) {
        $previous_apps = discord_user()->logged_in() ? Application::getApps(Application::TYPE_APPLY, discord_user()->get_id()) : [];

        return view('clans.applyform')
            ->meta('DTG Discord Clan Apply Form', 'page--discord page--clans page--clans_applyform')
            ->stylesheets('clans/forms')
            ->scripts('clans/applyform')
            ->use('previous_apps', $previous_apps)
            ->addInitOpt('ApplyForm', 'logged_in', discord_user()->logged_in())
            ->addInitOpt('ApplyForm', 'submitted', false);
    }

    protected function submit(array $params) {
        $form = new ApplyForm();

        $app = $form->generateApplication($_POST);
        $form->processApplication($app);

        $previous_apps = discord_user()->logged_in() ? Application::getApps(Application::TYPE_APPLY, discord_user()->get_id()) : [];

        return view('clans.applyform')
            ->meta('DTG Discord Clan Apply Form', 'page--discord page--clans page--clans_applyform')
            ->stylesheets('clans/forms')
            ->scripts('clans/applyform')
            ->use('app', $app)
            ->use('previous_apps', $previous_apps)
            ->addInitOpt('ApplyForm', 'logged_in', discord_user()->logged_in())
            ->addInitOpt('ApplyForm', 'submitted', $app->isValid() && $app->isStored());
    }

}
