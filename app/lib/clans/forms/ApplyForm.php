<?php
namespace app\lib\clans\forms;

use app\lib\DiscordMod;
use app\lib\clans\Application;
use app\lib\clans\Blacklist;
use app\lib\clans\forms\AbstractForm;
use app\lib\clans\forms\ApplyFormResult;

class ApplyForm extends AbstractForm {
    const FIELDS = [
        'bungie_profile',
        'platform_pc',
        'agree',
    ];

    public function generateApplication(array $data): Application {
        $data = (
            !discord_user()->logged_in()
            || empty($data)
            || empty($data = array_intersect_key($data, array_flip(self::FIELDS)))
            || count($data) !== count(self::FIELDS)
            || empty($data = AbstractForm::normalizePlatforms(['platform_pc'], $data))
            || !AbstractForm::checkBungieLink('bungie_profile', $data)
            || from($data, 'agree') !== '1'
        ) ? null : $data;

        if (empty($data))
            return Application::createInvalid(Application::TYPE_APPLY);

        return Application::fromData(Application::TYPE_APPLY, [
            'application' => [
                'platforms' => $data['platforms'],
                'submission_time' => time(),
                'last_submission_time' =>
                    db('SweeperBot')->queryFirstRow(
                    'SELECT * FROM "ClanApplyApps" WHERE d_user_id=%s AND accepted=true
                    ORDER BY submitted_utc DESC LIMIT 1',
                        discord_user()->get_id())['submitted_utc'] ?? null,
            ],
            'd_user' => discord_user()->get_info($data)
        ]);
    }

    public function processApplication(Application $app): Application {
        if (!$app->isValid()) {
            return $app;
        }

        $app->store();

        if (Application::hasPendingApp($app->type, $app->data['d_user']['id'])) {
            $app->applyResult(ApplyFormResult::ALREADY_PENDING());
            return $app;
        }

        /*if (!discord_user()->is_admin() && ($app->data['d_user']['activity_7d'] < 25)) {
            $app->applyResult(ApplyFormResult::DENIED_ACTIVITY());
            return $app;
        }*/

        if ($app->data['d_user']['is_blacklisted']) {
            $app->applyResult(ApplyFormResult::BLACKLISTED());
            return $app;
        }

        $app->applyResult(ApplyFormResult::SENT_TO_MANUAL());
        return $app;
    }

}
