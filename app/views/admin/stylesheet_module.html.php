<section id="<?php echo x_stash('pkg_name') ?>__<?php echo x_stash('mod_name') ?>"
        class="module <?php echo x_stash('mod_type') ?> collapsable"
        data-package="<?php echo x_stash('pkg_name') ?>"
        data-module="<?php echo x_stash('mod_name') ?>"
        data-enabled="<?php echo b_stash('enabled') ?>">
    <input type="hidden" name="pkg_name" value="<?php echo x_stash('pkg_name') ?>" />
    <?php if (u_stash('mod_type') == 'core_module'): ?>
        <input type="hidden" name="mod_name" value="<?php echo x_stash('mod_name') ?>" />
    <?php endif; ?>
    <div class="module_name">
        <span class="collapse-trigger"><i class="icon zmdi zmdi-chevron-down"></i></span>
        <?php if (u_stash('mod_type') == 'user_module'): ?>
            <input type="text" name="mod_name" value="<?php echo x_stash('mod_name') ?>" />
            <div class="module_state">
                <div class="ui-switch ui-switch--small">
                    <label>
                        <input type="checkbox" <?php xecho_if('checked', b_stash('enabled')) ?>
                            name="enabled" data-original-state="<?php xecho_if('1', b_stash('enabled')) ?>" />
                        <span class="knob"></span>
                    </label>
                </div>
            </div>
            <div class="module_orderTools">
                <span class="module_movePriorityDown"><i class="icon zmdi zmdi-chevron-down"></i></span>
                <span class="sep"></span>
                <span class="module_movePriorityUp"><i class="icon zmdi zmdi-chevron-up"></i></span>
            </div>
        <?php else: ?>
            <h3><?php echo x_stash('mod_name') ?></h3>
        <?php endif; ?>
        <div class="module_notice hide">
            <span class="module_notice_item module_notice--unsaved hide"
                title="Module has unsaved changes">*</span>
            <span class="module_notice_item module_notice--name_conflict hide">Module name already exists</span>
        </div>
        <div class="grow"></div>
        <div class="module_tools">
            <?php if (u_stash('mod_type') == 'user_module'): ?>
            <div class="module_tool module_tool__delete">
                <span><a class="module_tool__deleteButton">Delete module</a></span>
                <span>
                    <span style="color:red">deleting on save</span>
                    (<a class="module_tool__deleteUndo">undo</a>)
                </span>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="module_styles">
        <textarea name="styles" <?php xecho_if('readonly', x_stash('mod_type') == 'core_module') ?>><?php
            echo x_stash('styles')
        ?></textarea>
    </div>
</section>