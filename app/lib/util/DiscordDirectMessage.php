<?php
namespace app\lib\util;

use app\lib\clans\Application;
use app\lib\util\Message;

class DiscordDirectMessage extends Message {

    public function send(Application $app = null) {
        if (empty($this->to) || empty(trim($this->body))) {
            return;
        }

        discord_mod()->send_DM($this->to, $this->body);

        db('SweeperBot')->insert("ClanACPDirectMessages", [
            'app_id' => isset($app) ? $app->id : null,
            'acp_user' => $this->sent_by ?: (isset($app) ? from($app->entry, 'acp_user') : null),
            'body' => $this->body,
            'sent_to' => $this->to,
            'sent_utc' => time(),
        ]);
    }

    public static function create($user_id, $body, $sent_by = null, $addBlank = false) {
        if (empty($user_id) || empty($body))
            return null;
        if ($addBlank)
            $body .= "\n<:blank:364964889770590208>";
        return new self($user_id, $body, $sent_by);
    }

}