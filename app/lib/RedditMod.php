<?php
namespace app\lib;

use app\lib\util\SimpleOAuthClient;

class RedditMod extends SimpleOAuthClient {

    public function __construct() {
        $meta = [
            // CLIENT INFO
            'prefix'            => null,
            'client_id'         => OAUTH_MODERATOR_ID,
            'client_secret'     => OAUTH_MODERATOR_SECRET,
            'user_agent'        => OAUTH_MODERATOR_USER_AGENT,
            'grant_types'       => ['Password'], // "P" must be uppercase

            // AUTH INFO
            'auth_type'         => \OAuth2Client\Client::AUTH_TYPE_FORM,
            'access_token'      => null,
            'scopes'            => null,
            'duration'          => null,

            // AUTH REQUESTING
            'authorize_url'     => null,
            'redirect_url'      => OAUTH_MODERATOR_REDIRECT_URL,
            'access_token_url'  => null,
            'access_token_type' => 'bearer',

            'access_token_callback' => function($result) {
                if (!empty($result['access_token'])) {
                    $cached_exp = (60 * 60) - 60; // expire in 60 minutes (minus 1)

                    if (isset($result['expiry'])) {
                        $cached_exp = ($result['expiry'] - time()) - 60;
                    }

                    cached('dtg_modtoken', $result['access_token'], $cached_exp);
                }
            },
            'authorization_code_callback' => null,
        ];

        if (cached_exists('dtg_modtoken')) {
            $token = cached('dtg_modtoken');
            if (!empty($token)) {
                $meta['access_token'] = cached('dtg_modtoken');
            }
        }

        if (empty($meta['access_token'])) {
            $meta['access_token_url'] = OAUTH_ACCESS_TOKEN_URL;

            $meta['access_token'] = [
                'password',
                array(
                    'username'  => OAUTH_MODERATOR_USERNAME,
                    'password'  => OAUTH_MODERATOR_PASSWORD,
                ),
                array(
                    'Authorization' => 'Basic ' .
                        base64_encode($meta['client_id'] . ':' . $meta['client_secret'])
                ),
            ];
        }

        parent::__construct($meta);
    }

    protected function create(array $oauth) {
        return;
    }
}