<div class="banner">
    <h1>
        <img class="icon" src="<?php img_src('DTG_Discord/Avatar-TransparentEdge.png') ?>"/>
        <span>Want to Join our Discord Clan?</span>
    </h1>
</div>
<form id="ClanApplyForm" method="POST" class="form <?php if (!discord_user()->logged_in()): ?>not-logged-in<?php endif; ?>" style="margin-top:-10px;overflow:visible">
    <section class="form__main">
        <div class="field" style="
            box-shadow: 0 1px 5px rgba(0,0,0,0.2);
            padding: 20px 40px;
            font-size: 16px;">
            <p>We are no longer accepting applications to join the DTG Discord Clan at this time. If you have questions please message the Mod Team by sending a message to @Sweeper Bot#6175.</p>
        </div>
    </section>
</form>
