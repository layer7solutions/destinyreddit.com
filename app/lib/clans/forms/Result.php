<?php
namespace app\lib\clans\forms;

class Result {
    public $items;

    /**
     * Construct a new Result. Can be used for `Application->applyResult(Result $result)`.
     * You can create various Results with the factory functions in ApplyFormResult
     * or RecruitFormResult.
     *
     * @param mixed $args,... Message, ApplicationEntryUpdate, or arrays containing those
     */
    public function __construct() {
        $this->items = array_flatten(func_get_args());
    }

}