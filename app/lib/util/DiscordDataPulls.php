<?php
namespace app\lib\util;

use app\lib\clans\Blacklist;

abstract class DiscordDataPulls {

    /**
     * Get discord user info from currently logged in discord user.
     *
     * @param array $data=[] extra data
     * @return array
     */
    public static function get_user_info(array $data = []) {
        $u = discord_user();
        $uid = $u->get_id();
        return [
            'id'                        => $uid,
            'discriminator'             => $u->get_discriminator(),
            'username'                  => $u->get_username(),
            'userping'                  => $u->get_ping(),
            'bungie_profile'            => $data['bungie_profile'],
            'is_blacklisted'            => Blacklist::duser_is_blacklisted($uid, true),
            'activity_14d'               => self::get_activity_14d($uid, DISCORD_GUILD_ID),
            'actioned'                  => self::get_actioned_count($uid, DISCORD_GUILD_ID),
        ];
    }

    /**
     * Get actioned count for a certain user within a certain guild.
     *
     * @param string $uid discord user id
     * @param string $gid guild id
     * @return array an associative array of action type to action count
     */
    public static function get_actioned_count(string $uid, string $gid): array {
        $banCount = db('SweeperBot')->queryFirstField('SELECT COUNT(*) FROM ban WHERE "user_id"=(select id from "user" where discord_id=%l) AND "server_id"=(select id from "server" where discord_id=%l)', $uid, $gid) || 0;
        $kickCount = db('SweeperBot')->queryFirstField('SELECT COUNT(*) FROM kick WHERE "user_id"=(select id from "user" where discord_id=%l) AND "server_id"=(select id from "server" where discord_id=%l)', $uid, $gid) || 0;
        $warnCount = db('SweeperBot')->queryFirstField('SELECT COUNT(*) FROM warn WHERE "user_id"=(select id from "user" where discord_id=%l) AND "server_id"=(select id from "server" where discord_id=%l)', $uid, $gid) || 0;
        $noteCount = db('SweeperBot')->queryFirstField('SELECT COUNT(*) FROM note WHERE "user_id"=(select id from "user" where discord_id=%l) AND "server_id"=(select id from "server" where discord_id=%l)', $uid, $gid) || 0;
        $muteCount = db('SweeperBot')->queryFirstField('SELECT COUNT(*) FROM mute WHERE "user_id"=(select id from "user" where discord_id=%l) AND "server_id"=(select id from "server" where discord_id=%l)', $uid, $gid) || 0;

        return [
            'Ban' => $banCount,
            'Kick' => $kickCount,
            'Warn' => $warnCount,
            'Note' => $noteCount,
            'Mute' => $muteCount,
            'Infractions' => $banCount + $kickCount + $warnCount + $muteCount,
        ];
    }

    /**
     * Get amount activity in the given guild for the given user in past seven days.
     *
     * @param string $uid discord user id
     * @param string $gid guild id
     * @return int number of messages user has sent in past 7 days
     */
    public static function get_activity_7d(string $uid, string $gid): int {
        return db('SweeperBot')->queryFirstField('SELECT COUNT(*) FROM "message"
            WHERE "user_id"=(select id from "user" where discord_id=%l) AND "server_id"=(select id from "server" where discord_id=%l)
            AND "channel_id" NOT IN %ll
            AND "created" > (current_date - interval \'7 days\')',
            $uid, $gid, DISCORD_EXCLUDE_ACTIVITY_CHANNELS);
    }

    /**
     * Get amount activity in the given guild for the given user in past fourteen days.
     *
     * @param string $uid discord user id
     * @param string $gid guild id
     * @return int number of messages user has sent in past 14 days
     */
    public static function get_activity_14d(string $uid, string $gid): int {
        return db('SweeperBot')->queryFirstField('SELECT COUNT(*) FROM "message"
            WHERE "user_id"=(select id from "user" where discord_id=%l) AND "server_id"=(select id from "server" where discord_id=%l)
            AND "channel_id" NOT IN %ll
            AND "created" > (current_date - interval \'14 days\')',
            $uid, $gid, DISCORD_EXCLUDE_ACTIVITY_CHANNELS);
    }

}