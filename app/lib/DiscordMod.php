<?php
namespace app\lib;

use app\lib\util\SimpleOAuthClient;

class DiscordMod extends SimpleOAuthClient {

    public function __construct() {
        parent::__construct([
            // CLIENT INFO
            'prefix'            => null,
            'client_id'         => OAUTH_DISCORDMOD_CLIENT_ID,
            'client_secret'     => OAUTH_DISCORDMOD_CLIENT_SECRET,
            'user_agent'        => OAUTH_DISCORDMOD_USER_AGENT,
            'grant_types'       => [],

            // AUTH INFO
            'auth_type'         => \OAuth2Client\Client::AUTH_TYPE_FORM,
            'access_token'      => OAUTH_DISCORDMOD_TOKEN,
            'access_token_type' => \OAuth2Client\Client::ACCESS_TOKEN_BOT,
            'scopes'            => null,
            'duration'          => null,

            // AUTH REQUESTING
            'authorize_url'     => null,
            'redirect_url'      => null,
            'access_token_url'  => null,

            'authorization_code_callback' => null,
        ]);
    }

    public function force_add(string $serverId, string $user_id, string $access_token) {
        $url = 'https://discordapp.com/api/guilds/'.$serverId.'/members/'.$user_id;

        return $this->get_client()->fetch($url, [
            'access_token' => $access_token,
        ], \OAuth2Client\Client::HTTP_METHOD_PUT);
    }

    public function in_server(string $serverId, string $user_id): bool {
        // $state = db('SweeperBot')->queryFirstField(
        //     'SELECT "SvrJoinDate" IS NOT NULL AND ("SvrPartDate" IS NULL OR "SvrPartDate" < "SvrJoinDate") FROM "Users" WHERE "UserID"=%s_usrid AND "ServerID"=%s_srvid',
        //     [
        //         'usrid' => $user_id,
        //         'srvid' => $serverId,
        //     ]
        // );
        // return (true === $state);
        $msgCount = db('SweeperBot')->queryFirstField('SELECT COUNT(*) FROM "message" WHERE ' .
                '"user_id"=(select id from "user" where discord_id=%l) AND "server_id"=(select id from "server" where discord_id=%l)',
            $user_id, $serverId) || 0;
        return $msgCount > 0;
    }

    // public function get_user_joinpart(string $serverId, string $user_id): array {
    //     $row = db('SweeperBot')->queryFirstList(
    //         'SELECT "SvrJoinDate", "SvrPartDate" FROM "Users" WHERE "UserID"=%s_usrid AND "ServerID"=%s_srvid',
    //         [
    //             'usrid' => $user_id,
    //             'srvid' => $serverId,
    //         ]);
    //     return (!empty($row) && count($row) == 2) ? $row : [null, null];
    // }

    public function get_roles(string $serverId, string $user_id): array {
        $user = $this->get_client()->fetch(
            'https://discordapp.com/api/guilds/'.$serverId.'/members/'.$user_id)['result'];

        return $user['roles'] ?? [];
    }

    public function send_DM(string $user_id, string $content) {
        $dm_channel = $this->get_client()->fetch('https://discordapp.com/api/users/@me/channels', array(
            'recipient_id' => $user_id,
        ), \OAuth2Client\Client::HTTP_METHOD_POST, [], \OAuth2Client\Client::HTTP_FORM_CONTENT_TYPE_JSON)['result'];

        if (!empty($dm_channel)) {
            return $this->post_message($dm_channel['id'], $content);
        }

        return false;
    }

    public function post_message(string $channel_id, string $content) {
        return $this->get_client()->fetch('https://discordapp.com/api/channels/'.$channel_id.'/messages', array(
            'content' => $content,
        ), \OAuth2Client\Client::HTTP_METHOD_POST);
    }

    protected function create(array $oauth) {
        return;
    }
}