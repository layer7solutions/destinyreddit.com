<?php
namespace app\lib\clans;

use app\lib\clans\Blacklist;

class ManualQueue {
    const MAX_ITEMS_AMOUNT = 500;
    const DEFAULT_ITEMS_AMOUNT = 50;

    // recruit apps
    // ------------------------------------------------------------------------------------------

    public static function recruit_row_pass($row) {
        $row['data'] = json_decode($row['data'], true);
        if ($row['in_acp']) {
            $row['data']['clan']['is_blacklisted'] = Blacklist::clan_is_blacklisted($row['data']['clan']['id']);
            $row['data']['d_user']['is_blacklisted'] = Blacklist::duser_is_blacklisted($row['data']['d_user']['id']);
        }

        $row['DM_list'] =
            db('SweeperBot')->query(
                'SELECT * FROM "ClanACPDirectMessages"
                WHERE app_id=%i ORDER BY sent_utc DESC',
                $row['app_id']);

        return $row;
    }

    /**
     * Get recruit apps that are in the queue from newest to oldest
     *
     * @param bool $reverse if true, from oldest to newest instead
     * @return array
     */
    public static function get_recruit_apps(bool $reverse = false) {
        return array_map('self::recruit_row_pass',
                db('SweeperBot')->query(
                    'SELECT * FROM "ClanRecruitmentApps"
                    WHERE in_acp=true ORDER BY submitted_utc %l',
                    $reverse ? 'ASC' : 'DESC'));
    }

    public static function get_recruit_history($page = 1, $amount = null) {
        $page = max(intval($page), 1);
        $amount = min(intval($amount ?? self::DEFAULT_ITEMS_AMOUNT), self::MAX_ITEMS_AMOUNT);

        $page_count = intval(ceil((db('SweeperBot')->queryFirstField(
                    'SELECT count(*) FROM "ClanRecruitmentApps" WHERE in_acp=false') ?? 0) / $amount));

        $page = min($page, $page_count);

        return array(
            'listing' => array_map('self::recruit_row_pass',
                db('SweeperBot')->query(
                    'SELECT * FROM "ClanRecruitmentApps"
                    WHERE in_acp=false
                    ORDER BY submitted_utc DESC
                    LIMIT %i OFFSET %i',
                        $amount, ($page - 1) * $amount)),
            'page_number' => $page,
            'page_count'  => $page_count,
            'amount'      => $amount,
        );
    }

    // apply apps
    // ------------------------------------------------------------------------------------------

    public static function apply_row_pass($row) {
        $row['data'] = json_decode($row['data'], true);
        if ($row['in_acp']) {
            $row['data']['d_user']['is_blacklisted'] = Blacklist::duser_is_blacklisted($row['data']['d_user']['id']);
        }

        $row['DM_list'] =
            db('SweeperBot')->query(
                'SELECT * FROM "ClanACPDirectMessages"
                WHERE app_id=%i ORDER BY sent_utc DESC',
                $row['app_id']);

        return $row;
    }

    /**
     * Get apply apps that are in the queue from newest to oldest
     *
     * @param bool $reverse if true, from oldest to newest instead
     * @return array
     */
    public static function get_apply_apps(bool $reverse = false) {
        return array_map('self::apply_row_pass',
            db('SweeperBot')->query(
                    'SELECT * FROM "ClanApplyApps"
                    WHERE in_acp=true ORDER BY submitted_utc %l',
                    $reverse ? 'ASC' : 'DESC'));
    }

    public static function get_apply_history($page = 1, $amount = null) {
        $page = max(intval($page), 1);
        $amount = min(intval($amount ?? self::DEFAULT_ITEMS_AMOUNT), self::MAX_ITEMS_AMOUNT);

        $page_count = intval(ceil((db('SweeperBot')->queryFirstField(
                    'SELECT count(*) FROM "ClanApplyApps" WHERE in_acp=false') ?? 0) / $amount));

        $page = min($page, $page_count);

        return array(
            'listing' => array_map('self::apply_row_pass',
                db('SweeperBot')->query(
                    'SELECT * FROM "ClanApplyApps"
                    WHERE in_acp=false
                    ORDER BY submitted_utc DESC
                    LIMIT %i OFFSET %i',
                        $amount, ($page - 1) * $amount)),
            'page_number' => $page,
            'page_count'  => $page_count,
            'amount'      => $amount,
        );
    }

    // Direct Messages
    // ------------------------------------------------------------------------------------------

    public static function get_DM_list($page = 1, $amount = null) {
        $page = max(intval($page), 1);
        $amount = min(intval($amount ?? self::DEFAULT_ITEMS_AMOUNT), self::MAX_ITEMS_AMOUNT);

        $page_count = intval(ceil((db('SweeperBot')->queryFirstField(
                    'SELECT count(*) FROM "ClanACPDirectMessages"') ?? 0) / $amount));

        $page = min($page, $page_count);

        return array(
            'listing' =>
                db('SweeperBot')->query(
                    'SELECT * FROM "ClanACPDirectMessages"
                    ORDER BY sent_utc DESC
                    LIMIT %i OFFSET %i',
                        $amount, ($page - 1) * $amount),
            'page_number' => $page,
            'page_count'  => $page_count,
            'amount'      => $amount,
        );
    }

}
