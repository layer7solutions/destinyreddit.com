<?php
namespace app\controllers;

use app\lib\clans\forms\RecruitForm;

class ClanRecruitmentController extends \Controller {

    protected function default_action(array $params) {
        return view('clans.recruitform')
            ->meta('DTG Discord Clan Recruitment Form', 'page--discord page--clans page--clans_recruitform')
            ->stylesheets('clans/forms')
            ->scripts('clans/recruitform')
            ->addInitOpt('RecruitForm', 'logged_in', discord_user()->logged_in())
            ->addInitOpt('RecruitForm', 'submitted', false);
    }

    protected function submit(array $params) {
        $form = new RecruitForm();

        $app = $form->generateApplication($_POST);
        $form->processApplication($app);

        return view('clans.recruitform')
            ->meta('DTG Discord Clan Recruitment Form', 'page--discord page--clans page--clans_recruitform')
            ->stylesheets('clans/forms')
            ->scripts('clans/recruitform')
            ->use('app', $app)
            ->addInitOpt('RecruitForm', 'logged_in', discord_user()->logged_in())
            ->addInitOpt('RecruitForm', 'submitted', $app->isValid() && $app->isStored());
    }

}