init('Homepage', function(opts) {
    n.removeClass(document.body, 'no-js');

    n.select('.shine--on_hover')
        .removeClass('shine--on_hover')
        .on('mouseenter', function(event) {
            var element = this;
            n.addClass(element, 'shine--hovered');

            setTimeout(function() {
                if (!n.hasClass(element, 'shine--hovered')) {
                    return;
                }

                if (element.hasAttribute('data-shine-element')) {
                    element = document.querySelector(element.getAttribute('data-shine-element'));
                    n.addClass(element, 'shine');
                }

                if (n.hasClass(element, 'shine--active')) {
                    return;
                }

                n.addClass(element, 'shine--active');

                setTimeout(function() {
                    n.removeClass(element, 'shine--active');
                }, 750);
            }, 50);
        })
        .on('mouseleave', function(event) {
            var element = this;
            n.removeClass(element, 'shine--hovered');
        });

    $("#grid").vgrid({
        easing: "easeOutQuint",
        time: 400,
        delay: 20
    });
});
init('Homepage_BannerStars', function(opts) {
    var canvas = document.getElementById('banner__stars'),
        context = canvas.getContext('2d'),
        canvasW,
        canvasH,
        fps = 50,
        initTime,
        delaysComplete = 0,
        stars = [];

    function updateCanvasDimensions() {
        var banner  = document.getElementById('banner'),
            bannerW = banner.getBoundingClientRect().width,
            bannerH = banner.getBoundingClientRect().height;

        canvasW = canvas.width = bannerW;
        canvasH = canvas.height = bannerH;
    }

    function getCurrentTime() {
        return new Date() / 1000 | 0;
    }

    function Star(x, y, speed, delay) {
        this.x = parseInt(x);
        this.y = parseInt(y);
        this.size = 2;
        this.delay = delay;

        this.opacity = 0;
        this.increment = (1.0 / (speed * fps));
    }

    Star.prototype.draw = function(context) {
        context.save();

        var ease_factor = (this.opacity + 1) * (this.opacity + 1);
        this.opacity += this.increment * ease_factor;

        if (this.opacity >= 1 || this.opacity <= 0) {
            this.increment = -(this.increment);

            // make sure opacity doesn't go outside of bounds
            if (this.opacity >= 1) {
                this.opacity = 1;
            } else {
                this.opacity = 0;
            }
        }

        var scaled_size = Math.max(this.size - (1 - this.opacity), 0.5);

        context.beginPath();
        context.fillStyle = "rgba(255, 255, 255, " + this.opacity + ")";
        context.fillRect(this.x, this.y, scaled_size, scaled_size);
        context.fill();

        context.restore();
    }

    function animate() {
        if (delaysComplete !== null) {
            if (delaysComplete === opts.num_stars) {
                delaysComplete = null;
            } else {
                var now = getCurrentTime();
            }
        }

        context.clearRect(0, 0, canvasW, canvasH);
        for (var i = 0; i < stars.length; i++) {
            if (delaysComplete !== null && stars[i].delay !== null) {
                if (initTime + stars[i].delay > now) {
                    continue;
                } else {
                    stars[i].delay = null;
                    delaysComplete++;
                }
            }
            stars[i].draw(context);
        }
    }

    (function() {
        updateCanvasDimensions();
        window.addEventListener('resize', updateCanvasDimensions);

        for (var i = 0; i < opts.num_stars; i++) {
            var x = Math.random() * canvasW,
                y = Math.random() * canvasH,
                speed = ((Math.random() * 5) + 5) / 2,
                delay = Math.max(0, (Math.random()*5) - 1.5);

            stars.push(new Star(x, y, speed, delay));
        }

        initTime = getCurrentTime();
        setInterval(animate, 1000 / fps);
    })();
});
init('Homepage_BannerLinks', function(opts) {
    n.select('#banner__linksWrap, #banner__linksWrap .banner__link').addClass('js-intro');

    var promise = new Promise(function(resolve, reject) {
        setTimeout(function() {
            n.select('#banner__linksWrap').removeClass('js-intro').addClass('js-intro-complete');
            n.select('#banner__linksWrap .banner__link').removeClass('js-intro');
            resolve();
        }, 2000);
    }).then(function(result) {
        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                n.select('#banner__linksWrap .banner__link--subreddit').addClass('js-intro-complete');
                resolve();
            }, 500);
        });
    }).then(function(result) {
        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                n.select('#banner__linksWrap .banner__link--discord').addClass('js-intro-complete');
                resolve();
            }, 500);
        });
    }).then(function(result) {
        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                n.select('#banner__linksWrap .banner__link--twitter').addClass('js-intro-complete');
                resolve();
            }, 500);
        });
    });
});