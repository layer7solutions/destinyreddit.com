<?php
namespace app\lib;

use app\lib\util\SimpleOAuthClient;

class RedditClient extends SimpleOAuthClient {

    public function __construct() {
        parent::__construct([
            // CLIENT INFO
            'prefix'            => 'reddit_',
            'client_id'         => OAUTH_CLIENT_ID,
            'client_secret'     => OAUTH_CLIENT_SECRET,
            'user_agent'        => OAUTH_USER_AGENT,
            'grant_types'       => null,

            // AUTH INFO
            'auth_type'         => \OAuth2Client\Client::AUTH_TYPE_AUTHORIZATION_BASIC,
            'access_token'      => null,
            'scopes'            => 'identity',

            // AUTH REQUESTING
            'authorize_url'     => OAUTH_AUTHORIZE_URL,
            'redirect_url'      => OAUTH_REDIRECT_URL,
            'access_token_url'  => OAUTH_ACCESS_TOKEN_URL,

            'authorization_code_callback' => function($code) {
                return [
                    "authorization_code",
                    [
                        "code"          => $code,
                        "redirect_uri"  => OAUTH_REDIRECT_URL
                    ],
                ];
            },
        ]);
    }

    public function create(array $oauth) {

        // Retrieve Reddit username
        // ~~~~~~~~~~~~~~~~~~~~~~~~

        $userdata = $oauth['client']->fetch("https://oauth.reddit.com/api/v1/me.json");

        if (!isset($userdata['result']['name'])) {
            $this->dispatch_auth_fail_page(
                "we were unable to retrieve your username from the Reddit API");
        }

        // Initialize the session
        // ~~~~~~~~~~~~~~~~~~~~~~

        session_regenerate_id();

        $this->property('username', $userdata['result']['name']);
        $this->property('logged_in', true);
    }

    public function is_admin() {
        if (!$this->logged_in() || empty($this->get_username())) {
            return false;
        }
        return in_array($this->get_username(), SITE_ADMINS);
    }

}