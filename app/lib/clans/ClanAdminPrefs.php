<?php
namespace app\lib\clans;

class ClanAdminPrefs {
    const HISTORY_AMOUNT = "history_amount_";
    const QUEUE_SORT = "queue_sort_";

    private static $cache;

    private static function updateCache(string $user_id) {
        $row = db('SweeperBot')->queryFirstField(
            'SELECT prefs FROM "ClanAdminPrefs" WHERE d_user_id=%s', $user_id);
        self::$cache[$user_id] = empty($row) ? [] : json_decode($row, true);
    }

    public static function get(string $user_id, string $prop) {
        if (!isset(self::$cache[$user_id])) {
            self::updateCache($user_id);
        }
        return from(self::$cache[$user_id], $prop);
    }


    public static function set(string $user_id, string $prop, $value) {
        if (!isset(self::$cache[$user_id])) {
            self::updateCache($user_id);
        }

        self::$cache[$user_id][$prop] = $value;

        db('SweeperBot')->insertUpdate('ClanAdminPrefs', [
            'd_user_id' => $user_id,
            'prefs' => json_encode(self::$cache[$user_id]),
        ]);
    }

    // --------------------------------------------------------------------------------
    // Timezone

    public static function get_composite_timezone(string $user_id) {
        return tz_split_composite(self::get($user_id, 'timezone'));
    }

    /**
     * Get the user's timezone as a PHP DateTimeZone object.
     *
     * @return \DateTimeZone
     */
    public static function get_php_timezone(string $user_id) {
        return new \DateTimeZone(self::get_php_name_timezone($user_id) ?: date_default_timezone_get());
    }

    /**
     * Get the GMT offset of the user's timezone.
     *
     * @return int|float the offset in hours, may be a float (e.g. `9.5`) for half-hour offsets
     */
    public static function get_gmt_timezone_offset(string $user_id) {
        $tz = self::get_php_timezone($user_id);
        $offset = $tz->getOffset(new \DateTime()); // gets offset in seconds
        $offset /= 3600; // 3600 seconds in an hour
        return $offset; // return offset in hours
    }

    /**
     * Get the abbreviation of the user's timezone, typically three letters,
     * e.g. "PST" for Pacific Standard Time.
     *
     * @return string
     */
    public static function get_abrv_timezone(string $user_id) {
        $dt = new \DateTime();
        $dt->setTimezone(self::get_php_timezone($user_id));
        return xssafe($dt->format('T'));
    }

    /**
     * Get the PHP name of the user's timezone.
     * See http://php.net/manual/en/timezones.php for a list of PHP timezone names.
     *
     * @return string
     */
    public static function get_php_name_timezone(string $user_id) {
        return self::get_composite_timezone($user_id)['php_name'];
    }

    /**
     * Get a display name for the timezone in the format of "(GMT[<+|-><offset>]) <DisplayName>".
     * e.g.
     *   - "(GMT-10:00) Hawaii"
     *   - "(GMT-06:00) Central Time (US & Canada)"
     *   - "(GMT-08:00) Vancouver, BC"
     *   - "(GMT) London"
     *   - "(GMT+09:00) Tokyo"
     *   - "(GMT+09:30) Darwin"
     * You can get just the display name by splitting on the first instance of ") ".
     *
     * @return string
     */
    public static function get_display_timezone(string $user_id) {
        return self::get_composite_timezone($user_id)['display_name'];
    }

}