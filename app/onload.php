<?php
// This file is included from within App::load
// Note that functions declared within functions are in the global namespace

date_default_timezone_set('Europe/London');

// CACHE BUSTER
// --------------------------------------------------------------------------------
// increment these version numbers when you need to bust the cache
define('CSS_VERSION', '1.1.16');
define('JS_VERSION', '1.2.96');

// APPLICATION INCLUDES
// --------------------------------------------------------------------------------
require APP_ROOT.'lib/session.php';
require_once BOOTSTRAP_ROOT."oauth/oauth2-client-php/Client.php";

// ADMINS
// --------------------------------------------------------------------------------

define('SITE_ADMINS', [
    'kwwxis',
    'Clarkey7163',
    'D0cR3d',
    'DTG_Bot',
    'Fuzzle_hc',
    'Squeagley',
    'MikeyJayRaymond',
    'MisterWoodhouse',
    'spaghetticatt',
    'notliam',
    'NorseFenrir',
    'aaron-il-mentor',
    'MetalGilSolid',
    'redka243',
    'The--Marf',
    'thirdegree',
    'MattyMcD',
    'Hawkmoona_Matata',
    'OxboxturnoffO',
    'GenericDreadHead',
    'RiseOfBacon',
    'Ruley9',
    'Tahryl',
    'ZarathustraEck',
    'aslak1899',
    'LucentBeam8MP',
    'irJustineee',
    'CaptainCosmodrome',
    'CompuZockt',
    'THConer',
    'Glamdring804',
    'TheLittleMoa',
    'ChrisDAnimation',
    'rocksandfuns',
    'gsmebbs',
    'Skellums',
    'Techman-',
    'HeyItzSteve',
    'Cardzfan5',
    'Mastershroom',
    'Shabolt_',
    'IAmNot_ARussianBot',
    'The_Owl_Bard',
    'MrCranberryTea',
    'Kingofthered',
    'Mrfoxuk',
    'Daerog',
    'FallingEli',
    'Willyt2194',
    'NasusIsMyLover',
    'TH3_LUMENUX',
    'twofivethreetwo',
]);

// DECLARE ALIASES
// --------------------------------------------------------------------------------

/**
 * Global alias for `App::db($dbname)`.
 *
 * @return DB
 */
function db($dbname): DB {
    return App::db($dbname);
}

/**
 * Get the Reddit end-user.
 *
 * @return app\lib\RedditClient
 */
function reddit_user(): \app\lib\RedditClient {
    static $_user = null;
    return $_user ?? ($_user = new \app\lib\RedditClient);
}

/**
 * Get the Reddit moderator.
 *
 * @return app\lib\RedditMod
 */
function reddit_mod(): \app\lib\RedditMod {
    static $_user = null;
    return $_user ?? ($_user = new \app\lib\RedditMod);
}

/**
 * Get the Discord end-user.
 *
 * @return app\lib\DiscordClient
 */
function discord_user(): \app\lib\DiscordClient {
    static $_user = null;
    return $_user ?? ($_user = new \app\lib\DiscordClient);
}

/**
 * Get the Discord moderator.
 *
 * @return app\lib\DiscordMod
 */
function discord_mod(): \app\lib\DiscordMod {
    static $_user = null;
    return $_user ?? ($_user = new \app\lib\DiscordMod);
}

/**
 * Get the Bungie end-user.
 *
 * @return app\lib\BungieClient
 */
function bungie_user(): \app\lib\BungieClient {
    static $_user = null;
    return $_user ?? ($_user = new \app\lib\BungieClient);
}

// INIT WEB REQUEST
// --------------------------------------------------------------------------------

session_init();

\View::addProfile('net.layout', function($view) {
    $view->scripts('ProductionUtil');
    $view->usingLib('roboto_font');
    $view->stylesheets('main');
    $view->favicontype('png');
});

return 'routes/routes.php';
