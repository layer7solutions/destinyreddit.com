init('ApplyForm', function(opts) {
    if (opts.submitted || !opts.logged_in) {
        return;
    }

    (function() {
        var errors_enabled = 0;
        var is_submitting = false;

        function clear_errors() {
            var a = document.querySelectorAll('.field__errors');
            var b = document.querySelectorAll('.field_error');

            for (var i = 0; i < a.length; i++) {
                n.removeClass(a[i], 'enabled');
            }
            for (var i = 0; i < b.length; i++) {
                n.removeClass(b[i], 'enabled');
            }

            errors_enabled = 0;
        }

        function set_error_enabled(fieldname, errname, errstate) {
            errstate = is_undef(errstate) ? true : errstate; // default value -> true

            var errors = document.querySelector('.field[data-for="'+fieldname+'"] .field__errors');
            var error = errors.querySelector('[data-errname="'+errname+'"]');

            if (errstate) {
                if (n.hasClass(error, 'enabled')) {
                    return; // already enabled
                }
                n.addClass(error, 'enabled');
                n.addClass(errors, 'enabled');
                errors_enabled++;
            } else {
                if (!n.hasClass(error, 'enabled')) {
                    return; // already disabled
                }
                n.removeClass(error, 'enabled');
                if (!errors.querySelector('.enabled').length) {
                    n.removeClass(errors, 'enabled');
                }
                errors_enabled--;
            }
        }

        function is_bungie_url(url) {
            var domain = extract_domain(url).toLowerCase();
            if (domain == 'www.bungie.net' || domain == 'bungie.net') {
                return true;
            }
        }

        function form_validate(event) {
            if (is_submitting) {
                event.preventDefault();
                return;
            } else {
                is_submitting = true;
            }

            clear_errors();

            var field;

            // ---- BEGIN HELPER FUNCTIONS
            function field_input() {
                return document.querySelector('.field [name="'+field+'"]');
            }
            function field_required() {
                var has_value = true;
                if (field == 'platform_option') {
                    if (!document.querySelectorAll('.platform_option:checked').length) {
                        set_error_enabled(field, 'Required');
                        has_value = false;
                    }
                } else if (field_input().getAttribute('type') === 'checkbox') {
                    if (!field_input().checked) {
                        set_error_enabled(field, 'Required');
                        has_value = false;
                    }
                } else if (!field_input().value.length) {
                    set_error_enabled(field, 'Required');
                    has_value = false;
                }
                return has_value;
            }
            // ----- END HELPER FUNCTIONS

            // ----- BEGIN VALIDATION
            {
                field ='bungie_profile'
                if (field_required()) {
                    if (!is_bungie_url(field_input().value)) {
                        set_error_enabled(field, 'BungieURL');
                    }
                }
            }
            {
                field = 'platform_option';
                field_required();
            }
            {
                field = 'agree';
                field_required();
            }
            // ----- END VALIDATION

            if (errors_enabled && event)
                event.preventDefault();
            if (errors_enabled)
                is_submitting = false;
            return !errors_enabled;
        }

        n.on('#ClanApplyForm', 'submit', function(event) {
            return form_validate(event);
        });
    })();
});