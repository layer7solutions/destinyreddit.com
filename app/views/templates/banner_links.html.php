<div class="banner__links">
    <a class="banner__link banner__link--subreddit" href="https://www.reddit.com/r/DestinyTheGame" rel="noopener noreferrer" target="_blank">
        <span class="label">Subreddit</span>
        <svg class="hexagon" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 40 46">
            <g fill="<?php echo x_stash('link_fill'); ?>" fill-rule="nonzero">
                <path d="M.474 34.873l19.058 11.003a.93.93 0 0 0 .936 0l19.058-11.003a.936.936 0 0 0 .467-.81V12.057a.936.936 0 0 0-.467-.81L20.468.244a.935.935 0 0 0-.936 0L.474 11.247a.935.935 0 0 0-.467.81v22.006c0 .334.178.643.467.81zm1.404-22.276L20 2.134l18.122 10.463v20.926L20 43.986 1.878 33.523V12.597z"/>
            </g>
        </svg>
        <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 24 24">
            <path style="fill:<?php echo x_stash('link_fill'); ?>;" d="M24 11.779c0-1.459-1.192-2.645-2.657-2.645-.715 0-1.363.286-1.84.746-1.81-1.191-4.259-1.949-6.971-2.046l1.483-4.669 4.016.941-.006.058c0 1.193.975 2.163 2.174 2.163 1.198 0 2.172-.97 2.172-2.163s-.975-2.164-2.172-2.164c-.92 0-1.704.574-2.021 1.379l-4.329-1.015c-.189-.046-.381.063-.44.249l-1.654 5.207c-2.838.034-5.409.798-7.3 2.025-.474-.438-1.103-.712-1.799-.712-1.465 0-2.656 1.187-2.656 2.646 0 .97.533 1.811 1.317 2.271-.052.282-.086.567-.086.857 0 3.911 4.808 7.093 10.719 7.093s10.72-3.182 10.72-7.093c0-.274-.029-.544-.075-.81.832-.447 1.405-1.312 1.405-2.318zm-17.224 1.816c0-.868.71-1.575 1.582-1.575.872 0 1.581.707 1.581 1.575s-.709 1.574-1.581 1.574-1.582-.706-1.582-1.574zm9.061 4.669c-.797.793-2.048 1.179-3.824 1.179l-.013-.003-.013.003c-1.777 0-3.028-.386-3.824-1.179-.145-.144-.145-.379 0-.523.145-.145.381-.145.526 0 .65.647 1.729.961 3.298.961l.013.003.013-.003c1.569 0 2.648-.315 3.298-.962.145-.145.381-.144.526 0 .145.145.145.379 0 .524zm-.189-3.095c-.872 0-1.581-.706-1.581-1.574 0-.868.709-1.575 1.581-1.575s1.581.707 1.581 1.575-.709 1.574-1.581 1.574z"/>
        </svg>
    </a>
    <a class="banner__link banner__link--discord" href="https://destinyreddit.com/discord" rel="noopener noreferrer" target="_blank">
        <span class="label">Discord</span>
        <svg class="hexagon" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 40 46">
            <g fill="<?php echo x_stash('link_fill'); ?>" fill-rule="nonzero">
                <path d="M.474 34.873l19.058 11.003a.93.93 0 0 0 .936 0l19.058-11.003a.936.936 0 0 0 .467-.81V12.057a.936.936 0 0 0-.467-.81L20.468.244a.935.935 0 0 0-.936 0L.474 11.247a.935.935 0 0 0-.467.81v22.006c0 .334.178.643.467.81zm1.404-22.276L20 2.134l18.122 10.463v20.926L20 43.986 1.878 33.523V12.597z"/>
            </g>
        </svg>
        <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 48 48">
            <path style="fill:<?php echo x_stash('link_fill'); ?>;" d="M 40 12 C 40 12 35.414063 8.410156 30 8 L 29.511719 8.976563 C 34.40625 10.175781 36.652344 11.890625 39 14 C 34.953125 11.933594 30.960938 10 24 10 C 17.039063 10 13.046875 11.933594 9 14 C 11.347656 11.890625 14.019531 9.984375 18.488281 8.976563 L 18 8 C 12.320313 8.535156 8 12 8 12 C 8 12 2.878906 19.425781 2 34 C 7.160156 39.953125 15 40 15 40 L 16.640625 37.816406 C 13.855469 36.847656 10.714844 35.121094 8 32 C 11.238281 34.449219 16.125 37 24 37 C 31.875 37 36.761719 34.449219 40 32 C 37.285156 35.121094 34.144531 36.847656 31.359375 37.816406 L 33 40 C 33 40 40.839844 39.953125 46 34 C 45.121094 19.425781 40 12 40 12 Z M 17.5 30 C 15.566406 30 14 28.210938 14 26 C 14 23.789063 15.566406 22 17.5 22 C 19.433594 22 21 23.789063 21 26 C 21 28.210938 19.433594 30 17.5 30 Z M 30.5 30 C 28.566406 30 27 28.210938 27 26 C 27 23.789063 28.566406 22 30.5 22 C 32.433594 22 34 23.789063 34 26 C 34 28.210938 32.433594 30 30.5 30 Z "/>
        </svg>
    </a>
    <a class="banner__link banner__link--twitter" href="https://twitter.com/DestinyReddit" rel="noopener noreferrer" target="_blank">
        <span class="label">Twitter</span>
        <svg class="hexagon" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 40 46">
            <g fill="<?php echo x_stash('link_fill'); ?>" fill-rule="nonzero">
                <path d="M.474 34.873l19.058 11.003a.93.93 0 0 0 .936 0l19.058-11.003a.936.936 0 0 0 .467-.81V12.057a.936.936 0 0 0-.467-.81L20.468.244a.935.935 0 0 0-.936 0L.474 11.247a.935.935 0 0 0-.467.81v22.006c0 .334.178.643.467.81zm1.404-22.276L20 2.134l18.122 10.463v20.926L20 43.986 1.878 33.523V12.597z"/>
            </g>
        </svg>
        <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 612 612">
            <g>
                <path fill="<?php echo x_stash('link_fill'); ?>" d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411
                    c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513
                    c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101
                    c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104
                    c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194
                    c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485
                    c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z"/>
            </g>
        </svg>
    </a>
</div>