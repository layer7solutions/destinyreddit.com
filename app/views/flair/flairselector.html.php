<style type="text/css"><?php echo u_stash('flair_css') ?></style>
<form id="FlairSelectorForm" class="flair-form" name="FlairSelect"
        method="POST" action="<?php echo SITE_URL ?>flair">
    <input type="hidden" name="flair_class" required />
    <div class="flair-display halign">
        <?php if (reddit_user()->logged_in()): ?>
        <div class="flair-display-inner">
            <div class="flair-display-cell">
                <div class="flair-option flair-option-disabled">
                    <div class="flair flair-none"></div>
                    <div>None chosen</div>
                </div>
            </div>
            <div class="flair flair-submit-area halign">
                <?php if (reddit_user()->is_admin()): ?>
                <div class="flair-submit-field">
                    <input type="text" name="flair_user" maxlength="20" placeholder="User (optional)"
                        value="<?php echo reddit_user()->get_username() ?>" required />
                </div>
                <?php endif; ?>
                <div class="flair-submit-field">
                    <input type="text" name="flair_text" maxlength="64" placeholder="Flair hover text (required)" />
                </div>
                <input class="flair-submit-button" type="submit" value="Submit" disabled />
            </div>
        </div>
        <?php else: ?>
        <a href="<?php echo SITE_URL ?>reddit-login?cont=<?php echo safe_current_url() ?>" class="reddit-login-button">Please log in to choose a flair</a>
        <?php endif; ?>
    </div>
    <div class="flair-searchbox">
        <select id="flair-category-filter">
        <option value="all" selected style="font-weight:bold">All Flairs</option>
            <?php foreach (xa_stash('flair_categs') as $category):
                $disp_category = $category;
                if ($category === 'none') {
                    $disp_category = 'Non-categorized';
                }
                ?>
                <option value="<?php echo $category ?>"><?php echo $disp_category . ' flairs' ?></option>
            <?php endforeach; ?>
        </select>
        <input id="flair-search-text" type="text" placeholder="Search for a flair..." />
        <div class="flair-searchbox__NoResults" style="display:none">
            <p>No results</p>
        </div>
    </div>
    <div class="flair-table">
        <?php foreach (xa_stash('flair_data') as $name => $data): ?>
            <?php if (!$data['available'] && !reddit_user()->is_admin()) continue; ?>
            <div class="flair-option" id="FlairOption--<?php echo $name; ?>"
                    data-name="<?php echo $name; ?>"
                    data-displayname="<?php echo $data['display_name'] ?>"
                    data-flairclass="<?php echo $data['flair_class'] ?>"
                    data-category="<?php echo $data['category'] ?>">
                <div class="flair <?php echo $data['flair_class2'] ?>"></div>
                <div><?php echo $data['display_name'] ?></div>
            </div>
        <?php endforeach; ?>
    </div>
</form>
<a href="#top" class="back-to-top">Back to top</a>