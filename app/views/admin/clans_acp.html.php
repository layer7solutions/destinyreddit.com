<aside>
    <?php if (u_stash('page') == '403'): ?>
    <h1>&nbsp;</h1>
    <?php else: ?>
    <h1>Clans ACP</h1>
    <h3>Navigation</h3>
    <ul>
        <li><a href="<?php echo SITE_URL ?>admin/clans"
            class="<?php if (u_stash('page') == 'home') echo 'selected' ?>">ACP Home</a></li>
    </ul>
    <?php if (discord_user()->is_general_admin()): ?>
    <h4>Recruitment Posting Requests</h4>
    <ul class="spacer-left">
        <li><a href="<?php echo SITE_URL ?>admin/clans/recruit-queue"
            class="<?php if (u_stash('page') == 'recruit-queue') echo 'selected' ?>">Queue</a></li>
        <li><a href="<?php echo SITE_URL ?>admin/clans/recruit-history"
            class="<?php if (u_stash('page') == 'recruit-history') echo 'selected' ?>">History</a></li>
    </ul>
    <?php endif; ?>
    <?php if (discord_user()->is_clan_admin()): ?>
    <h4>DTG Discord Clan Applications</h4>
    <ul class="spacer-left">
        <li><a href="<?php echo SITE_URL ?>admin/clans/apply-queue"
            class="<?php if (u_stash('page') == 'apply-queue') echo 'selected' ?>">Queue</a></li>
        <li><a href="<?php echo SITE_URL ?>admin/clans/apply-history"
            class="<?php if (u_stash('page') == 'apply-history') echo 'selected' ?>">History</a></li>
    </ul>
    <?php endif; ?>
    <ul>
        <li><a href="<?php echo SITE_URL ?>admin/clans/message"
            class="<?php if (u_stash('page') == 'message') echo 'selected' ?>">Message</a></li>
        <li><a href="<?php echo SITE_URL ?>admin/clans/blacklist"
            class="<?php if (u_stash('page') == 'blacklist') echo 'selected' ?>">Blacklist</a></li>
        <li><a href="<?php echo SITE_URL ?>discord-logout?cont=/admin/clans">Logout</a></li>
    </ul>
    <?php endif; ?>
</aside>
<article>
    <h2><?php echo x_stash('acp_title') ?></h2>
<?php if (u_stash('page') == 'home'): ?>
    <p>Welcome, <?php xecho(discord_user()->get_ping()); ?>.</p>

    <p style="margin-top:10px">Select an option from the navigation to the left.</p>

    <h3>Your ACP credentials:</h3>
    <table>
        <thead>
            <tr>
                <th>Credential</th>
                <th>Status</th>
                <td>Server Id</td>
                <td>Method of verification</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Admin on main server</th>
                <td><?php echo discord_user()->is_general_admin() ? 'yes' : 'no' ?></td>
                <td><?php echo DISCORD_GUILD_ID ?></td>
                <td>checks your status in the SweeperBot.servermodrels or SweeperBot.serveradminrels table</td>
            </tr>
            <tr>
                <th>Admin on clan server</th>
                <td><?php echo discord_user()->is_clan_admin() ? 'yes' : 'no' ?></td>
                <td><?php echo DISCORD_CLAN_GUILD_ID ?></td>
                <td>checks if you have the role: <?php echo DISCORD_CLAN_MOD_ID ?></td>
            </tr>
        </tbody>
    </table>

    <h3>Preferences</h3>
    <form class="prefs_update" method="POST" action="<?php echo SITE_URL ?>admin/clans/home">
        <input type="hidden" name="cont" value="/admin/clans" />
        <fieldset>
            <legend>Timezone</legend>
            <div>
                <input type="hidden" name="prop" value="timezone"/>
                <select name="value" required>
                    <option value="" disabled selected hidden>Please Choose...</option>
                    <?php foreach (get_timezones_by_region() as $region => $timezones): ?>
                    <optgroup label="<?php xecho($region) ?>">
                        <?php foreach ($timezones as $display_name => $php_name):
                            $composite_name = tz_create_composite($php_name, $display_name);
                            ?>
                            <option value="<?php xecho($composite_name) ?>"
                                    <?php echo_if('selected', u_stash('current_tz') === $composite_name) ?>>
                                <?php xecho($display_name) ?>
                            </option>
                        <?php endforeach; ?>
                    </optgroup>
                    <?php endforeach; ?>
                </select>
                <button type="submit" name="action" value="prefs">Save</button>
            </div>
        </fieldset>
    </form>
<?php elseif (u_stash('page') == 'blacklist'): ?>
    <form class="blacklist_add" style="margin-bottom:20px;"
            method="POST" action="<?php echo SITE_URL ?>admin/clans/blacklist/add">
        <input type="hidden" name="cont" value="/admin/clans/blacklist" />
        <fieldset>
            <legend>Add To Blacklist</legend>
            <select name="fieldtype" required>
                <option value="clan_id">Clan ID</option>
                <option value="d_user_id">Discord User ID</option>
            </select>
            <input type="text" name="fieldval" value="" placeholder="Value" required />
            <input type="submit" value="Add" />
        </fieldset>
    </form>
    <h3>Blacklist Table</h3>
    <table class="blacklist_table">
        <thead>
            <tr>
                <th class="fieldval">Blacklisted Entity</th>
                <th class="fieldtype">Entity Type</th>
                <td class="blacklisted_utc">Date added (UTC)</td>
                <td class="blacklisted_by">Added by</td>
                <td class="blacklisted_by">Attempts</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach (xa_stash('blacklist') as $item): ?>
            <tr>
                <?php if ($item['fieldtype'] == 'clan_id'): ?>
                    <th class="fieldval"><?php xecho($item['fieldval']) ?></th>
                    <td class="fieldtype">Clan ID</td>
                <?php elseif ($item['fieldtype'] == 'd_user_id'): ?>
                    <th class="fieldval"><?php xecho($item['fieldval']) ?></th>
                    <td class="fieldtype">Discord User ID</td>
                <?php endif; ?>
                <td class="blacklisted_utc"><?php echo timeConvert($item['blacklisted_utc']) ?></td>
                <td class="blacklisted_by"><?php xecho($item['blacklisted_by']) ?></td>
                <td class="attempts"><?php xecho($item['attempts']) ?></td>
                <td>
                    <form method="POST" action="<?php echo SITE_URL ?>admin/clans/blacklist/remove">
                        <input type="hidden" name="cont" value="/admin/clans/blacklist" />
                        <input type="hidden" name="id" value="<?php xecho($item['id']) ?>" />
                        <input type="hidden" name="fieldtype" value="<?php xecho($item['fieldtype']) ?>" />
                        <input type="hidden" name="fieldval" value="<?php xecho($item['fieldval']) ?>" />
                        <input type="submit" value="Remove from blacklist" />
                    </form>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php elseif (u_stash('page') == 'recruit-queue' || u_stash('page') == 'apply-queue'): ?>
    <?php if (empty(u_stash('apps'))): ?>
    <div style="display:flex;justify-content:center;">
        <img src="<?php img_src('queue_kitteh.png'); ?>" />
    </div>
    <?php else: ?>
    <div class="App__manualQueue">
        <?php foreach (u_stash('apps') as $app): ?>
        <form method="POST" action="<?php echo SITE_URL ?>admin/clans/<?php echo stash('app_type') ?>-queue">
            <input type="hidden" name="cont" value="/admin/clans/<?php echo stash('app_type') ?>-queue" />
            <input type="hidden" name="app_id" value="<?php xecho($app['app_id']) ?>" />
            <?php render_template('admin.clans_acp-appview', [
                'app'       => $app,
                'app_type'  => stash('app_type'),
                'in_queue'  => true,
            ]); ?>
        </form>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
<?php elseif (u_stash('page') == 'recruit-history' || u_stash('page') == 'apply-history'): ?>
    <div class="App__history">
        <p><span style="color:red">*</span> - property is dynamic, calculated on every page load</p>
        <p class="spacer-bottom">All other properties not marked as dynamic are static, i.e. they
        are calculated only at the time the application was submitted and never automatically updated
        afterwards.</p>
        <?php render_template('admin.clans_acp-historynav', [
            'cont' => '/admin/clans/' . stash('app_type') . '-history',
        ]); ?>

        <?php foreach (u_stash('listing') as $app): ?>
        <form method="POST" action="<?php echo SITE_URL ?>admin/clans/<?php echo stash('app_type') ?>-queue">
            <input type="hidden" name="cont" value="<?php xecho(current_url()) ?>" />
            <input type="hidden" name="app_id" value="<?php xecho($app['app_id']) ?>" />
            <?php render_template('admin.clans_acp-appview', [
                'app'       => $app,
                'app_type'  => stash('app_type'),
                'in_queue'  => false,
            ]); ?>
        </form>
        <?php endforeach; ?>
    </div>
<?php elseif (u_stash('page') == 'message'): ?>
    <p>Send a direct message to a specific user by their User ID through Sweeper Bot.</p>
    <form class="DirectMessageForm" method="POST">
        <input type="hidden" name="cont" value="/admin/clans/message" />
        <input name="to" type="text" placeholder="Discord user ID" required /><br/>
        <textarea name="body" placeholder="Message body" required></textarea><br/>
        <button type="submit">Send</button>
    </form>
    <div class="DL_area spacer-top">
        <?php render_template('admin.clans_acp-historynav', [
            'cont' => '/admin/clans/message',
        ]); ?>
        <table class="DirectMessageList" style="margin:0">
            <thead>
                <tr>
                    <th style="width:100px;">Date Sent</th>
                    <th style="width:130px;">Recipient</th>
                    <th style="width:150px;">Sender</th>
                    <th>Message</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (u_stash('listing') as $msg): ?>
                <tr>
                    <td style="vertical-align:top;line-height:18px;"><?php xecho(timeConvert($msg['sent_utc'], false, true)) ?></td>
                    <td style="vertical-align:top;"><code><?php xecho($msg['sent_to']) ?></code></td>
                    <td style="vertical-align:top;"><?php xecho($msg['acp_user']) ?></td>
                    <td style="vertical-align:top;"><?php echo html_entity_decode(xssafe($msg['body'])) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php elseif (u_stash('page') == '403'): ?>
    <?php if (discord_user()->logged_in()): ?>
    <p>You do not have permission to access this page.</p>
    <?php else: ?>
    <p>You must be logged in to access this page.</p>
    <a href="<?php echo SITE_URL ?>discord-login?cont=<?php echo safe_current_url() ?>">Login with Discord</a>
    <?php endif; ?>
<?php elseif (u_stash('page') == '404'): ?>
    <p>404 not found.</p>
<?php endif; ?>
</article>