<?php
namespace app\lib\clans\forms;

use app\lib\clans\Application;

abstract class AbstractForm {

    public abstract function generateApplication(array $data): Application;

    public abstract function processApplication(Application $app): Application;

    public static function normalizePlatforms(array $validPlatforms, array $data): ?array {
        if (empty($data))
            return null;

        $platforms = [];

        foreach ($validPlatforms as $platform) {
            $platforms[] = from($data, $platform);
        }

        $platforms = array_filter($platforms);

        if (empty($platforms)) {
            return null;
        }

        $data['platforms'] = trim(implode(', ', $platforms));

        foreach ($validPlatforms as $platform) {
            unset($data[$platform]);
        }

        return $data;
    }

    public static function normalizeRegionOption(string $field, array $data): ?array {
        if (empty($data) || empty($data[$field]))
            return null;

        $data[$field] = array_filter($data[$field]);

        if (empty($data[$field])) {
            return null;
        }

        if (count($data[$field]) == 7) { // 7 total regions on earth
            $data[$field] = 'Global (all regions)';
        } else {
            $data[$field] = ucwords(trim(implode(', ', $data[$field])));
        }

        return $data;
    }

    public static function checkBungieLink(string $link_type, array $data): bool {
        if (empty($data) || empty($data[$link_type]))
            return false;

        if ($link_type === 'bungie_profile' && str_contains($data[$link_type], "5267024"))
            return false;

        return in_array(full_domain(strtolower($data[$link_type])), ['bungie.net', 'www.bungie.net']);
    }

    /**
     * Get the clan id from a bungie.net clan link.
     *
     * @return string|false the clan id or `false` if not found
     */
    public static function getClanIdFromLink(string $clan_link) {
        $clan_link_test = explode('bungie.net', strtolower($clan_link), 2);

        if (count($clan_link_test) < 2) {
            return false;
        } else {
            $clan_link_test = $clan_link_test[1];
        }

        if (!startsWith($clan_link_test, '/en/clanv2?groupid=') &&
            !startsWith($clan_link_test, '/en/clanv2/chat?groupid=')) {
            return false;
        } else {
            $clan_id = explode('groupid=', $clan_link_test, 2);
            if (count($clan_id) < 2) {
                return false;
            }
            $clan_id = preg_split('/[^0-9]/', $clan_id[1])[0];
            if (empty($clan_id)) {
                return false;
            }

            return $clan_id;
        }
    }

}