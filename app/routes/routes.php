<?php
/*
routes.php
~~~~~~~~~~
# -> runs controller
% -> runs string as PHP code
*/

App::router()

    // Home
    // --------------------------------------------------------------------------------
    ->GET('/',                                  '#Homepage')
    ->GET('/discord',                           '%redirect_ext("https://discord.gg/DestinyReddit")')

    // Login / Logout
    // --------------------------------------------------------------------------------
    ->GET('/reddit-login',                      '#Login:reddit_login')
    ->GET('/reddit-logout',                     '#Login:reddit_logout')

    ->GET('/discord-login',                     '#Login:discord_login')
    ->GET('/discord-logout',                    '#Login:discord_logout')

    ->GET('/bungie-login',                      '#Login:bungie_login')
    ->GET('/bungie-logout',                     '#Login:bungie_logout')

    ->GET('/logout',                            '#Login:real_logout')

    // DTG Flair Selector
    // --------------------------------------------------------------------------------
    ->GET('/flair',                             '#FlairSelector')
    ->POST('/flair',                            '#FlairSelector:submit')
    ->GET('/flair.json',                        '#FlairSelector:json')

    // Admin
    // --------------------------------------------------------------------------------
    ->addGroup('/admin')
        ->addgroup('/stylesheet')
            ->GET('/',                          '#admin/StylesheetACP:page')
            ->ANY('/{action}',                  '#admin/StylesheetACP:action')
        ->endGroup()
        ->addgroup('/clans')
            ->GET('[/{page}]',                  '#admin/ClansACP')
            ->POST('/home',                     '#admin/ClansACP:home_action')
            ->POST('/recruit-queue',            '#admin/ClansACP:modqueue_recruit')
            ->POST('/apply-queue',              '#admin/ClansACP:modqueue_apply')
            ->POST('/{app_type}-history',       '#admin/ClansACP:history_update')
            ->POST('/history_update',           '#admin/ClansACP:history_update')
            ->POST('/blacklist/add',            '#admin/ClansACP:blacklist_add')
            ->POST('/blacklist/remove',         '#admin/ClansACP:blacklist_remove')
            ->POST('/message',                  '#admin/ClansACP:send_message')
            ->POST('/recruit_app_clan_prop',    '#admin/ClansACP:recruit_app_clan_prop')
        ->endGroup()
        ->GET('/{page}',                        '#admin/AdminMiscEndpoint')
    ->endGroup()

    // DTG Discord #clan-recruitment form
    // --------------------------------------------------------------------------------
    ->GET('/clan-recruitment',                  '#ClanRecruitment')
    ->POST('/clan-recruitment',                 '#ClanRecruitment:submit')

    // DTG Discord Clan Apply
    // --------------------------------------------------------------------------------
    //->GET('/clan-apply',                        '#ClanApply')
    //->POST('/clan-apply',                       '#ClanApply:submit')

;