<?php
namespace app\lib\flair;

use app\lib\RedditMod;
use app\lib\flair\FlairConfig;
use app\lib\flair\FlairChangeResult;

class FlairManager {
    const SUBREDDIT     = "DestinyTheGame";
    const API_ENDPOINT  = array(
        'SET_FLAIR' => "https://oauth.reddit.com/r/%s/api/flair",
        'STYLESHEET' => "https://oauth.reddit.com/r/%s/about/stylesheet",
    );
    const CACHED_IMAGES_KEY = 'dtg_images';
    const CACHED_IMAGES_EXP = 60 * 60 * 24 * 7; // 7 days
    const DB_LOG_TABLE = 'user_flair_log';

    private static function get_endpoint($name) {
        return sprintf(self::API_ENDPOINT[$name], self::SUBREDDIT);
    }

    // stylesheet images
    // --------------------------------------------------------------------------------
    public static function force_stylesheet_images_cache_expiry() {
        cached(self::CACHED_IMAGES_KEY, '');
    }

    /**
     * Get stylesheet images.
     *
     * @return array returns an associative array in the form: `name => {url, css_link, name}`
     */
    public static function get_stylesheet_images(): array {
        $cached_val = cached(self::CACHED_IMAGES_KEY);

        if (empty($cached_val)) {
            $res = reddit_mod()->get_client()->fetch(self::get_endpoint('STYLESHEET'));
            $res = $res['result']['data']['images'];

            $image_map = [];
            foreach ($res as $item) {
                $image_map[$item['name']] = $item;
            }

            cached(self::CACHED_IMAGES_KEY, serialize($image_map), self::CACHED_IMAGES_EXP);
            return $image_map;
        } else {
            return unserialize($cached_val);
        }
    }

    /**
     * Get style image URLs.
     *
     * @return string[] an array of the URLs of all stylesheet images
     */
    public static function get_stylesheet_image_urls(): array {
        return array_filter(array_map(function($item) {
            return $item['url'] ?? null;
        }, array_values(self::get_stylesheet_images())));
    }

    // validators
    // --------------------------------------------------------------------------------
    public static function isValidFlairClass(string $css_class): string {
        if (empty($css_class) || !FlairConfig::is_valid_class($css_class)) {
            return FlairChangeResult::INVALID_FLAIR_CLASS;
        }
        return FlairChangeResult::VALID;
    }

    public static function isValidFlairText(string $text): string {
        if (strlen($text) > 64) {
            return FlairChangeResult::FLAIR_TEXT_TOO_LONG;
        }
        return FlairChangeResult::VALID;
    }

    public static function isValidAvailability(string $css_class): string {
        if (reddit_user()->is_admin()) {
            return FlairChangeResult::VALID;
        }
        if (!FlairConfig::is_class_available($css_class)) {
            return FlairChangeResult::NOT_AUTHORIZED;
        }
        return FlairChangeResult::VALID;
    }

    protected static function guard(string $user, string $flair_class, string $flair_text) {
        yield self::isValidFlairClass($flair_class);
        yield self::isValidAvailability($flair_class);
        yield self::isValidFlairText($flair_text);
    }

    // set flair
    // --------------------------------------------------------------------------------

    /**
     * Set a user's flair on `FlairManager::SUBREDDIT`.
     *
     * @param string $user the username of the reddit user to assign the flair to
     * @param string $flair_class flair class, use spaces to assign multiple classes
     * @param string $flair_text='' optional flair text
     * @return string a constant under `FlairChangeResult`; if successful, `FlairChangeResult::SUCCESS`
     */
    public static function set_flair(string $user, string $flair_class, string $flair_text = ''): string {
        foreach (self::guard($user, $flair_class, $flair_text) as $guardResultCode) {
            if ($guardResultCode !== FlairChangeResult::VALID) {
                return $guardResultCode;
            }
        }

        $res = reddit_mod()->get_client()->fetch(
            self::get_endpoint('SET_FLAIR'),
            array(
                'name'      => $user,
                'css_class' => $flair_class,
                'text'      => $flair_text,
            ),
            \OAuth2Client\Client::HTTP_METHOD_POST
        );

        $res_status = from($res['result'], 'success', 'to_bool');

        return $res_status ? FlairChangeResult::SUCCESS : FlairChangeResult::REDDIT_REJECTED;
    }

}