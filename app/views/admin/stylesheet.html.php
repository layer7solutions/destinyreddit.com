
<div class="deployable_stylesheet_area">
    <h2>Stylesheet CTL</h2>
    <?php foreach (a_stash('mod_tree') as $pkg_name => $mod_files): ?>
    <article class="deployable_stylesheet" for="<?php xecho($pkg_name) ?>">
        <div class="header">
            <h2><?php xecho($pkg_name) ?></h2>
        </div>
        <div id="<?php xecho($pkg_name) ?>__core_base" class="core_base collapsable">
            <?php render_template('admin.stylesheet_modhead', [
                'mod_name' => 'Core Modules (readonly)', 'mod_type' => 'core_module' ]
            ); ?>
            <div class="core_modules">
                <?php foreach ($mod_files as $mod_name => $mod_data) {
                    if ($mod_data['mod_type'] == 'core_module') {
                        render_template('admin.stylesheet_module', $mod_data);
                    }
                } ?>
            </div>
        </div>
        <div id="<?php xecho($pkg_name) ?>__user_base" class="user_base collapsable">
            <?php render_template('admin.stylesheet_modhead', [
                'mod_name' => 'User Modules', 'mod_type' => 'user_module' ]
            ); ?>
            <div class="user_modules">
                <?php foreach ($mod_files as $mod_name => $mod_data) {
                    if ($mod_data['mod_type'] == 'user_module') {
                        render_template('admin.stylesheet_module', $mod_data);
                    }
                } ?>
                <section class="new_user_module_area">
                    <button class="new_user_module_button primary primary--2"
                        data-pkg-name="<?php xecho($pkg_name) ?>">New Module</button>
                </section>
            </div>
        </div>
        <div class="deploy_area">
            <button class="save_button primary primary--2"
                data-pkg-name="<?php xecho($pkg_name) ?>">Save</button>
            <button class="deploy_button primary primary--2"
                data-pkg-name="<?php xecho($pkg_name) ?>">Save &amp; Deploy</button>
            <div class="deploy_statuses">
                <p class="deploy_status success deploy_status--save hide">
                    <span>Successfully saved!</span>
                </p>
                <p class="deploy_status failure deploy_status--save hide">
                    <span>Failed to saved.</span>
                </p>
                <p class="deploy_status success deploy_status--save_deploy hide">
                    <span>Successfully saved &amp; deployed!</span>
                </p>
                <p class="deploy_status failure deploy_status--save_deploy hide">
                    <span>Failed to deploy.</span>
                </p>
            </span>
        </div>
    </article>
    <?php endforeach; ?>
</div>