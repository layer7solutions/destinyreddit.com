<?php
namespace app\lib\flair;

abstract class FlairChangeResult {
    const SUCCESS = "success";
    const VALID = "valid";
    const REDDIT_REJECTED = "Reddit rejected the request. Try again later.";
    const INVALID_FLAIR_CLASS = "Not a valid choice.";
    const FLAIR_TEXT_TOO_LONG = "Flair text is too long (max: 64 characters).";
    const NOT_AUTHORIZED = "You are not allowed to select this flair option.";
}