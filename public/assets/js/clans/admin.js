init('ClansACP', function(opts) {
    n.on('.App__editableField .edit', 'click', function() {
        var parent = n.closest(this, '.App__editableField');
        n.addClass(parent.querySelectorAll('.outOfEdit'), 'hide');
        n.removeClass(parent.querySelectorAll('.inEdit'), 'hide');
    });

    n.on('.App__editableField .cancel', 'click', function() {
        var parent = n.closest(this, '.App__editableField');
        n.addClass(parent.querySelectorAll('.inEdit'), 'hide');
        n.removeClass(parent.querySelectorAll('.outOfEdit'), 'hide');
    });

    n.on('.App__editableField .save', 'click', function() {
        var parent = n.closest(this, '.App__editableField');
        var input = parent.querySelector('.fieldEdit');
        var status = parent.querySelector('.status');
        var saveButton = parent.querySelector('.save');
        var cancelButton = parent.querySelector('.cancel');

        var app_id = n.closest(this, '.App__view').getAttribute('data-id');
        var field = parent.getAttribute('data-field');

        if (!app_id || !field)
            return;

        r('POST', '/admin/clans/recruit_app_clan_prop', {
            data: {
                'app_id': app_id,
                'field': field,
                'value': input.value,
            },
            start: function() {
                saveButton.setAttribute('disabled', 'disabled');
                cancelButton.setAttribute('disabled', 'disabled');
                status.innerHTML = 'Saving...';
            },
            success: function(result) {
                status.innerHTML = 'Saved.';
                parent.querySelector('.fieldDisplay').innerText = input.value;
            },
            error: function() {
                status.innerHTML = 'An error occurred. Try again later.';
            },
            status_error: function() {
                status.innerHTML = 'An error occurred. Try again later.';
            },
            done: function() {
                n.addClass(parent.querySelectorAll('.inEdit'), 'hide');
                n.removeClass(parent.querySelectorAll('.outOfEdit'), 'hide');
                saveButton.removeAttribute('disabled');
                cancelButton.removeAttribute('disabled');
                setTimeout(function() {
                    status.innerHTML = '';
                }, 3000);
            }
        });
    });
});