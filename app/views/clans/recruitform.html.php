<div class="banner">
    <h1>
        <img class="icon" src="<?php img_src('DTG_Discord/Avatar-TransparentEdge.png') ?>"/>
        <span>r/DestinyTheGame Discord</span>
    </h1>
</div>
<aside>
    <h3>Application rules</h3>
    <ul>
        <li>As clan leader or admin, you must be an active server member in good standing.
        Please keep in mind that your clan will also be held responsible for the clan leader/admin's
        actions in the Discord server. Please participate in a few discussions prior to posting and
        maintain your activity level in the Discord server.</li>
        <li>Do not post more than once every 7 days.</li>
        <li>Limit your clan descriptions to 400 characters with proper grammar and spelling.</li>
        <li>No low effort/low quality recruitment posts.</li>
        <li>The only acceptable links are Bungie.net clan links.</li>
    </ul>
    <small>Failure to adhere to the above rules may result in your inability to advertise on behalf of your
    clan for an extended period of time or the removal of your advertisement entirely.</small>
</aside>
<form id="ClanRecruitmentForm" method="POST" class="form <?php if (!discord_user()->logged_in()): ?>not-logged-in<?php endif; ?>">
    <div class="form__head">
        <p>This form is for requesting your clan to be posted to the <code>#clan-recruitment</code>
        channel of the /r/DestinyTheGame Discord server.</p>
    </div>
    <section class="form__userlogin">
        <?php if (!discord_user()->logged_in()): ?>
        <div class="field">
            <div class="field__head">
                <h3>You must identify your Discord account to submit the form.</h3>
            </div>
            <div class="field__input">
                <a class="discord-login-button"
                    href="<?php echo SITE_URL ?>discord-login?cont=/clan-recruitment">Confirm Your Discord</a>
            </div>
        </div>
        <?php else: ?>
        <div class="field" style="padding:8px 10px 5px">
            <h3>Welcome <?php xecho(discord_user()->get_ping()) ?>
                <a class="discord-logout-button"
                    href="<?php echo SITE_URL ?>discord-logout?cont=/clan-recruitment">Logout</a>
            </h3>
            <div class="field__input hide">
                <input name="discord_username" type="hidden"
                    value="<?php xecho(discord_user()->get_username()) ?>" />
                <input name="discord_id" type="hidden"
                    value="<?php xecho(discord_user()->get_id()) ?>" />
                <input name="discord_discriminator" type="hidden"
                    value="<?php xecho(discord_user()->get_discriminator()) ?>" />
            </div>
        </div>
        <?php endif; ?>
    </section>
    <?php if (is_stashed('app') && u_stash('app')->isValid() && u_stash('app')->isStored()): ?>
    <section class="form__main">
        <div class="field">
            <div class="form__submitResponse"><?php echo u_stash('app')->getHTMLResponse() ?></div>
        </div>
    </section>
    <?php else: ?>
    <section class="form__main" <?php if (!discord_user()->logged_in()): ?>style="opacity:0.5"<?php endif; ?>>
        <?php if (is_stashed('app')): ?>
        <div class="field" style="color:red">The form was not filled out correctly.</div>
        <?php endif; ?>
        <div class="field" data-for="bungie_profile">
            <div class="field__head">
                <h3>Please link us your own Bungie.net profile.</h3>
                <span>This must be your own Bungie.net profile (NOT your Bungie.net clan link).</span>
            </div>
            <div class="field__input">
                <input name="bungie_profile" type="url" <?php if (!discord_user()->logged_in()) echo "disabled" ?> />
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">This field is required</span>
                <span class="field_error" data-errname="BungieURL">Must be a Bungie.net URL.</span>
            </div>
        </div>
        <div class="field" data-for="clan_name">
            <div class="field__head">
                <h3>What is the name of your clan?</h3>
                <span>e.g. Reddit Discord</span>
            </div>
            <div class="field__input">
                <input name="clan_name" type="text" <?php if (!discord_user()->logged_in()) echo "disabled" ?> />
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">This field is required</span>
                <span class="field_error" data-errname="MaxLengthExceeded">Must be less than 50 characters.</span>
                <span class="field_error" data-errname="MinLengthNotMet">Must be at least 3 characters.</span>
            </div>
        </div>
        <div class="field" data-for="platform_option">
            <div class="field__head">
                <h3>What platform(s) will your clan allow applicants from?</h3>
            </div>
            <div class="field__input">
                <label for="platform_pc" class="spacer-right">
                    <input type="hidden" name="platform_pc" value="">
                    <input id="platform_pc" name="platform_pc" class="platform_option"
                            type="checkbox" value="PC"
                        <?php if (!discord_user()->logged_in()) echo "disabled" ?>>
                    <span>PC</span>
                </label>
                <label for="platform_playstation" class="spacer-right">
                    <input type="hidden" name="platform_playstation" value="">
                    <input id="platform_playstation" name="platform_playstation" class="platform_option"
                            type="checkbox" value="Playstation"
                        <?php if (!discord_user()->logged_in()) echo "disabled" ?>>
                    <span>Playstation</span>
                </label>
                <label for="platform_xbox" class="spacer-right">
                    <input type="hidden" name="platform_xbox" value="">
                    <input id="platform_xbox" name="platform_xbox" class="platform_option"
                            type="checkbox" value="Xbox"
                        <?php if (!discord_user()->logged_in()) echo "disabled" ?>>
                    <span>Xbox</span>
                </label>
                <label for="platform_stadia" class="spacer-right">
                    <input type="hidden" name="platform_stadia" value="">
                    <input id="platform_stadia" name="platform_stadia" class="platform_option"
                            type="checkbox" value="Stadia"
                        <?php if (!discord_user()->logged_in()) echo "disabled" ?>>
                    <span>Stadia</span>
                </label>
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">Must select at least one platform.</span>
            </div>
        </div>
        <div id="RegionSelector" class="field" data-for="RegionOption[]">
            <div class="field__head">
                <h3>What region(s) will your clan allow applicants from?</h3>
                <span>Tell us what region(s) your clan wants to recruit from:</span>
            </div>
            <div class="field__input">
                <button type="button" class="RegionOption no-select" data-value="north america">
                    <span>North America</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </button>
                <button type="button" class="RegionOption no-select" data-value="south america">
                    <span>South America</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </button>
                <button type="button" class="RegionOption no-select" data-value="europe">
                    <span>Europe</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </button>
                <button type="button" class="RegionOption no-select" data-value="middle east">
                    <span>Middle East</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </button>
                <button type="button" class="RegionOption no-select" data-value="oceania">
                    <span>Oceania</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </button>
                <button type="button" class="RegionOption no-select" data-value="asia">
                    <span>Asia</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </button>
                <button type="button" class="RegionOption no-select" data-value="africa">
                    <span>Africa</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </button>
                <button type="button" class="RegionSelectAll no-select">
                    <span>Select All</span>
                </button>
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">Must select at least one region.</span>
            </div>
            <svg id="RegionSelector__map"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:amcharts="http://amcharts.com/ammap"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                version="1.1"
                viewBox="0 0 1020 660"
                width="40%"
                class="">
                <defs>
                    <style type="text/css">.land { fill: #CCCCCC; fill-opacity: 1; }</style>
                    <amcharts:ammap projection="mercator" leftLongitude="-169.6" topLatitude="83.68" rightLongitude="190.25" bottomLatitude="-55.55"></amcharts:ammap>
                </defs>
                <?php render_template('clans.recruitform-regionmap') ?>
            </svg>
        </div>
        <div class="field"  data-for="clan_desc">
            <div class="field__head">
                <h3>Briefly describe your clan.</h3>
                <div>
                    <span>Please limit your description to 400 characters with proper grammar and spelling.</span>
                </div>
                <div>
                    <span class="clan_desc__charcount">0</span><span> characters</span>
                </div>
            </div>
            <div class="field__input">
                <textarea name="clan_desc" style="min-height:100px"
                    <?php if (!discord_user()->logged_in()) echo "disabled" ?>></textarea>
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">This field is required</span>
                <span class="field_error" data-errname="MaxLengthExceeded">Must be 400 characters or less.</span>
                <span class="field_error" data-errname="MinLengthNotMet">Must be at least 25 characters.</span>
                <span class="field_error" data-errname="DiscordInvite">Should not contain a Discord invite.</span>
            </div>
        </div>
        <div class="field" data-for="clan_link">
            <div class="field__head">
                <h3>Please provide a link to your Bungie.net clan page.</h3>
                <span>e.g. https://www.bungie.net/en/ClanV2?groupid=2463957</span>
            </div>
            <div class="field__input">
                <input name="clan_link" type="url" <?php if (!discord_user()->logged_in()) echo "disabled" ?> />
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">This field is required</span>
                <span class="field_error" data-errname="BungieURL">Must be a Bungie.net clan link.</span>
            </div>
        </div>
        <?php if (discord_user()->logged_in()): ?>
        <div class="form__bottom">
            <input type="submit" />
        </div>
        <?php endif; ?>
    </section>
    <?php endif; ?>
</form>
