<?php
namespace app\controllers\admin;

use app\lib\clans\Application;
use app\lib\clans\ClanAdminPrefs;
use app\lib\clans\ManualQueue;
use app\lib\clans\Blacklist;
use app\lib\clans\forms\ApplyFormResult;
use app\lib\clans\forms\RecruitFormResult;
use app\lib\util\DiscordDirectMessage;

use app\controllers\admin\AdminMiscEndpointController;

class ClansACPController extends AdminMiscEndpointController {

    public function __construct() {
        if (\App::request_method() === 'GET') {
            $tz_name = discord_user()->logged_in() ?
                ClanAdminPrefs::get_php_name_timezone(discord_user()->get_id()) : null;
            if (!empty($tz_name)) {
                date_default_timezone_set($tz_name);
            }

            $this->middleware(function($params) {
                if (!discord_user()->is_admin()) {
                    return view('admin.clans_acp')
                        ->meta('Clans ACP', 'page--admin page--admin_clans')
                        ->stylesheets('clans/admin')
                        ->using([
                            'acp_title' => 'Access Denied',
                            'page'      => '403',
                        ]);
                }

                if (startsWith(from($params, 'page'), 'recruit') && !discord_user()->is_general_admin()) {
                    return view('admin.clans_acp')
                        ->meta('Clans ACP', 'page--admin page--admin_clans')
                        ->stylesheets('clans/admin')
                        ->using([
                            'acp_title' => 'Access Denied',
                            'page'      => '403',
                        ]);
                }

                if (startsWith(from($params, 'page'), 'apply') && !discord_user()->is_clan_admin()) {
                    return view('admin.clans_acp')
                        ->meta('Clans ACP', 'page--admin page--admin_clans')
                        ->stylesheets('clans/admin')
                        ->using([
                            'acp_title' => 'Access Denied',
                            'page'      => '403',
                        ]);
                }
            }, true);
        } else {
            $this->middleware(function($params) {
                if (!discord_user()->is_admin()) {
                    return view('admin.clans_acp')
                        ->meta('Clans ACP', 'page--admin page--admin_clans')
                        ->stylesheets('clans/admin')
                        ->using([
                            'acp_title' => 'Access Denied',
                            'page'      => '403',
                        ]);
                }
            });

            $this->interceptor(function() {
                redirect_cont();
            });
        }
    }

    // ------------------------------------------------------------------------------------------
    // GET - PAGE DISPATCH

    protected function default_action(array $params) {
        $data = ['page' => $params['page'] ?? 'home'];

        switch ($data['page']) {
            case 'home':
                $data['acp_title'] = 'ACP Home';
                $data['current_tz'] = discord_user()->logged_in() ?
                    ClanAdminPrefs::get(discord_user()->get_id(), 'timezone') : null;
                break;

            // recruitment apps
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            case 'recruit-queue':
                $data['acp_title'] = 'Manual Queue - Recruitment Requests';
                $data['app_type'] = 'recruit';
                $data['apps'] = ManualQueue::get_recruit_apps();
                $data['sort_type'] = ClanAdminPrefs::get(discord_user()->get_id(), ClanAdminPrefs::HISTORY_AMOUNT.$data['app_type']);
                break;
            case 'recruit-history':
                $data['acp_title'] = 'History - Recruitment Requests';
                $data['app_type'] = 'recruit';
                $data['listing_type'] = 'recruit_history';

                $page = intval($_REQUEST['page'] ?? 1);
                $amount = ClanAdminPrefs::get(discord_user()->get_id(), ClanAdminPrefs::HISTORY_AMOUNT.$data['listing_type']);
                $data += ManualQueue::get_recruit_history($page, $amount);
                break;

            // apply apps
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            case 'apply-queue':
                $data['acp_title'] = 'Manual Queue - DTG Discord Clan Applications';
                $data['app_type'] = 'apply';
                $data['apps'] = ManualQueue::get_apply_apps();
                $data['sort_type'] = ClanAdminPrefs::get(discord_user()->get_id(), ClanAdminPrefs::HISTORY_AMOUNT.$data['app_type']);
                break;
            case 'apply-history':
                $data['acp_title'] = 'History - DTG Discord Clan Applications';
                $data['app_type'] = 'apply';
                $data['listing_type'] = 'apply_history';

                $page = intval($_REQUEST['page'] ?? 1);
                $amount = ClanAdminPrefs::get(discord_user()->get_id(), ClanAdminPrefs::HISTORY_AMOUNT.$data['listing_type']);
                $data += ManualQueue::get_apply_history($page, $amount);
                break;

            // other pages
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            case 'blacklist':
                $data['acp_title'] = 'Blacklist';
                $data['blacklist'] = Blacklist::get_blacklisted();
                break;
            case 'message':
                $data['acp_title'] = 'Send a message';
                $data['listing_type'] = 'DM_list';

                $page = intval($_REQUEST['page'] ?? 1);
                $amount = ClanAdminPrefs::get(discord_user()->get_id(), ClanAdminPrefs::HISTORY_AMOUNT.$data['listing_type']);
                $data += ManualQueue::get_DM_list($page, $amount);
                break;
            default:
                $data['page'] = '404';
                $data['acp_title'] = '404 Not found';
                break;
        }

        return view('admin.clans_acp')
            ->meta('Clans ACP', 'page--admin page--admin_clans')
            ->stylesheets('clans/admin')
            ->scripts('clans/admin')
            ->using($data);
    }

    // ------------------------------------------------------------------------------------------
    // POST - MAIN SERVER ADMIN ONLY

    protected function recruit_app_clan_prop(array $params) {
        if (!discord_user()->is_general_admin()) {
            return view('admin.clans_acp')
                ->meta('Clans ACP', 'page--admin page--admin_clans')
                ->stylesheets('clans/admin')
                ->using([
                    'acp_title' => 'Access Denied',
                    'page'      => '403',
                ]);
        }

        if (isset($_REQUEST['app_id'], $_REQUEST['field'], $_REQUEST['value'])) {
            $app = Application::fromId(Application::TYPE_RECRUIT, $_REQUEST['app_id']);
            $app->data['clan'][$_REQUEST['field']] = $_REQUEST['value'];
            dispatch_json(['success' => $app->updateData()]);
        }
        dispatch_json(['success' => false], 400);
    }

    protected function modqueue_recruit(array $params) {
        if (!discord_user()->is_general_admin()) {
            return view('admin.clans_acp')
                ->meta('Clans ACP', 'page--admin page--admin_clans')
                ->stylesheets('clans/admin')
                ->using([
                    'acp_title' => 'Access Denied',
                    'page'      => '403',
                ]);
        }

        if (!isset($_POST['app_id'])) {
            return;
        }

        $app = Application::fromId(Application::TYPE_RECRUIT, $_POST['app_id']);
        $ACP_USER = discord_user()->get_ping();
        $result = null;

        if ($_POST['action'] === 'accept') {
            $result = RecruitFormResult::ACCEPTED_BY_MODERATOR(from($_POST, 'reason'), $ACP_USER, from($_POST, 'message'));
        } else if ($_POST['action'] === 'deny') {
            $result = RecruitFormResult::DENIED_BY_MODERATOR(from($_POST, 'reason'), $ACP_USER, from($_POST, 'message'));
        } else if ($_POST['action'] === 'dequeue') {
            $result = ApplyFormResult::SILENT_DEQUEUE(from($_POST, 'reason'));
        } else if ($_POST['action'] === 'enqueue') {
            $result = ApplyFormResult::SILENT_ENQUEUE(from($_POST, 'reason'));
        }

        $app->applyResult($result);
    }

    // ------------------------------------------------------------------------------------------
    // POST - CLAN SERVER ADMIN ONLY

    protected function modqueue_apply(array $params) {
        if (!discord_user()->is_clan_admin()) {
            return view('admin.clans_acp')
                ->meta('Clans ACP', 'page--admin page--admin_clans')
                ->stylesheets('clans/admin')
                ->using([
                    'acp_title' => 'Access Denied',
                    'page'      => '403',
                ]);
        }

        if (!isset($_POST['app_id'])) {
            return;
        }

        $app = Application::fromId(Application::TYPE_APPLY, $_POST['app_id']);
        $ACP_USER = discord_user()->get_ping();
        $result = null;

        if ($_POST['action'] === 'approve') {
            $result = ApplyFormResult::ACCEPTED_BY_MODERATOR(from($_POST, 'reason'), $ACP_USER, from($_POST, 'message'));
        } else if ($_POST['action'] === 'deny') {
            $result = ApplyFormResult::DENIED_INFRACTIONS(from($_POST, 'reason'), $ACP_USER, from($_POST, 'message'));
        } else if ($_POST['action'] === 'approve_custom') {
            if (empty($_POST['message']))
                die("Must supply custom message. Click the back button in your browser and try again.");
            $result = ApplyFormResult::ACCEPTED_BY_MODERATOR(from($_POST, 'reason'), $ACP_USER, from($_POST, 'message'));
        } else if ($_POST['action'] === 'deny_custom') {
            if (empty($_POST['message']))
                die("Must supply custom message. Click the back button in your browser and try again.");
            $result = ApplyFormResult::DENIED_BY_MODERATOR(from($_POST, 'reason'), $ACP_USER, from($_POST, 'message'));
        } else if ($_POST['action'] === 'dequeue') {
            $result = ApplyFormResult::SILENT_DEQUEUE(from($_POST, 'reason'));
        } else if ($_POST['action'] === 'enqueue') {
            $result = ApplyFormResult::SILENT_ENQUEUE(from($_POST, 'reason'));
        }

        $app->applyResult($result);
    }

    // ------------------------------------------------------------------------------------------
    // POST - EITHER ADMIN

    protected function home_action(array $params) {
        if ($_POST['action'] === 'prefs' && isset($_POST['prop']) && isset($_POST['value'])) {
            ClanAdminPrefs::set(discord_user()->get_id(), $_POST['prop'], $_POST['value']);
        }
    }

    protected function history_update(array $params) {
        if (isset($_POST['listing_type'])) {
            $property = ClanAdminPrefs::HISTORY_AMOUNT.$_POST['listing_type'];

            if ($_POST['action'] === 'history_amount' && isset($_POST['amount'])) {
                ClanAdminPrefs::set(discord_user()->get_id(), $property, $_POST['amount']);
            }

            if ($_POST['action'] === 'history_amount_reset') {
                ClanAdminPrefs::set(discord_user()->get_id(), $property, ManualQueue::DEFAULT_ITEMS_AMOUNT);
            }
        }

        if ($_POST['action'] === 'page_jump') {
            redirect(($_POST['cont'] ?? \App::request_uri()).'?page='.$_POST['page']);
        }
    }

    protected function modqueue_sorting(array $params) {
        if (isset($_POST['queue_type']) && isset($_POST['sort_type'])) {
            $property = ClanAdminPrefs::QUEUE_SORT.$_POST['queue_type'];
            ClanAdminPrefs::set(discord_user()->get_id(), $property, $_POST['sort_type']);
        }
    }

    protected function blacklist_add(array $params) {
        if (isset($_POST['fieldtype'], $_POST['fieldval'])) {
            Blacklist::add_to_blacklist($_POST['fieldtype'], $_POST['fieldval']);
        }
    }

    protected function blacklist_remove(array $params) {
        if (isset($_POST['id'])) {
            Blacklist::remove_from_blacklist($_POST['id']);
        }
    }

    protected function send_message(array $params) {
        if (!empty($_POST['to']) && !empty($_POST['body'])) {
            $msg = new DiscordDirectMessage($_POST['to'], $_POST['body'], discord_user()->get_ping());
            $msg->send();
        } else {
            die("Missing one or more fields. Click the back button in your browser and try again.");
        }
    }

}