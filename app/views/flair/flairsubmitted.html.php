<div><h1>Flair Selector</h1></div>
<style type="text/css"><?php echo u_stash('flair_css') ?></style>
<?php
$flair_user = x_stash('flair_user');
if ($flair_user == reddit_user()->get_username()) {
    $flair_user = 'your';
} else {
    $flair_user .= "'s";
}
?>
<div class="flair-submitted">
    <?php if (!empty(u_stash('error'))): ?>
        <div class="flair-submitted-error">
            <p>Failed to set <?php echo $flair_user ?> flair: <?php echo x_stash('error') ?></p>
        </div>
    <?php else: ?>
        <div class="flair-submitted-result">
            <p>You have succesfully set <?php echo $flair_user ?> new /r/<?php echo x_stash('subreddit') ?> flair to:</p>

            <div class="flair flair-<?php echo implode(' flair-', explode(' ', x_stash('flair_class'))) ?>"></div>
        </div>
        <div class="more-info">
            <p>It may take an hour or so for <?php echo $flair_user ?> existing comments to update to
            <?php echo $flair_user ?> newly set flair. But all new comments should have <?php echo $flair_user ?>
            new flair.</p>
        </div>
    <?php endif; ?>
    <div class="links">
        <a href="https://reddit.com/r/<?php echo x_stash('subreddit') ?>">Return to /r/<?php echo x_stash('subreddit') ?></a><br/>
        <a href="<?php echo SITE_URL ?>flair">Back to flair selector</a>
    </div>
</div>